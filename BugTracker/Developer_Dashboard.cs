﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient; // Library for connecting to SQL Server
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Tulpep.NotificationWindow; //Library for Displaying Notification

namespace BugTracker
{
    public partial class Developer_Dashboard : Form
    {
        //String con = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        // Creates a connection to the database
        SqlConnection con = new SqlConnection(@"Data Source=utsav;Initial Catalog=Bug_tracker_db;Integrated Security=True");

        //Global variable declarations
        SqlCommand cmd;
        String Name;
        String link;
        String filepath;
        OpenFileDialog open = new OpenFileDialog();

        //Username Argument passed for the Session Purpose
        public Developer_Dashboard(String Username)
        {
            InitializeComponent();
            session_username.Text = Username;            
            Name = Username;
        }

        private void Developer_Dashboard_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet11.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter1.Fill(this.bug_tracker_dbDataSet11.Project);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet10.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter.Fill(this.bug_tracker_dbDataSet10.Project);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet5.Bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter1.Fill(this.bug_tracker_dbDataSet5.Bugs);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet4.Bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter.Fill(this.bug_tracker_dbDataSet4.Bugs);
            getBugs();
            getProject();
            getProfile();
            projectPanel.Show();
            view_report_Panel.Hide();
            view_bugsPanel.Hide();
            update_bugs_Panel.Hide();
            profilePanel.Hide();
            notification();
        }

        // Event handler for Exit Button
        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit(); // Exits the application
        }

        //Event handler When view Project Button is clicked to show or hide panels
        private void view_project_Click(object sender, EventArgs e)
        {
            update_projectPanel.Hide();
            projectPanel.Show();
            view_report_Panel.Hide();
            view_bugsPanel.Hide();
            update_bugs_Panel.Hide();
            profilePanel.Hide();
        }

        //Event handler When View Bugs Button is clicked to show or hide panels
        private void view_BugsButton_Click(object sender, EventArgs e)
        {
            update_projectPanel.Hide();
            view_report_Panel.Hide();
            view_bugsPanel.Show();
            profilePanel.Hide();
            update_bugs_Panel.Hide();
            projectPanel.Hide();
        }

        //Event handler When Update Bugs Button is clicked to show or hide panels
        private void Update_BugsButton_Click(object sender, EventArgs e)
        {
            update_projectPanel.Hide();
            update_bugs_Panel.Show();
            view_report_Panel.Hide();
            view_bugsPanel.Hide();
            profilePanel.Hide();
            projectPanel.Hide();
        }

        //Event handler When Update Project Button is clicked to show or hide panels
        private void update_project_Click(object sender, EventArgs e)
        {
            update_projectPanel.Show();
            update_bugs_Panel.Hide();
            view_report_Panel.Hide();
            view_bugsPanel.Hide();
            profilePanel.Hide();
            projectPanel.Hide();
        }

        // Event handler When Viewe Bugs Button is clicked
        private void view_bugsPanel_Click(object sender, EventArgs e)
        {
            getBugs();
        }

        //Event handler When LogoutButton is clicked 
        private void logout_Click(object sender, EventArgs e)
        {
            Application.Restart(); // restarts the application
        }

        //Event handler When Profile Button is clicked to show or hide panels
        private void profile_Click(object sender, EventArgs e)
        {
            update_projectPanel.Hide();
            profilePanel.Show();
            view_report_Panel.Hide();
            view_bugsPanel.Hide();
            update_bugs_Panel.Hide();
            projectPanel.Hide();
        }

        //Event handler When View Report Button is clicked to show or hide panels
        private void view_reportButton_Click(object sender, EventArgs e)
        {
            view_report_Panel.Show();
            view_bugsPanel.Hide();
            update_bugs_Panel.Hide();
            profilePanel.Hide();
            projectPanel.Hide();
        }

        /// <summary>
        /// Displays all the Bugs from the database table and assigns to a particular DataGridView
        /// </summary>
        public void getBugs()
        {
            con.Open();
            String str = "select * from Bugs where Assigned_to='" + Name + "' Order by FillDate ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        /// <summary>
        /// Displays all the Projects from the database table and assigns to a particular DataGridView
        /// </summary>
        public void getProject()
        {
            con.Open();
            String str = "select * from Project where Assigned_to='" + Name + "' Order by Assigned_date ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            projectReportdatagridView.DataSource = dt;
            con.Close();
        }

        /// <summary>
        /// Selects the logged in user and displays the value in a profile Textbox
        /// </summary>
        public void getProfile()
        {
            //fetch and display the data in textbox
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Users Where Username='" + Name + "'", con);
            SqlDataReader dr = cmd.ExecuteReader(); 
            //Displays all the data from database in respective profile textBoxes 
            if (dr.Read())
            {
                text_name.Text = (dr["Name"].ToString());
                text_email.Text = (dr["Email"].ToString());
                text_username.Text = (dr["Username"].ToString());
                text_password.Text = (dr["Password"].ToString());

                // Checks if the table row consist of a image path value
                try
                {
                    String img_path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    pictureBox1.Image = Image.FromFile(img_path + dr["Image"].ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("please update your profile picture!");
                }
            }
            con.Close();
        }

       //Event handler for filtering data into the datagrid view
        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where status = '" + filter.Text + "' and Assigned_to='" + Name + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd); //Represents a set of data commands and a database connection that are used to fill the DataSet and update a SQL Server database.
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        //Event handler for Searching data inside the datagrid view
        private void searchBtn_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where CONCAT(Status,Bug_title,Priority,Assigned_to,Bug_Assigned_by,Date_of_delivery,Additional_info,Bug_line_no,project,Class,method,Code_author_name) LIKE '%" + search_textbox.Text + "%' and Assigned_to='" + Name + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);//Represents a set of data commands and a database connection that are used to fill the DataSet and update a SQL Server database.
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        private void filter_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where status = '" + filter_combobox.Text + "' and Assigned_to='" + Name + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler for Filtering data inside the datagrid view
        private void comboBox_filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Project where project_status = '" + comboBox_filter.Text + "' and Assigned_to='" + Name + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            projectReportdatagridView.DataSource = dt;
            con.Close();
        }

        //Event handler when view report button is clicked
        private void view_report_Panel_Click(object sender, EventArgs e)
        {
            getBugs(); //function calling 
        }

        //Event handler when Update bugs Button is clicked
        private void update_bugsClick(object sender, EventArgs e)
        {
                using (OpenFileDialog open = new OpenFileDialog())
                {
                //Selects only file with the following extentions
                    open.Filter = "Text documents (.pdf)|*.pdf|Excel(.xls)|*.xls|Excel(.xlsx)|*.xlsx|All Files (*.*)|*.*";
                    open.Multiselect = false;
                    var result = open.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        filepath = open.FileName;
                    }
                    String completeImageFileName = open.FileName;
                    String file = open.FileName; string[] f = file.Split('\\'); // to get the only file name
                    //Copies the file in the following folder
                    String fn = f[(f.Length) - 1]; string dest = @"C:\Users\Vastu\source\repos\BugTracker\BugTracker\Files\" + fn; //to copy the file to the destination folder 
                    File.Copy(file, dest, true);
                }
                con.Open();
                String str = "update bugs set Files='" + filepath + "', FixedDate='" + dateTimePicker2.Text + "', status='" + status_combo.SelectedItem + "', Bug_title='" + title_txtbox.Text + "',Priority='" + priority_combo.SelectedItem + "',Additional_info='" + desc_txtbox.Text + "',Bug_line_no='" + line_txtbox.Text + "', Project='" + project_txtbox.Text + "', Class='" + class_txtbox.Text + "', method='" + method_txtbox.Text + "',Code_author_name='" + author_txtbox.Text + "' where Bug_ID='" + id_txtbox.Text + "' ";
                SqlCommand cmd = new SqlCommand(str, con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Bugs Sucessfully Updated!");
                con.Close();
        }

        //Event handler When Show Values button is clicked while updating bugs
        private void show_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Bugs where Bug_ID='" + id_txtbox.Text + "' ", con);
            SqlDataReader dr = cmd.ExecuteReader(); //Provides a way of reading a forward - only stream of rows from a SQL Server database

            if (dr.Read())
            {
                title_txtbox.Text = (dr["Bug_title"].ToString());
                line_txtbox.Text = (dr["Bug_line_no"].ToString());
                project_txtbox.Text = (dr["project"].ToString());
                class_txtbox.Text = (dr["class"].ToString());
                method_txtbox.Text = (dr["method"].ToString());
                author_txtbox.Text = (dr["Code_author_name"].ToString());
                desc_txtbox.Text = (dr["Additional_info"].ToString());

            }

            con.Close();
        }

        //Event Handler when Upload button is clicked
        private void upload_btn_Click(object sender, EventArgs e)
        {
            //To where your opendialog box get starting location. My initial directory location is desktop.
            open.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            open.Title = "Select image to be upload.";
            //which type image format you want to upload in database. just add them.
            open.Filter = "Image Only(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            open.FilterIndex = 1;
            try
            {
                if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (open.CheckFileExists)
                    {
                        //Gets the full path  of the file
                        string path = System.IO.Path.GetFullPath(open.FileName);
                        label1.Text = path;
                        pictureBox1.Image = new Bitmap(open.FileName);
                        pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage; //Stretches the images according to the size of PictureBox
                    }
                }
                else
                {
                    MessageBox.Show("Please Upload image.");
                }
            }
            catch (Exception ex)
            {
                //it will give if file is already exits..
                MessageBox.Show(ex.Message);
            }
        }

        //Event handler when update button is clicked in a profile panel
        private void Update_Click(object sender, EventArgs e)
        {
            try
            {
                //Gets the full path of the file
                String filename = System.IO.Path.GetFileName(open.FileName);

                con.Open();
                String sql = "UPDATE users SET Image='\\Image\\" + filename + "', Name = '" + text_name.Text + "', Email = '" + text_email.Text + "',Username='" + text_username.Text + "',Password= '" + text_password.Text + "' where Username='" + Name + "' ";

                SqlCommand cmd = new SqlCommand(sql, con);
                String path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(open.FileName, path + "\\Image\\" + filename);
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Updated successfully!!");


            }
            catch (Exception ex)
            {
                con.Close();
                MessageBox.Show(ex.Message);

            }
        }

        // Event handler when link Label is clicked
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Bugs Where Assigned_to='" + Name + "' ", con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                link = (dr["Version_Control_link"].ToString());
                
            }
            con.Close();
            //linkLabel1.Text = link;
            if (link != "")
            {
                Process.Start(link);
            }
            else
                MessageBox.Show("No link has been provided!");
            
        }

        //links to the version control  inside the datagrid view report
        private void reports_dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string sUrl = reports_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                ProcessStartInfo sInfo = new ProcessStartInfo(sUrl);

                Process.Start(sInfo);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Oops, something is wrong!");
            }
            

        }

        //links to the version control  inside the datagrid view report
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

            try
            {
                string sUrl = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                ProcessStartInfo sInfo = new ProcessStartInfo(sUrl);

                Process.Start(sInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Oops, something is wrong!");
            }
        }

        //Syntax Highlighting for CSharp Programming Language
        private void desc_txtbox_Load(object sender, EventArgs e)
        {
            desc_txtbox.Language = FastColoredTextBoxNS.Language.CSharp;
        }

        //links to the version control  inside the datagrid view report
        private void projectReportdatagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string sUrl = projectReportdatagridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                ProcessStartInfo sInfo = new ProcessStartInfo(sUrl);

                Process.Start(sInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Oops, something is wrong!");
            }
            
        }

        // Event handler when Show Project Putton is clicked
        private void show_project_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Project where Project_ID='" + id_textbox.Text + "' ", con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
             
                comments_textbox.Text = (dr["Additional_details"].ToString());

            }

            con.Close();
        }

        //Event handler when update project Button is clicked
        private void update_button_Click(object sender, EventArgs e)
        {
            con.Open();
            String str = "update Project set  FixedDate='" + dateTimePicker1.Text + "', project_status='" + status_combobox.SelectedItem + "', Additional_details='" + comments_textbox.Text + "' where Project_ID='" + id_textbox.Text + "' ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Project Sucessfully Updated!");
            con.Close();
        }

        //event handler when refresh button is clicked
        private void refresh_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Project";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            projectReportdatagridView.DataSource = dt;
            con.Close();
        }

        //event handler when refresh button is clicked
        private void refresh2_Click(object sender, EventArgs e)
        {
            con.Open();
            String str = "select * from Bugs where Assigned_to='" + Name + "' Order by FillDate ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        /// <summary>
        /// Displays the notification to the User
        /// </summary>
        public void notification()
        {

            con.Open();
            string query = "SELECT * FROM Bugs WHERE Status='Open' and Assigned_to='"+Name+"' ";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dtbll = new DataTable();
            sda.Fill(dtbll);
            if (dtbll.Rows.Count == 0)
            {
                // Creating objects Of PopupNotifier Class
                PopupNotifier popup = new PopupNotifier();
                popup.Image = Properties.Resources.noti;
                popup.TitleText = "Bug Tracking System";
                popup.ContentText = "You have things to do.. Good Luck!";
                popup.Popup(); // shows the notification
            }
            else
            {
                // Creating objects Of PopupNotifier Class
                PopupNotifier popup = new PopupNotifier();
                popup.Image = Properties.Resources.noti;
                popup.TitleText = "Bug Tracking System";
                popup.ContentText = "Hello, You have no work until now!";
                popup.Popup(); // shows the notification
            }
            con.Close();

        }
    }
}
