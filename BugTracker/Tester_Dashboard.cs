﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient; //Library for connecting to SQL Server
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Tulpep.NotificationWindow; // Library for displaying Notification

namespace BugTracker
{
    public partial class Tester_Dashboard : Form
    {
        //String con = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        // Creates a connection to the database
        SqlConnection con = new SqlConnection(@"Data Source=utsav;Initial Catalog=Bug_tracker_db;Integrated Security=True");
        DataSet ds = new DataSet();
        // global variable declarations
        SqlCommand cmd;
        String excelPath;
        String Name;
        OpenFileDialog open = new OpenFileDialog(); //Opens the new FIle Dialog


        //Username Argument passed for the Session Purpose
        public Tester_Dashboard(String Username)
        {
            InitializeComponent();
            session_username.Text = Username;
            assigned_byCombobox.Text = Username;
            Name=Username;
        }

        // Event handler When Exit button is clicked
        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit(); //Exits the application
        }

        // Event handler When Add Bugs button is clicked
        private void bugsButton_Click(object sender, EventArgs e)
        {
            add_bugsPanel.Show();
            //bug_detailsPanel.Hide();
            update_bugs_Panel.Hide();
            view_report_Panel.Hide();
            profilePanel.Hide();
        }

        // Event handler To load the Tester's Dashboard
        private void Tester_Dashboard_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet3.Bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter.Fill(this.bug_tracker_dbDataSet3.Bugs);
            display_report();
            getUsers();
            add_bugsPanel.Show();
            //bug_detailsPanel.Hide();
            update_bugs_Panel.Hide();
            view_report_Panel.Hide();
            profilePanel.Hide();
            notification(); 

            //fetch and display the data in textbox
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Users Where Username='" + Name + "'", con);
            SqlDataReader dr = cmd.ExecuteReader();//provides the way of reading Rows from the database Tables

            //Displays all the data from database in respective profile textBoxes 
            if (dr.Read())
            {
                text_name.Text = (dr["Name"].ToString());
                text_email.Text = (dr["Email"].ToString());
                text_username.Text = (dr["Username"].ToString());
                text_password.Text = (dr["Password"].ToString());

                // Checks if the table row consist of a image path value
                try
                {
                    String img_path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    pictureBox1.Image = Image.FromFile(img_path + dr["Image"].ToString());
                }
                catch(Exception ex)
                {
                    MessageBox.Show("please update your profile picture!");
                }
            }
            con.Close();

        }

        // Event handler When Update Bugs Button is clicked to show or hide panels
        private void update_bugsButton_Click(object sender, EventArgs e)
        {
            display_report();
            add_bugsPanel.Hide();
            //bug_detailsPanel.Hide();
            update_bugs_Panel.Show();
            getUsers();
            view_report_Panel.Hide();
            profilePanel.Hide();
        }

        // Event handler When View Report Button is clicked to show or hide panels
        private void view_reportButton_Click(object sender, EventArgs e)
        {
            display_report();
            add_bugsPanel.Hide();
            //bug_detailsPanel.Hide();
            update_bugs_Panel.Hide();
            view_report_Panel.Show();
            profilePanel.Hide();
        }

        // Event handler When Profile Button is clicked to show or hide panels
        private void profile_Click(object sender, EventArgs e)
        {
            display_report();
            add_bugsPanel.Hide();
            //bug_detailsPanel.Hide();
            update_bugs_Panel.Hide();
            view_report_Panel.Hide();
            profilePanel.Show();
        }

        // Event handler When Add Bugs Button is clicked to add bugs into the database table
        private void add_bugsButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog open = new OpenFileDialog())
            {
                // opens the files with following extentions
                open.Filter = "Text documents (.pdf)|*.pdf|Excel(.xls)|*.xls|Excel(.xlsx)|*.xlsx|All Files (*.*)|*.*";
                open.Multiselect = false;
                var result = open.ShowDialog();
                if (result == DialogResult.OK)
                {
                    excelPath = open.FileName;
                }
            String completeImageFileName = open.FileName;
            String file = open.FileName; string[] f = file.Split('\\'); // to get the only file name
            // copy the file to the destination folder 
            String fn = f[(f.Length) - 1]; string dest = @"C:\Users\Vastu\source\repos\BugTracker\BugTracker\Files\" + fn; 
            File.Copy(file, dest, true);
            }
            con.Open();
            String str = "insert into Bugs(Version_Control_Link,Files,Status,Bug_title,Priority,Assigned_to,Bug_Assigned_by,Date_of_delivery,Additional_info,Bug_line_no,project,Class,method,Code_author_name) values('" + git.Text + "','" + excelPath + "','" + statusTextbox.Text + "','" + bug_titleTextbox.Text + "','" + priorityCombobox.SelectedItem + "','" + assigned_toCombobox.Text + "', '" + assigned_byCombobox.Text + "','" + dateTimePicker1.Text + "','" + bug_descriptionTextbox.Text + "','" + line_no_Textbox.Text + "','" + project_Textbox.Text + "','" + class_Textbox.Text + "','" + method_Textbox.Text + "','" + author_Textbox.Text + "' )";
            SqlCommand cmd = new SqlCommand(str, con);
            
            cmd.ExecuteNonQuery();
            MessageBox.Show("Bugs Sucessfully Assigned!");
            con.Close(); 

        }

        // Event Handler For Logout button 
        private void logout_Click(object sender, EventArgs e)
        {
            Application.Restart(); // Application restarts
        }

        /// <summary>
        /// Selects all the users whose designation is developer and assignes the value to a particular Combobox
        /// </summary>
        public void getUsers()
        {
            //Represents an in-memory cache of data.
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select Name, Username from users where designation='Developer' ", con);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
                assigned_toCombobox.DisplayMember = "Name";
                assigned_toCombobox.ValueMember = "Username";
                assigned_toCombobox.DataSource = ds.Tables[0];
                assigned_combo.DisplayMember = "Name";
                assigned_combo.ValueMember = "Username";
                assigned_combo.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //Exception Message
            }
            finally
            {
                con.Close();
            }
        
        }


        /// <summary>
        /// Selects  all the data from bugs table and displays in the respective fields when updating Bugs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void show_Click(object sender, EventArgs e)
        {
            
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Bugs where Bug_ID='"+ id_txtbox.Text + "' ", con);
            SqlDataReader dr = cmd.ExecuteReader();
           
            if (dr.Read())
            {
                title_txtbox.Text = (dr["Bug_title"].ToString());
                line_txtbox.Text = (dr["Bug_line_no"].ToString());
                project_txtbox.Text = (dr["project"].ToString());
                class_txtbox.Text = (dr["class"].ToString());
                method_txtbox.Text = (dr["method"].ToString());
                author_txtbox.Text = (dr["Code_author_name"].ToString());
                desc_txtbox.Text = (dr["Additional_info"].ToString());
                
            }
            
            con.Close();
            
        }

        //Event Handler for Update bugs button
        private void update_bugs_Click(object sender, EventArgs e)
        {
            con.Open();
            String str = "update bugs set status='" + status_combo.SelectedItem + "',Bug_title='" + title_txtbox.Text + "',Priority='" + priority_combo.SelectedItem + "',Additional_info='" + desc_txtbox.Text + "',Bug_line_no='" + line_txtbox.Text + "', Project='" + project_txtbox.Text + "', Class='" + class_txtbox.Text + "', method='" + method_txtbox.Text + "',Code_author_name='" + author_txtbox.Text + "' where Bug_ID='" + id_txtbox.Text + "' ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Bugs Sucessfully Updated!");
            con.Close();
        }

        /// <summary>
        /// Selects all the data from database and displays in a particular DataGridView
        /// </summary>
        public void display_report()
        {
            con.Open();
            String str = "select * from Bugs where Bug_Assigned_by='"+Name+"' ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            //Represents a set of data commands and a database connection that are used to fill the DataSet and update a SQL Server database.
            SqlDataAdapter adapt = new SqlDataAdapter(cmd); 
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler for Filtering data
        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where status = '"+filter.Text+ "' and Bug_Assigned_by='" + Name + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
                    
        }

        //Event handler for Searching data
        private void search_button_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where CONCAT(Status,Bug_title,Priority,Assigned_to,Bug_Assigned_by,Date_of_delivery,Additional_info,Bug_line_no,project,Class,method,Code_author_name) LIKE '%" + search.Text+ "%' and Bug_Assigned_by='" + Name + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }
        //Event handler Upload Button CLick
        private void upload_btn_Click(object sender, EventArgs e)
        {
            //To where your opendialog box get starting location. My initial directory location is desktop.
            open.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            open.Title = "Select image to be upload.";
            //which type image format you want to upload in database. just add them.
            open.Filter = "Image Only(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            open.FilterIndex = 1;
            try
            {
                if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (open.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(open.FileName); // Gets the full Path of the file
                        label1.Text = path;
                        pictureBox1.Image = new Bitmap(open.FileName);
                        pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;// Stretches the image according to the size of PictureBox
                    }
                }
                else
                {
                    MessageBox.Show("Please Upload image.");
                }
            }
            catch (Exception ex)
            {
                //it will give if file is already exits..
                MessageBox.Show(ex.Message);
            }
        }


        //Event handler for Updating data
        private void Update_Click(object sender, EventArgs e)
        {
            try
            {
                String filename = System.IO.Path.GetFileName(open.FileName);

                con.Open();
                // sets the filename inside Images Folder
                String sql = "UPDATE users SET Image='\\Image\\" + filename + "', Name = '" + text_name.Text + "', Email = '" + text_email.Text + "',Username='" + text_username.Text + "',Password= '" + text_password.Text + "' where Username='" + Name + "' ";

                SqlCommand cmd = new SqlCommand(sql, con);
                String path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(open.FileName, path + "\\Image\\" + filename);
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Updated successfully!!");

            }
            catch (Exception ex)
            {
                con.Close();
                MessageBox.Show(ex.Message);

            }
        }

        //Syntax Highlighting for CSharp Programming Language
        private void desc_txtbox_Load(object sender, EventArgs e)
        {
            desc_txtbox.Language = FastColoredTextBoxNS.Language.CSharp;

        }

        //Syntax Highlighting for CSharp Programming Language
        private void bug_descTextbox_Load_1(object sender, EventArgs e)
        {
            bug_descriptionTextbox.Language = FastColoredTextBoxNS.Language.CSharp;
        }

        /// <summary>
        /// Displays the notification to the User according to the project and Bug Status
        /// </summary>
        public void notification()
        {

            con.Open();
            string query = "SELECT * FROM Bugs WHERE Status='Open' and Assigned_to='" + Name + "' ";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dtbll = new DataTable();
            sda.Fill(dtbll);
            if (dtbll.Rows.Count == 0)
            {
                // Creating objects Of PopupNotifier Class
                PopupNotifier popup = new PopupNotifier();
                popup.Image = Properties.Resources.noti;
                popup.TitleText = "Bug Tracking System";
                popup.ContentText = "You have things to do.. Good Luck!";
                popup.Popup(); // shows the notification
            }
            else
            {
                // Creating objects Of PopupNotifier Class
                PopupNotifier popup = new PopupNotifier();
                popup.Image = Properties.Resources.noti;
                popup.TitleText = "Bug Tracking System";
                popup.ContentText = "Hello, You have no work until now!";
                popup.Popup(); // shows the notification
            }
            con.Close();
            
        }
    }
}
