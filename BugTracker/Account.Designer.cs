﻿namespace BugTracker
{
    partial class Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Account));
            this.name_label = new System.Windows.Forms.Label();
            this.email_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.Button();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.text_password = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.Login = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.designation = new System.Windows.Forms.ComboBox();
            this.Register = new System.Windows.Forms.Button();
            this.designation_label = new System.Windows.Forms.Label();
            this.cpassword_label = new System.Windows.Forms.Label();
            this.txt_cpassword = new System.Windows.Forms.TextBox();
            this.text_username = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.forget_password = new System.Windows.Forms.Label();
            this.register_title = new System.Windows.Forms.Label();
            this.login_title = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.designation_null_error = new System.Windows.Forms.Label();
            this.login_error = new System.Windows.Forms.Label();
            this.username_check_error = new System.Windows.Forms.Label();
            this.passwords_error = new System.Windows.Forms.Label();
            this.username_null_error = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_label.Location = new System.Drawing.Point(26, 19);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(61, 24);
            this.name_label.TabIndex = 0;
            this.name_label.Text = "Name";
            // 
            // email_label
            // 
            this.email_label.AutoSize = true;
            this.email_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email_label.Location = new System.Drawing.Point(26, 91);
            this.email_label.Name = "email_label";
            this.email_label.Size = new System.Drawing.Size(57, 24);
            this.email_label.TabIndex = 0;
            this.email_label.Text = "Email";
            this.email_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_label.Location = new System.Drawing.Point(26, 164);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(97, 24);
            this.username_label.TabIndex = 0;
            this.username_label.Text = "Username";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_label.Location = new System.Drawing.Point(26, 234);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(92, 24);
            this.password_label.TabIndex = 0;
            this.password_label.Text = "Password";
            this.password_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.Transparent;
            this.Exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Exit.BackgroundImage")));
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Exit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Exit.Location = new System.Drawing.Point(830, 7);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(34, 29);
            this.Exit.TabIndex = 1;
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // txt_name
            // 
            this.txt_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.ForeColor = System.Drawing.Color.Gray;
            this.txt_name.Location = new System.Drawing.Point(30, 46);
            this.txt_name.Multiline = true;
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(342, 34);
            this.txt_name.TabIndex = 2;
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.ForeColor = System.Drawing.Color.Gray;
            this.txt_email.Location = new System.Drawing.Point(30, 118);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(342, 34);
            this.txt_email.TabIndex = 2;
            // 
            // txt_username
            // 
            this.txt_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_username.ForeColor = System.Drawing.Color.Gray;
            this.txt_username.Location = new System.Drawing.Point(29, 190);
            this.txt_username.Multiline = true;
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(342, 32);
            this.txt_username.TabIndex = 2;
            // 
            // txt_password
            // 
            this.txt_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.ForeColor = System.Drawing.Color.Gray;
            this.txt_password.Location = new System.Drawing.Point(30, 261);
            this.txt_password.Multiline = true;
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(342, 34);
            this.txt_password.TabIndex = 2;
            // 
            // text_password
            // 
            this.text_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_password.ForeColor = System.Drawing.Color.Gray;
            this.text_password.Location = new System.Drawing.Point(24, 119);
            this.text_password.Multiline = true;
            this.text_password.Name = "text_password";
            this.text_password.PasswordChar = '*';
            this.text_password.Size = new System.Drawing.Size(337, 34);
            this.text_password.TabIndex = 3;
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(20, 19);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(97, 24);
            this.username.TabIndex = 0;
            this.username.Text = "Username";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(25, 92);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(92, 24);
            this.password.TabIndex = 0;
            this.password.Text = "Password";
            this.password.Click += new System.EventHandler(this.label1_Click);
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.Color.MediumTurquoise;
            this.Login.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Login.FlatAppearance.BorderSize = 0;
            this.Login.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login.ForeColor = System.Drawing.Color.White;
            this.Login.Location = new System.Drawing.Point(24, 252);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(337, 43);
            this.Login.TabIndex = 4;
            this.Login.Text = "Login";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.designation);
            this.panel1.Controls.Add(this.name_label);
            this.panel1.Controls.Add(this.txt_name);
            this.panel1.Controls.Add(this.Register);
            this.panel1.Controls.Add(this.txt_email);
            this.panel1.Controls.Add(this.txt_username);
            this.panel1.Controls.Add(this.email_label);
            this.panel1.Controls.Add(this.username_label);
            this.panel1.Controls.Add(this.designation_label);
            this.panel1.Controls.Add(this.cpassword_label);
            this.panel1.Controls.Add(this.password_label);
            this.panel1.Controls.Add(this.txt_cpassword);
            this.panel1.Controls.Add(this.txt_password);
            this.panel1.Location = new System.Drawing.Point(38, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(406, 549);
            this.panel1.TabIndex = 5;
            // 
            // designation
            // 
            this.designation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.designation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.designation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.designation.FormattingEnabled = true;
            this.designation.Items.AddRange(new object[] {
            "Tester",
            "Developer"});
            this.designation.Location = new System.Drawing.Point(29, 406);
            this.designation.Name = "designation";
            this.designation.Size = new System.Drawing.Size(343, 33);
            this.designation.TabIndex = 6;
            // 
            // Register
            // 
            this.Register.BackColor = System.Drawing.Color.DodgerBlue;
            this.Register.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Register.FlatAppearance.BorderSize = 0;
            this.Register.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Register.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Register.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Register.ForeColor = System.Drawing.Color.White;
            this.Register.Location = new System.Drawing.Point(23, 478);
            this.Register.Name = "Register";
            this.Register.Size = new System.Drawing.Size(348, 43);
            this.Register.TabIndex = 4;
            this.Register.Text = "Register";
            this.Register.UseVisualStyleBackColor = false;
            this.Register.Click += new System.EventHandler(this.Register_Click);
            // 
            // designation_label
            // 
            this.designation_label.AutoSize = true;
            this.designation_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.designation_label.Location = new System.Drawing.Point(26, 379);
            this.designation_label.Name = "designation_label";
            this.designation_label.Size = new System.Drawing.Size(109, 24);
            this.designation_label.TabIndex = 0;
            this.designation_label.Text = "Designation";
            this.designation_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // cpassword_label
            // 
            this.cpassword_label.AutoSize = true;
            this.cpassword_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpassword_label.Location = new System.Drawing.Point(25, 306);
            this.cpassword_label.Name = "cpassword_label";
            this.cpassword_label.Size = new System.Drawing.Size(162, 24);
            this.cpassword_label.TabIndex = 0;
            this.cpassword_label.Text = "Confirm Password";
            this.cpassword_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // txt_cpassword
            // 
            this.txt_cpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cpassword.ForeColor = System.Drawing.Color.Gray;
            this.txt_cpassword.Location = new System.Drawing.Point(29, 333);
            this.txt_cpassword.Multiline = true;
            this.txt_cpassword.Name = "txt_cpassword";
            this.txt_cpassword.PasswordChar = '*';
            this.txt_cpassword.Size = new System.Drawing.Size(342, 34);
            this.txt_cpassword.TabIndex = 2;
            // 
            // text_username
            // 
            this.text_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_username.ForeColor = System.Drawing.Color.Gray;
            this.text_username.Location = new System.Drawing.Point(24, 46);
            this.text_username.Multiline = true;
            this.text_username.Name = "text_username";
            this.text_username.Size = new System.Drawing.Size(337, 34);
            this.text_username.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.forget_password);
            this.panel2.Controls.Add(this.username);
            this.panel2.Controls.Add(this.Login);
            this.panel2.Controls.Add(this.text_password);
            this.panel2.Controls.Add(this.text_username);
            this.panel2.Controls.Add(this.password);
            this.panel2.Location = new System.Drawing.Point(450, 106);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(394, 367);
            this.panel2.TabIndex = 6;
            // 
            // forget_password
            // 
            this.forget_password.AutoSize = true;
            this.forget_password.ForeColor = System.Drawing.Color.Blue;
            this.forget_password.Location = new System.Drawing.Point(26, 190);
            this.forget_password.Name = "forget_password";
            this.forget_password.Size = new System.Drawing.Size(196, 17);
            this.forget_password.TabIndex = 5;
            this.forget_password.Text = "Forgot Password?  Click here.";
            // 
            // register_title
            // 
            this.register_title.AutoSize = true;
            this.register_title.Font = new System.Drawing.Font("MS Reference Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.register_title.ForeColor = System.Drawing.Color.Gray;
            this.register_title.Location = new System.Drawing.Point(33, 55);
            this.register_title.Name = "register_title";
            this.register_title.Size = new System.Drawing.Size(209, 29);
            this.register_title.TabIndex = 7;
            this.register_title.Text = "Create Account";
            // 
            // login_title
            // 
            this.login_title.AutoSize = true;
            this.login_title.Font = new System.Drawing.Font("MS Reference Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_title.ForeColor = System.Drawing.Color.Gray;
            this.login_title.Location = new System.Drawing.Point(445, 56);
            this.login_title.Name = "login_title";
            this.login_title.Size = new System.Drawing.Size(82, 29);
            this.login_title.TabIndex = 7;
            this.login_title.Text = "Login";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel3.Controls.Add(this.designation_null_error);
            this.panel3.Controls.Add(this.login_error);
            this.panel3.Controls.Add(this.username_check_error);
            this.panel3.Controls.Add(this.passwords_error);
            this.panel3.Controls.Add(this.username_null_error);
            this.panel3.Location = new System.Drawing.Point(450, 512);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(394, 143);
            this.panel3.TabIndex = 8;
            // 
            // designation_null_error
            // 
            this.designation_null_error.AutoSize = true;
            this.designation_null_error.Font = new System.Drawing.Font("Bahnschrift", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.designation_null_error.ForeColor = System.Drawing.Color.Crimson;
            this.designation_null_error.Location = new System.Drawing.Point(73, 59);
            this.designation_null_error.Name = "designation_null_error";
            this.designation_null_error.Size = new System.Drawing.Size(261, 23);
            this.designation_null_error.TabIndex = 0;
            this.designation_null_error.Text = "Please select your designation";
            this.designation_null_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // login_error
            // 
            this.login_error.AutoSize = true;
            this.login_error.Font = new System.Drawing.Font("Bahnschrift", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_error.ForeColor = System.Drawing.Color.Crimson;
            this.login_error.Location = new System.Drawing.Point(70, 56);
            this.login_error.Name = "login_error";
            this.login_error.Size = new System.Drawing.Size(264, 23);
            this.login_error.TabIndex = 0;
            this.login_error.Text = "Invalid username or password!";
            this.login_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // username_check_error
            // 
            this.username_check_error.AutoSize = true;
            this.username_check_error.Font = new System.Drawing.Font("Bahnschrift", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_check_error.ForeColor = System.Drawing.Color.Crimson;
            this.username_check_error.Location = new System.Drawing.Point(92, 56);
            this.username_check_error.Name = "username_check_error";
            this.username_check_error.Size = new System.Drawing.Size(217, 23);
            this.username_check_error.TabIndex = 0;
            this.username_check_error.Text = "Username already exists!";
            this.username_check_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // passwords_error
            // 
            this.passwords_error.AutoSize = true;
            this.passwords_error.Font = new System.Drawing.Font("Bahnschrift", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwords_error.ForeColor = System.Drawing.Color.Crimson;
            this.passwords_error.Location = new System.Drawing.Point(55, 56);
            this.passwords_error.Name = "passwords_error";
            this.passwords_error.Size = new System.Drawing.Size(279, 23);
            this.passwords_error.TabIndex = 0;
            this.passwords_error.Text = "The two passwords do not match!";
            this.passwords_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // username_null_error
            // 
            this.username_null_error.AutoSize = true;
            this.username_null_error.Font = new System.Drawing.Font("Bahnschrift", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_null_error.ForeColor = System.Drawing.Color.Crimson;
            this.username_null_error.Location = new System.Drawing.Point(10, 56);
            this.username_null_error.Name = "username_null_error";
            this.username_null_error.Size = new System.Drawing.Size(351, 23);
            this.username_null_error.TabIndex = 0;
            this.username_null_error.Text = "Please fill Username and Password fields";
            this.username_null_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(877, 703);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.login_title);
            this.Controls.Add(this.register_title);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Exit);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Account";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account";
            this.Load += new System.EventHandler(this.Account_Load);
            this.Click += new System.EventHandler(this.Account_Click);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Label email_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox text_password;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox text_username;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label forget_password;
        private System.Windows.Forms.Label register_title;
        private System.Windows.Forms.Label login_title;
        private System.Windows.Forms.Label cpassword_label;
        private System.Windows.Forms.TextBox txt_cpassword;
        private System.Windows.Forms.Button Register;
        private System.Windows.Forms.ComboBox designation;
        private System.Windows.Forms.Label designation_label;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label username_null_error;
        private System.Windows.Forms.Label designation_null_error;
        private System.Windows.Forms.Label login_error;
        private System.Windows.Forms.Label passwords_error;
        private System.Windows.Forms.Label username_check_error;
    }
}

