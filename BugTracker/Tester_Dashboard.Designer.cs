﻿namespace BugTracker
{
    partial class Tester_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tester_Dashboard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menu = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.profile = new System.Windows.Forms.Button();
            this.view_reportButton = new System.Windows.Forms.Button();
            this.update_bugsButton = new System.Windows.Forms.Button();
            this.bugsButton = new System.Windows.Forms.Button();
            this.dash_heading = new System.Windows.Forms.Label();
            this.border = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Button();
            this.add_bugsPanel = new System.Windows.Forms.Panel();
            this.bug_descriptionTextbox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.session_username = new System.Windows.Forms.TextBox();
            this.assigned_byCombobox = new System.Windows.Forms.ComboBox();
            this.assigned_toCombobox = new System.Windows.Forms.ComboBox();
            this.priorityCombobox = new System.Windows.Forms.ComboBox();
            this.statusTextbox = new System.Windows.Forms.ComboBox();
            this.author_Textbox = new System.Windows.Forms.TextBox();
            this.method_Textbox = new System.Windows.Forms.TextBox();
            this.class_Textbox = new System.Windows.Forms.TextBox();
            this.project_Textbox = new System.Windows.Forms.TextBox();
            this.line_no_Textbox = new System.Windows.Forms.TextBox();
            this.bug_titleTextbox = new System.Windows.Forms.TextBox();
            this.add_bugsButton = new System.Windows.Forms.Button();
            this.delivery_date = new System.Windows.Forms.Label();
            this.assigned_by = new System.Windows.Forms.Label();
            this.assigned_to = new System.Windows.Forms.Label();
            this.priority = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.code_author = new System.Windows.Forms.Label();
            this.method = new System.Windows.Forms.Label();
            this.bugTitle = new System.Windows.Forms.Label();
            this.class_label = new System.Windows.Forms.Label();
            this.project = new System.Windows.Forms.Label();
            this.code_line = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.Label();
            this.Panel_Heading = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.git = new System.Windows.Forms.TextBox();
            this.update_bugs_Panel = new System.Windows.Forms.Panel();
            this.desc_txtbox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.assigned_combo = new System.Windows.Forms.ComboBox();
            this.priority_combo = new System.Windows.Forms.ComboBox();
            this.status_combo = new System.Windows.Forms.ComboBox();
            this.show = new System.Windows.Forms.Button();
            this.author_txtbox = new System.Windows.Forms.TextBox();
            this.method_txtbox = new System.Windows.Forms.TextBox();
            this.class_txtbox = new System.Windows.Forms.TextBox();
            this.project_txtbox = new System.Windows.Forms.TextBox();
            this.line_txtbox = new System.Windows.Forms.TextBox();
            this.id_txtbox = new System.Windows.Forms.TextBox();
            this.title_txtbox = new System.Windows.Forms.TextBox();
            this.update_bugs = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bug_id = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.view_report_Panel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.search = new System.Windows.Forms.TextBox();
            this.search_label = new System.Windows.Forms.Label();
            this.filter_label = new System.Windows.Forms.Label();
            this.filter = new System.Windows.Forms.ComboBox();
            this.reports_dataGridView = new System.Windows.Forms.DataGridView();
            this.bugIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fillDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedtoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionalinfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugAssignedbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugtitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateofdeliveryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buglinenoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.methodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeauthornameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bug_tracker_dbDataSet3 = new BugTracker.Bug_tracker_dbDataSet3();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.heading = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.bug_tracker_dbDataSet2 = new BugTracker.Bug_tracker_dbDataSet2();
            this.bugtrackerdbDataSet2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bugsTableAdapter = new BugTracker.Bug_tracker_dbDataSet3TableAdapters.BugsTableAdapter();
            this.profilePanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.profileHeader = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.text_name = new System.Windows.Forms.TextBox();
            this.upload_btn = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_username = new System.Windows.Forms.TextBox();
            this.email_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.text_password = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.add_bugsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bug_descriptionTextbox)).BeginInit();
            this.update_bugs_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.desc_txtbox)).BeginInit();
            this.view_report_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugtrackerdbDataSet2BindingSource)).BeginInit();
            this.profilePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.menu);
            this.panel1.Controls.Add(this.logout);
            this.panel1.Controls.Add(this.profile);
            this.panel1.Controls.Add(this.view_reportButton);
            this.panel1.Controls.Add(this.update_bugsButton);
            this.panel1.Controls.Add(this.bugsButton);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(12, 88);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 681);
            this.panel1.TabIndex = 2;
            // 
            // menu
            // 
            this.menu.AutoSize = true;
            this.menu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menu.Font = new System.Drawing.Font("Stencil", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.ForeColor = System.Drawing.Color.Black;
            this.menu.Location = new System.Drawing.Point(62, 23);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(124, 46);
            this.menu.TabIndex = 1;
            this.menu.Text = "MENU";
            // 
            // logout
            // 
            this.logout.BackColor = System.Drawing.Color.RosyBrown;
            this.logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.logout.FlatAppearance.BorderSize = 0;
            this.logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logout.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout.ForeColor = System.Drawing.Color.White;
            this.logout.Location = new System.Drawing.Point(21, 422);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(222, 49);
            this.logout.TabIndex = 0;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = false;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // profile
            // 
            this.profile.BackColor = System.Drawing.Color.RosyBrown;
            this.profile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.profile.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.profile.FlatAppearance.BorderSize = 0;
            this.profile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profile.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profile.ForeColor = System.Drawing.Color.White;
            this.profile.Location = new System.Drawing.Point(21, 347);
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(222, 49);
            this.profile.TabIndex = 0;
            this.profile.Text = "Profile";
            this.profile.UseVisualStyleBackColor = false;
            this.profile.Click += new System.EventHandler(this.profile_Click);
            // 
            // view_reportButton
            // 
            this.view_reportButton.BackColor = System.Drawing.Color.RosyBrown;
            this.view_reportButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_reportButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.view_reportButton.FlatAppearance.BorderSize = 0;
            this.view_reportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_reportButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_reportButton.ForeColor = System.Drawing.Color.White;
            this.view_reportButton.Location = new System.Drawing.Point(21, 272);
            this.view_reportButton.Name = "view_reportButton";
            this.view_reportButton.Size = new System.Drawing.Size(222, 49);
            this.view_reportButton.TabIndex = 0;
            this.view_reportButton.Text = "View Report";
            this.view_reportButton.UseVisualStyleBackColor = false;
            this.view_reportButton.Click += new System.EventHandler(this.view_reportButton_Click);
            // 
            // update_bugsButton
            // 
            this.update_bugsButton.BackColor = System.Drawing.Color.RosyBrown;
            this.update_bugsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_bugsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.update_bugsButton.FlatAppearance.BorderSize = 0;
            this.update_bugsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update_bugsButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_bugsButton.ForeColor = System.Drawing.Color.White;
            this.update_bugsButton.Location = new System.Drawing.Point(21, 180);
            this.update_bugsButton.Name = "update_bugsButton";
            this.update_bugsButton.Size = new System.Drawing.Size(222, 70);
            this.update_bugsButton.TabIndex = 0;
            this.update_bugsButton.Text = "Update Bug Details";
            this.update_bugsButton.UseVisualStyleBackColor = false;
            this.update_bugsButton.Click += new System.EventHandler(this.update_bugsButton_Click);
            // 
            // bugsButton
            // 
            this.bugsButton.BackColor = System.Drawing.Color.RosyBrown;
            this.bugsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bugsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bugsButton.FlatAppearance.BorderSize = 0;
            this.bugsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bugsButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugsButton.ForeColor = System.Drawing.Color.White;
            this.bugsButton.Location = new System.Drawing.Point(21, 104);
            this.bugsButton.Name = "bugsButton";
            this.bugsButton.Size = new System.Drawing.Size(222, 51);
            this.bugsButton.TabIndex = 0;
            this.bugsButton.Text = "Assign Bugs";
            this.bugsButton.UseVisualStyleBackColor = false;
            this.bugsButton.Click += new System.EventHandler(this.bugsButton_Click);
            // 
            // dash_heading
            // 
            this.dash_heading.AutoSize = true;
            this.dash_heading.BackColor = System.Drawing.Color.BurlyWood;
            this.dash_heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dash_heading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dash_heading.Location = new System.Drawing.Point(12, 34);
            this.dash_heading.Name = "dash_heading";
            this.dash_heading.Size = new System.Drawing.Size(201, 25);
            this.dash_heading.TabIndex = 3;
            this.dash_heading.Text = "Tester\'s Dashboard";
            // 
            // border
            // 
            this.border.AutoSize = true;
            this.border.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.border.Location = new System.Drawing.Point(12, 59);
            this.border.Name = "border";
            this.border.Size = new System.Drawing.Size(1064, 17);
            this.border.TabIndex = 4;
            this.border.Text = "_________________________________________________________________________________" +
    "___________________________________________________";
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("exit.BackgroundImage")));
            this.exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.exit.FlatAppearance.BorderSize = 0;
            this.exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exit.Location = new System.Drawing.Point(1044, 12);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(34, 29);
            this.exit.TabIndex = 9;
            this.exit.UseVisualStyleBackColor = false;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // add_bugsPanel
            // 
            this.add_bugsPanel.BackColor = System.Drawing.Color.White;
            this.add_bugsPanel.Controls.Add(this.bug_descriptionTextbox);
            this.add_bugsPanel.Controls.Add(this.label16);
            this.add_bugsPanel.Controls.Add(this.dateTimePicker1);
            this.add_bugsPanel.Controls.Add(this.session_username);
            this.add_bugsPanel.Controls.Add(this.assigned_byCombobox);
            this.add_bugsPanel.Controls.Add(this.assigned_toCombobox);
            this.add_bugsPanel.Controls.Add(this.priorityCombobox);
            this.add_bugsPanel.Controls.Add(this.statusTextbox);
            this.add_bugsPanel.Controls.Add(this.author_Textbox);
            this.add_bugsPanel.Controls.Add(this.method_Textbox);
            this.add_bugsPanel.Controls.Add(this.class_Textbox);
            this.add_bugsPanel.Controls.Add(this.project_Textbox);
            this.add_bugsPanel.Controls.Add(this.line_no_Textbox);
            this.add_bugsPanel.Controls.Add(this.bug_titleTextbox);
            this.add_bugsPanel.Controls.Add(this.add_bugsButton);
            this.add_bugsPanel.Controls.Add(this.delivery_date);
            this.add_bugsPanel.Controls.Add(this.assigned_by);
            this.add_bugsPanel.Controls.Add(this.assigned_to);
            this.add_bugsPanel.Controls.Add(this.priority);
            this.add_bugsPanel.Controls.Add(this.description);
            this.add_bugsPanel.Controls.Add(this.code_author);
            this.add_bugsPanel.Controls.Add(this.method);
            this.add_bugsPanel.Controls.Add(this.bugTitle);
            this.add_bugsPanel.Controls.Add(this.class_label);
            this.add_bugsPanel.Controls.Add(this.project);
            this.add_bugsPanel.Controls.Add(this.code_line);
            this.add_bugsPanel.Controls.Add(this.email);
            this.add_bugsPanel.Controls.Add(this.Panel_Heading);
            this.add_bugsPanel.Controls.Add(this.label3);
            this.add_bugsPanel.Controls.Add(this.git);
            this.add_bugsPanel.ForeColor = System.Drawing.Color.DimGray;
            this.add_bugsPanel.Location = new System.Drawing.Point(294, 88);
            this.add_bugsPanel.Name = "add_bugsPanel";
            this.add_bugsPanel.Size = new System.Drawing.Size(782, 681);
            this.add_bugsPanel.TabIndex = 10;
            // 
            // bug_descriptionTextbox
            // 
            this.bug_descriptionTextbox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.bug_descriptionTextbox.AutoScrollMinSize = new System.Drawing.Size(181, 18);
            this.bug_descriptionTextbox.BackBrush = null;
            this.bug_descriptionTextbox.BackColor = System.Drawing.Color.Silver;
            this.bug_descriptionTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bug_descriptionTextbox.CharHeight = 18;
            this.bug_descriptionTextbox.CharWidth = 10;
            this.bug_descriptionTextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bug_descriptionTextbox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.bug_descriptionTextbox.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.bug_descriptionTextbox.ForeColor = System.Drawing.Color.White;
            this.bug_descriptionTextbox.IsReplaceMode = false;
            this.bug_descriptionTextbox.Location = new System.Drawing.Point(404, 429);
            this.bug_descriptionTextbox.Name = "bug_descriptionTextbox";
            this.bug_descriptionTextbox.Paddings = new System.Windows.Forms.Padding(0);
            this.bug_descriptionTextbox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.bug_descriptionTextbox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("bug_descriptionTextbox.ServiceColors")));
            this.bug_descriptionTextbox.Size = new System.Drawing.Size(366, 230);
            this.bug_descriptionTextbox.TabIndex = 15;
            this.bug_descriptionTextbox.Text = "Enter your code";
            this.bug_descriptionTextbox.Zoom = 100;
            this.bug_descriptionTextbox.Load += new System.EventHandler(this.bug_descTextbox_Load_1);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 534);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 24);
            this.label16.TabIndex = 13;
            this.label16.Text = "Git Link";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(26, 500);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(354, 22);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // session_username
            // 
            this.session_username.BackColor = System.Drawing.Color.White;
            this.session_username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.session_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.session_username.ForeColor = System.Drawing.Color.Maroon;
            this.session_username.Location = new System.Drawing.Point(603, 0);
            this.session_username.Multiline = true;
            this.session_username.Name = "session_username";
            this.session_username.Size = new System.Drawing.Size(261, 28);
            this.session_username.TabIndex = 11;
            // 
            // assigned_byCombobox
            // 
            this.assigned_byCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.assigned_byCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.assigned_byCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_byCombobox.ForeColor = System.Drawing.Color.Gray;
            this.assigned_byCombobox.FormattingEnabled = true;
            this.assigned_byCombobox.Location = new System.Drawing.Point(26, 430);
            this.assigned_byCombobox.Name = "assigned_byCombobox";
            this.assigned_byCombobox.Size = new System.Drawing.Size(354, 33);
            this.assigned_byCombobox.TabIndex = 10;
            // 
            // assigned_toCombobox
            // 
            this.assigned_toCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.assigned_toCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.assigned_toCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_toCombobox.ForeColor = System.Drawing.Color.Gray;
            this.assigned_toCombobox.FormattingEnabled = true;
            this.assigned_toCombobox.Location = new System.Drawing.Point(26, 361);
            this.assigned_toCombobox.Name = "assigned_toCombobox";
            this.assigned_toCombobox.Size = new System.Drawing.Size(354, 33);
            this.assigned_toCombobox.TabIndex = 10;
            // 
            // priorityCombobox
            // 
            this.priorityCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.priorityCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.priorityCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priorityCombobox.ForeColor = System.Drawing.Color.Gray;
            this.priorityCombobox.FormattingEnabled = true;
            this.priorityCombobox.Items.AddRange(new object[] {
            "High",
            "Medium",
            "Low"});
            this.priorityCombobox.Location = new System.Drawing.Point(26, 288);
            this.priorityCombobox.Name = "priorityCombobox";
            this.priorityCombobox.Size = new System.Drawing.Size(354, 33);
            this.priorityCombobox.TabIndex = 10;
            // 
            // statusTextbox
            // 
            this.statusTextbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusTextbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusTextbox.FormattingEnabled = true;
            this.statusTextbox.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Fixed"});
            this.statusTextbox.Location = new System.Drawing.Point(26, 137);
            this.statusTextbox.Name = "statusTextbox";
            this.statusTextbox.Size = new System.Drawing.Size(354, 33);
            this.statusTextbox.TabIndex = 10;
            // 
            // author_Textbox
            // 
            this.author_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.author_Textbox.Location = new System.Drawing.Point(406, 358);
            this.author_Textbox.Multiline = true;
            this.author_Textbox.Name = "author_Textbox";
            this.author_Textbox.Size = new System.Drawing.Size(364, 36);
            this.author_Textbox.TabIndex = 1;
            // 
            // method_Textbox
            // 
            this.method_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.method_Textbox.Location = new System.Drawing.Point(406, 285);
            this.method_Textbox.Multiline = true;
            this.method_Textbox.Name = "method_Textbox";
            this.method_Textbox.Size = new System.Drawing.Size(364, 36);
            this.method_Textbox.TabIndex = 1;
            // 
            // class_Textbox
            // 
            this.class_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.class_Textbox.Location = new System.Drawing.Point(406, 216);
            this.class_Textbox.Multiline = true;
            this.class_Textbox.Name = "class_Textbox";
            this.class_Textbox.Size = new System.Drawing.Size(364, 36);
            this.class_Textbox.TabIndex = 1;
            // 
            // project_Textbox
            // 
            this.project_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project_Textbox.Location = new System.Drawing.Point(591, 132);
            this.project_Textbox.Multiline = true;
            this.project_Textbox.Name = "project_Textbox";
            this.project_Textbox.Size = new System.Drawing.Size(179, 36);
            this.project_Textbox.TabIndex = 1;
            // 
            // line_no_Textbox
            // 
            this.line_no_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_no_Textbox.Location = new System.Drawing.Point(406, 132);
            this.line_no_Textbox.Multiline = true;
            this.line_no_Textbox.Name = "line_no_Textbox";
            this.line_no_Textbox.Size = new System.Drawing.Size(179, 36);
            this.line_no_Textbox.TabIndex = 1;
            // 
            // bug_titleTextbox
            // 
            this.bug_titleTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bug_titleTextbox.Location = new System.Drawing.Point(26, 216);
            this.bug_titleTextbox.Multiline = true;
            this.bug_titleTextbox.Name = "bug_titleTextbox";
            this.bug_titleTextbox.Size = new System.Drawing.Size(354, 36);
            this.bug_titleTextbox.TabIndex = 1;
            // 
            // add_bugsButton
            // 
            this.add_bugsButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.add_bugsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add_bugsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.add_bugsButton.FlatAppearance.BorderSize = 0;
            this.add_bugsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add_bugsButton.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add_bugsButton.ForeColor = System.Drawing.Color.White;
            this.add_bugsButton.Location = new System.Drawing.Point(26, 621);
            this.add_bugsButton.Name = "add_bugsButton";
            this.add_bugsButton.Size = new System.Drawing.Size(354, 39);
            this.add_bugsButton.TabIndex = 0;
            this.add_bugsButton.Text = "Add Bugs";
            this.add_bugsButton.UseVisualStyleBackColor = false;
            this.add_bugsButton.Click += new System.EventHandler(this.add_bugsButton_Click);
            // 
            // delivery_date
            // 
            this.delivery_date.AutoSize = true;
            this.delivery_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delivery_date.Location = new System.Drawing.Point(22, 473);
            this.delivery_date.Name = "delivery_date";
            this.delivery_date.Size = new System.Drawing.Size(156, 24);
            this.delivery_date.TabIndex = 0;
            this.delivery_date.Text = "Date of Delivery";
            // 
            // assigned_by
            // 
            this.assigned_by.AutoSize = true;
            this.assigned_by.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_by.Location = new System.Drawing.Point(22, 403);
            this.assigned_by.Name = "assigned_by";
            this.assigned_by.Size = new System.Drawing.Size(169, 24);
            this.assigned_by.TabIndex = 0;
            this.assigned_by.Text = "Bug Assigned By";
            // 
            // assigned_to
            // 
            this.assigned_to.AutoSize = true;
            this.assigned_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_to.Location = new System.Drawing.Point(22, 333);
            this.assigned_to.Name = "assigned_to";
            this.assigned_to.Size = new System.Drawing.Size(163, 24);
            this.assigned_to.TabIndex = 0;
            this.assigned_to.Text = "Bug Assigned to";
            // 
            // priority
            // 
            this.priority.AutoSize = true;
            this.priority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priority.Location = new System.Drawing.Point(22, 261);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(74, 24);
            this.priority.TabIndex = 0;
            this.priority.Text = "Priority";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.Location = new System.Drawing.Point(402, 403);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(164, 24);
            this.description.TabIndex = 0;
            this.description.Text = "Bug / Comments";
            // 
            // code_author
            // 
            this.code_author.AutoSize = true;
            this.code_author.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code_author.Location = new System.Drawing.Point(405, 333);
            this.code_author.Name = "code_author";
            this.code_author.Size = new System.Drawing.Size(128, 24);
            this.code_author.TabIndex = 0;
            this.code_author.Text = "Code Author";
            // 
            // method
            // 
            this.method.AutoSize = true;
            this.method.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.method.Location = new System.Drawing.Point(405, 261);
            this.method.Name = "method";
            this.method.Size = new System.Drawing.Size(80, 24);
            this.method.TabIndex = 0;
            this.method.Text = "Method";
            // 
            // bugTitle
            // 
            this.bugTitle.AutoSize = true;
            this.bugTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugTitle.Location = new System.Drawing.Point(22, 187);
            this.bugTitle.Name = "bugTitle";
            this.bugTitle.Size = new System.Drawing.Size(93, 24);
            this.bugTitle.TabIndex = 0;
            this.bugTitle.Text = "Bug Title";
            // 
            // class_label
            // 
            this.class_label.AutoSize = true;
            this.class_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.class_label.Location = new System.Drawing.Point(405, 187);
            this.class_label.Name = "class_label";
            this.class_label.Size = new System.Drawing.Size(60, 24);
            this.class_label.TabIndex = 0;
            this.class_label.Text = "Class";
            // 
            // project
            // 
            this.project.AutoSize = true;
            this.project.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project.Location = new System.Drawing.Point(587, 104);
            this.project.Name = "project";
            this.project.Size = new System.Drawing.Size(75, 24);
            this.project.TabIndex = 0;
            this.project.Text = "Project";
            // 
            // code_line
            // 
            this.code_line.AutoSize = true;
            this.code_line.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code_line.Location = new System.Drawing.Point(405, 104);
            this.code_line.Name = "code_line";
            this.code_line.Size = new System.Drawing.Size(132, 24);
            this.code_line.TabIndex = 0;
            this.code_line.Text = "Bug Line No.";
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email.Location = new System.Drawing.Point(22, 104);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(66, 24);
            this.email.TabIndex = 0;
            this.email.Text = "Status";
            // 
            // Panel_Heading
            // 
            this.Panel_Heading.AutoSize = true;
            this.Panel_Heading.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Panel_Heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Heading.ForeColor = System.Drawing.Color.DimGray;
            this.Panel_Heading.Location = new System.Drawing.Point(30, 23);
            this.Panel_Heading.Name = "Panel_Heading";
            this.Panel_Heading.Size = new System.Drawing.Size(155, 36);
            this.Panel_Heading.TabIndex = 0;
            this.Panel_Heading.Text = "Add Bugs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(22, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(725, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "_________________________________________________________________";
            // 
            // git
            // 
            this.git.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.git.Location = new System.Drawing.Point(26, 563);
            this.git.Multiline = true;
            this.git.Name = "git";
            this.git.Size = new System.Drawing.Size(354, 36);
            this.git.TabIndex = 14;
            // 
            // update_bugs_Panel
            // 
            this.update_bugs_Panel.BackColor = System.Drawing.Color.White;
            this.update_bugs_Panel.Controls.Add(this.desc_txtbox);
            this.update_bugs_Panel.Controls.Add(this.textBox1);
            this.update_bugs_Panel.Controls.Add(this.assigned_combo);
            this.update_bugs_Panel.Controls.Add(this.priority_combo);
            this.update_bugs_Panel.Controls.Add(this.status_combo);
            this.update_bugs_Panel.Controls.Add(this.show);
            this.update_bugs_Panel.Controls.Add(this.author_txtbox);
            this.update_bugs_Panel.Controls.Add(this.method_txtbox);
            this.update_bugs_Panel.Controls.Add(this.class_txtbox);
            this.update_bugs_Panel.Controls.Add(this.project_txtbox);
            this.update_bugs_Panel.Controls.Add(this.line_txtbox);
            this.update_bugs_Panel.Controls.Add(this.id_txtbox);
            this.update_bugs_Panel.Controls.Add(this.title_txtbox);
            this.update_bugs_Panel.Controls.Add(this.update_bugs);
            this.update_bugs_Panel.Controls.Add(this.label1);
            this.update_bugs_Panel.Controls.Add(this.bug_id);
            this.update_bugs_Panel.Controls.Add(this.label4);
            this.update_bugs_Panel.Controls.Add(this.label5);
            this.update_bugs_Panel.Controls.Add(this.label6);
            this.update_bugs_Panel.Controls.Add(this.label7);
            this.update_bugs_Panel.Controls.Add(this.label8);
            this.update_bugs_Panel.Controls.Add(this.label9);
            this.update_bugs_Panel.Controls.Add(this.label10);
            this.update_bugs_Panel.Controls.Add(this.label11);
            this.update_bugs_Panel.Controls.Add(this.label12);
            this.update_bugs_Panel.Controls.Add(this.label13);
            this.update_bugs_Panel.Controls.Add(this.label14);
            this.update_bugs_Panel.Controls.Add(this.label15);
            this.update_bugs_Panel.Controls.Add(this.dateTimePicker2);
            this.update_bugs_Panel.ForeColor = System.Drawing.Color.DimGray;
            this.update_bugs_Panel.Location = new System.Drawing.Point(289, 91);
            this.update_bugs_Panel.Name = "update_bugs_Panel";
            this.update_bugs_Panel.Size = new System.Drawing.Size(785, 681);
            this.update_bugs_Panel.TabIndex = 11;
            // 
            // desc_txtbox
            // 
            this.desc_txtbox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.desc_txtbox.AutoScrollMinSize = new System.Drawing.Size(181, 18);
            this.desc_txtbox.BackBrush = null;
            this.desc_txtbox.BackColor = System.Drawing.Color.Silver;
            this.desc_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.desc_txtbox.CharHeight = 18;
            this.desc_txtbox.CharWidth = 10;
            this.desc_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.desc_txtbox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.desc_txtbox.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.desc_txtbox.ForeColor = System.Drawing.Color.White;
            this.desc_txtbox.IsReplaceMode = false;
            this.desc_txtbox.Location = new System.Drawing.Point(406, 451);
            this.desc_txtbox.Name = "desc_txtbox";
            this.desc_txtbox.Paddings = new System.Windows.Forms.Padding(0);
            this.desc_txtbox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.desc_txtbox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("desc_txtbox.ServiceColors")));
            this.desc_txtbox.Size = new System.Drawing.Size(366, 175);
            this.desc_txtbox.TabIndex = 14;
            this.desc_txtbox.Text = "Enter your code";
            this.desc_txtbox.Zoom = 100;
            this.desc_txtbox.Load += new System.EventHandler(this.desc_txtbox_Load);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Maroon;
            this.textBox1.Location = new System.Drawing.Point(603, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(261, 28);
            this.textBox1.TabIndex = 11;
            // 
            // assigned_combo
            // 
            this.assigned_combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.assigned_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.assigned_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_combo.ForeColor = System.Drawing.Color.Gray;
            this.assigned_combo.FormattingEnabled = true;
            this.assigned_combo.Location = new System.Drawing.Point(23, 451);
            this.assigned_combo.Name = "assigned_combo";
            this.assigned_combo.Size = new System.Drawing.Size(354, 33);
            this.assigned_combo.TabIndex = 10;
            // 
            // priority_combo
            // 
            this.priority_combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.priority_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.priority_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priority_combo.ForeColor = System.Drawing.Color.Gray;
            this.priority_combo.FormattingEnabled = true;
            this.priority_combo.Items.AddRange(new object[] {
            "High",
            "Medium",
            "Low"});
            this.priority_combo.Location = new System.Drawing.Point(23, 378);
            this.priority_combo.Name = "priority_combo";
            this.priority_combo.Size = new System.Drawing.Size(354, 33);
            this.priority_combo.TabIndex = 10;
            // 
            // status_combo
            // 
            this.status_combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.status_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.status_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status_combo.FormattingEnabled = true;
            this.status_combo.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Fixed"});
            this.status_combo.Location = new System.Drawing.Point(23, 227);
            this.status_combo.Name = "status_combo";
            this.status_combo.Size = new System.Drawing.Size(354, 33);
            this.status_combo.TabIndex = 10;
            // 
            // show
            // 
            this.show.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.show.Cursor = System.Windows.Forms.Cursors.Hand;
            this.show.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.show.FlatAppearance.BorderSize = 0;
            this.show.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.show.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.show.ForeColor = System.Drawing.Color.Black;
            this.show.Location = new System.Drawing.Point(239, 120);
            this.show.Name = "show";
            this.show.Size = new System.Drawing.Size(138, 24);
            this.show.TabIndex = 0;
            this.show.Text = "Show value";
            this.show.UseVisualStyleBackColor = false;
            this.show.Click += new System.EventHandler(this.show_Click);
            // 
            // author_txtbox
            // 
            this.author_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.author_txtbox.Location = new System.Drawing.Point(406, 374);
            this.author_txtbox.Multiline = true;
            this.author_txtbox.Name = "author_txtbox";
            this.author_txtbox.Size = new System.Drawing.Size(364, 36);
            this.author_txtbox.TabIndex = 1;
            // 
            // method_txtbox
            // 
            this.method_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.method_txtbox.Location = new System.Drawing.Point(406, 301);
            this.method_txtbox.Multiline = true;
            this.method_txtbox.Name = "method_txtbox";
            this.method_txtbox.Size = new System.Drawing.Size(364, 36);
            this.method_txtbox.TabIndex = 1;
            // 
            // class_txtbox
            // 
            this.class_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.class_txtbox.Location = new System.Drawing.Point(406, 232);
            this.class_txtbox.Multiline = true;
            this.class_txtbox.Name = "class_txtbox";
            this.class_txtbox.Size = new System.Drawing.Size(364, 36);
            this.class_txtbox.TabIndex = 1;
            // 
            // project_txtbox
            // 
            this.project_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project_txtbox.Location = new System.Drawing.Point(591, 148);
            this.project_txtbox.Multiline = true;
            this.project_txtbox.Name = "project_txtbox";
            this.project_txtbox.Size = new System.Drawing.Size(179, 36);
            this.project_txtbox.TabIndex = 1;
            // 
            // line_txtbox
            // 
            this.line_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_txtbox.Location = new System.Drawing.Point(406, 148);
            this.line_txtbox.Multiline = true;
            this.line_txtbox.Name = "line_txtbox";
            this.line_txtbox.Size = new System.Drawing.Size(179, 36);
            this.line_txtbox.TabIndex = 1;
            // 
            // id_txtbox
            // 
            this.id_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id_txtbox.Location = new System.Drawing.Point(23, 146);
            this.id_txtbox.Multiline = true;
            this.id_txtbox.Name = "id_txtbox";
            this.id_txtbox.Size = new System.Drawing.Size(354, 36);
            this.id_txtbox.TabIndex = 1;
            // 
            // title_txtbox
            // 
            this.title_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title_txtbox.Location = new System.Drawing.Point(23, 306);
            this.title_txtbox.Multiline = true;
            this.title_txtbox.Name = "title_txtbox";
            this.title_txtbox.Size = new System.Drawing.Size(354, 36);
            this.title_txtbox.TabIndex = 1;
            // 
            // update_bugs
            // 
            this.update_bugs.BackColor = System.Drawing.Color.DodgerBlue;
            this.update_bugs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_bugs.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.update_bugs.FlatAppearance.BorderSize = 0;
            this.update_bugs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update_bugs.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_bugs.ForeColor = System.Drawing.Color.White;
            this.update_bugs.Location = new System.Drawing.Point(26, 592);
            this.update_bugs.Name = "update_bugs";
            this.update_bugs.Size = new System.Drawing.Size(354, 39);
            this.update_bugs.TabIndex = 0;
            this.update_bugs.Text = "Update Bugs";
            this.update_bugs.UseVisualStyleBackColor = false;
            this.update_bugs.Click += new System.EventHandler(this.update_bugs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 494);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date of Delivery";
            // 
            // bug_id
            // 
            this.bug_id.AutoSize = true;
            this.bug_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bug_id.Location = new System.Drawing.Point(22, 120);
            this.bug_id.Name = "bug_id";
            this.bug_id.Size = new System.Drawing.Size(72, 24);
            this.bug_id.TabIndex = 0;
            this.bug_id.Text = "Bug ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 423);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Bug Assigned to";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 351);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Priority";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(402, 419);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Bug";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(405, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Code Author";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(405, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Method";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bug Title";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(405, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 24);
            this.label10.TabIndex = 0;
            this.label10.Text = "Class";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(599, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 24);
            this.label11.TabIndex = 0;
            this.label11.Text = "Project";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(405, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Bug Line No.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(19, 194);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 24);
            this.label13.TabIndex = 0;
            this.label13.Text = "Status";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DimGray;
            this.label14.Location = new System.Drawing.Point(30, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(282, 36);
            this.label14.TabIndex = 0;
            this.label14.Text = "Update Bug Status";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Silver;
            this.label15.Location = new System.Drawing.Point(22, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(725, 24);
            this.label15.TabIndex = 9;
            this.label15.Text = "_________________________________________________________________";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(26, 530);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(354, 22);
            this.dateTimePicker2.TabIndex = 12;
            // 
            // view_report_Panel
            // 
            this.view_report_Panel.BackColor = System.Drawing.Color.White;
            this.view_report_Panel.Controls.Add(this.button1);
            this.view_report_Panel.Controls.Add(this.search);
            this.view_report_Panel.Controls.Add(this.search_label);
            this.view_report_Panel.Controls.Add(this.filter_label);
            this.view_report_Panel.Controls.Add(this.filter);
            this.view_report_Panel.Controls.Add(this.reports_dataGridView);
            this.view_report_Panel.Controls.Add(this.textBox2);
            this.view_report_Panel.Controls.Add(this.heading);
            this.view_report_Panel.Controls.Add(this.label2);
            this.view_report_Panel.Location = new System.Drawing.Point(287, 87);
            this.view_report_Panel.Name = "view_report_Panel";
            this.view_report_Panel.Size = new System.Drawing.Size(796, 678);
            this.view_report_Panel.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(320, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 42);
            this.button1.TabIndex = 17;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.search_button_Click);
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.ForeColor = System.Drawing.Color.Gray;
            this.search.Location = new System.Drawing.Point(26, 146);
            this.search.Multiline = true;
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(288, 33);
            this.search.TabIndex = 16;
            // 
            // search_label
            // 
            this.search_label.AutoSize = true;
            this.search_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_label.ForeColor = System.Drawing.Color.Gray;
            this.search_label.Location = new System.Drawing.Point(21, 118);
            this.search_label.Name = "search_label";
            this.search_label.Size = new System.Drawing.Size(75, 25);
            this.search_label.TabIndex = 15;
            this.search_label.Text = "Search";
            this.search_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // filter_label
            // 
            this.filter_label.AutoSize = true;
            this.filter_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter_label.ForeColor = System.Drawing.Color.Gray;
            this.filter_label.Location = new System.Drawing.Point(404, 119);
            this.filter_label.Name = "filter_label";
            this.filter_label.Size = new System.Drawing.Size(54, 25);
            this.filter_label.TabIndex = 13;
            this.filter_label.Text = "Filter";
            // 
            // filter
            // 
            this.filter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.filter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter.ForeColor = System.Drawing.Color.Gray;
            this.filter.FormattingEnabled = true;
            this.filter.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.filter.Location = new System.Drawing.Point(409, 147);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(354, 33);
            this.filter.TabIndex = 12;
            this.filter.SelectedIndexChanged += new System.EventHandler(this.filter_SelectedIndexChanged);
            // 
            // reports_dataGridView
            // 
            this.reports_dataGridView.AutoGenerateColumns = false;
            this.reports_dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.reports_dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.reports_dataGridView.ColumnHeadersHeight = 43;
            this.reports_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bugIDDataGridViewTextBoxColumn,
            this.fillDateDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.priorityDataGridViewTextBoxColumn,
            this.assignedtoDataGridViewTextBoxColumn,
            this.additionalinfoDataGridViewTextBoxColumn,
            this.bugAssignedbyDataGridViewTextBoxColumn,
            this.bugtitleDataGridViewTextBoxColumn,
            this.dateofdeliveryDataGridViewTextBoxColumn,
            this.buglinenoDataGridViewTextBoxColumn,
            this.methodDataGridViewTextBoxColumn,
            this.classDataGridViewTextBoxColumn,
            this.projectDataGridViewTextBoxColumn,
            this.codeauthornameDataGridViewTextBoxColumn,
            this.filesDataGridViewTextBoxColumn});
            this.reports_dataGridView.DataSource = this.bugsBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reports_dataGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.reports_dataGridView.EnableHeadersVisualStyles = false;
            this.reports_dataGridView.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.Location = new System.Drawing.Point(23, 251);
            this.reports_dataGridView.Name = "reports_dataGridView";
            this.reports_dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.reports_dataGridView.RowHeadersVisible = false;
            this.reports_dataGridView.RowHeadersWidth = 50;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(215)))), ((int)(((byte)(208)))));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.reports_dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.reports_dataGridView.RowTemplate.Height = 24;
            this.reports_dataGridView.Size = new System.Drawing.Size(747, 411);
            this.reports_dataGridView.TabIndex = 11;
            // 
            // bugIDDataGridViewTextBoxColumn
            // 
            this.bugIDDataGridViewTextBoxColumn.DataPropertyName = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn.HeaderText = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn.Name = "bugIDDataGridViewTextBoxColumn";
            this.bugIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fillDateDataGridViewTextBoxColumn
            // 
            this.fillDateDataGridViewTextBoxColumn.DataPropertyName = "FillDate";
            this.fillDateDataGridViewTextBoxColumn.HeaderText = "FillDate";
            this.fillDateDataGridViewTextBoxColumn.Name = "fillDateDataGridViewTextBoxColumn";
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            // 
            // priorityDataGridViewTextBoxColumn
            // 
            this.priorityDataGridViewTextBoxColumn.DataPropertyName = "Priority";
            this.priorityDataGridViewTextBoxColumn.HeaderText = "Priority";
            this.priorityDataGridViewTextBoxColumn.Name = "priorityDataGridViewTextBoxColumn";
            // 
            // assignedtoDataGridViewTextBoxColumn
            // 
            this.assignedtoDataGridViewTextBoxColumn.DataPropertyName = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.HeaderText = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.Name = "assignedtoDataGridViewTextBoxColumn";
            // 
            // additionalinfoDataGridViewTextBoxColumn
            // 
            this.additionalinfoDataGridViewTextBoxColumn.DataPropertyName = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn.HeaderText = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn.Name = "additionalinfoDataGridViewTextBoxColumn";
            // 
            // bugAssignedbyDataGridViewTextBoxColumn
            // 
            this.bugAssignedbyDataGridViewTextBoxColumn.DataPropertyName = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn.HeaderText = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn.Name = "bugAssignedbyDataGridViewTextBoxColumn";
            // 
            // bugtitleDataGridViewTextBoxColumn
            // 
            this.bugtitleDataGridViewTextBoxColumn.DataPropertyName = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn.HeaderText = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn.Name = "bugtitleDataGridViewTextBoxColumn";
            // 
            // dateofdeliveryDataGridViewTextBoxColumn
            // 
            this.dateofdeliveryDataGridViewTextBoxColumn.DataPropertyName = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn.HeaderText = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn.Name = "dateofdeliveryDataGridViewTextBoxColumn";
            // 
            // buglinenoDataGridViewTextBoxColumn
            // 
            this.buglinenoDataGridViewTextBoxColumn.DataPropertyName = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn.HeaderText = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn.Name = "buglinenoDataGridViewTextBoxColumn";
            // 
            // methodDataGridViewTextBoxColumn
            // 
            this.methodDataGridViewTextBoxColumn.DataPropertyName = "method";
            this.methodDataGridViewTextBoxColumn.HeaderText = "method";
            this.methodDataGridViewTextBoxColumn.Name = "methodDataGridViewTextBoxColumn";
            // 
            // classDataGridViewTextBoxColumn
            // 
            this.classDataGridViewTextBoxColumn.DataPropertyName = "Class";
            this.classDataGridViewTextBoxColumn.HeaderText = "Class";
            this.classDataGridViewTextBoxColumn.Name = "classDataGridViewTextBoxColumn";
            // 
            // projectDataGridViewTextBoxColumn
            // 
            this.projectDataGridViewTextBoxColumn.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn.Name = "projectDataGridViewTextBoxColumn";
            // 
            // codeauthornameDataGridViewTextBoxColumn
            // 
            this.codeauthornameDataGridViewTextBoxColumn.DataPropertyName = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn.HeaderText = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn.Name = "codeauthornameDataGridViewTextBoxColumn";
            // 
            // filesDataGridViewTextBoxColumn
            // 
            this.filesDataGridViewTextBoxColumn.DataPropertyName = "Files";
            this.filesDataGridViewTextBoxColumn.HeaderText = "Files";
            this.filesDataGridViewTextBoxColumn.Name = "filesDataGridViewTextBoxColumn";
            // 
            // bugsBindingSource
            // 
            this.bugsBindingSource.DataMember = "Bugs";
            this.bugsBindingSource.DataSource = this.bug_tracker_dbDataSet3;
            // 
            // bug_tracker_dbDataSet3
            // 
            this.bug_tracker_dbDataSet3.DataSetName = "Bug_tracker_dbDataSet3";
            this.bug_tracker_dbDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Maroon;
            this.textBox2.Location = new System.Drawing.Point(521, 3);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(261, 28);
            this.textBox2.TabIndex = 10;
            // 
            // heading
            // 
            this.heading.AutoSize = true;
            this.heading.BackColor = System.Drawing.Color.Transparent;
            this.heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heading.ForeColor = System.Drawing.Color.DimGray;
            this.heading.Location = new System.Drawing.Point(27, 28);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(111, 36);
            this.heading.TabIndex = 7;
            this.heading.Text = "Report";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(725, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "_________________________________________________________________";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.Maroon;
            this.textBox3.Location = new System.Drawing.Point(940, 47);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(261, 28);
            this.textBox3.TabIndex = 14;
            // 
            // bug_tracker_dbDataSet2
            // 
            this.bug_tracker_dbDataSet2.DataSetName = "Bug_tracker_dbDataSet2";
            this.bug_tracker_dbDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bugtrackerdbDataSet2BindingSource
            // 
            this.bugtrackerdbDataSet2BindingSource.DataSource = this.bug_tracker_dbDataSet2;
            this.bugtrackerdbDataSet2BindingSource.Position = 0;
            // 
            // bugsTableAdapter
            // 
            this.bugsTableAdapter.ClearBeforeFill = true;
            // 
            // profilePanel
            // 
            this.profilePanel.BackColor = System.Drawing.Color.White;
            this.profilePanel.Controls.Add(this.pictureBox1);
            this.profilePanel.Controls.Add(this.profileHeader);
            this.profilePanel.Controls.Add(this.name_label);
            this.profilePanel.Controls.Add(this.text_name);
            this.profilePanel.Controls.Add(this.upload_btn);
            this.profilePanel.Controls.Add(this.Update);
            this.profilePanel.Controls.Add(this.text_email);
            this.profilePanel.Controls.Add(this.text_username);
            this.profilePanel.Controls.Add(this.email_label);
            this.profilePanel.Controls.Add(this.username_label);
            this.profilePanel.Controls.Add(this.password_label);
            this.profilePanel.Controls.Add(this.text_password);
            this.profilePanel.Controls.Add(this.label20);
            this.profilePanel.Location = new System.Drawing.Point(286, 86);
            this.profilePanel.Name = "profilePanel";
            this.profilePanel.Size = new System.Drawing.Size(793, 678);
            this.profilePanel.TabIndex = 15;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.Location = new System.Drawing.Point(463, 175);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 250);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // profileHeader
            // 
            this.profileHeader.AutoSize = true;
            this.profileHeader.BackColor = System.Drawing.Color.Transparent;
            this.profileHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profileHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHeader.ForeColor = System.Drawing.Color.DimGray;
            this.profileHeader.Location = new System.Drawing.Point(41, 28);
            this.profileHeader.Name = "profileHeader";
            this.profileHeader.Size = new System.Drawing.Size(158, 36);
            this.profileHeader.TabIndex = 7;
            this.profileHeader.Text = "My Profile";
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_label.Location = new System.Drawing.Point(44, 149);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(61, 24);
            this.name_label.TabIndex = 0;
            this.name_label.Text = "Name";
            // 
            // text_name
            // 
            this.text_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_name.ForeColor = System.Drawing.Color.Gray;
            this.text_name.Location = new System.Drawing.Point(48, 176);
            this.text_name.Multiline = true;
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(342, 34);
            this.text_name.TabIndex = 2;
            // 
            // upload_btn
            // 
            this.upload_btn.BackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.upload_btn.FlatAppearance.BorderSize = 0;
            this.upload_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.upload_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upload_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upload_btn.ForeColor = System.Drawing.Color.White;
            this.upload_btn.Location = new System.Drawing.Point(512, 475);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(167, 43);
            this.upload_btn.TabIndex = 4;
            this.upload_btn.Text = "Upload";
            this.upload_btn.UseVisualStyleBackColor = false;
            this.upload_btn.Click += new System.EventHandler(this.upload_btn_Click);
            // 
            // Update
            // 
            this.Update.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Update.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Update.FlatAppearance.BorderSize = 0;
            this.Update.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Update.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update.ForeColor = System.Drawing.Color.White;
            this.Update.Location = new System.Drawing.Point(48, 475);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(348, 43);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = false;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // text_email
            // 
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.ForeColor = System.Drawing.Color.Gray;
            this.text_email.Location = new System.Drawing.Point(48, 248);
            this.text_email.Multiline = true;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(342, 34);
            this.text_email.TabIndex = 2;
            // 
            // text_username
            // 
            this.text_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_username.ForeColor = System.Drawing.Color.Gray;
            this.text_username.Location = new System.Drawing.Point(47, 320);
            this.text_username.Multiline = true;
            this.text_username.Name = "text_username";
            this.text_username.Size = new System.Drawing.Size(342, 32);
            this.text_username.TabIndex = 2;
            // 
            // email_label
            // 
            this.email_label.AutoSize = true;
            this.email_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email_label.Location = new System.Drawing.Point(44, 221);
            this.email_label.Name = "email_label";
            this.email_label.Size = new System.Drawing.Size(57, 24);
            this.email_label.TabIndex = 0;
            this.email_label.Text = "Email";
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_label.Location = new System.Drawing.Point(44, 294);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(97, 24);
            this.username_label.TabIndex = 0;
            this.username_label.Text = "Username";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_label.Location = new System.Drawing.Point(44, 364);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(92, 24);
            this.password_label.TabIndex = 0;
            this.password_label.Text = "Password";
            // 
            // text_password
            // 
            this.text_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_password.ForeColor = System.Drawing.Color.Gray;
            this.text_password.Location = new System.Drawing.Point(48, 391);
            this.text_password.Multiline = true;
            this.text_password.Name = "text_password";
            this.text_password.Size = new System.Drawing.Size(342, 34);
            this.text_password.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Silver;
            this.label20.Location = new System.Drawing.Point(29, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(725, 24);
            this.label20.TabIndex = 8;
            this.label20.Text = "_________________________________________________________________";
            // 
            // Tester_Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 781);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.border);
            this.Controls.Add(this.dash_heading);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.add_bugsPanel);
            this.Controls.Add(this.profilePanel);
            this.Controls.Add(this.view_report_Panel);
            this.Controls.Add(this.update_bugs_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Tester_Dashboard";
            this.Text = "Tester_Dashboard";
            this.Load += new System.EventHandler(this.Tester_Dashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.add_bugsPanel.ResumeLayout(false);
            this.add_bugsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bug_descriptionTextbox)).EndInit();
            this.update_bugs_Panel.ResumeLayout(false);
            this.update_bugs_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.desc_txtbox)).EndInit();
            this.view_report_Panel.ResumeLayout(false);
            this.view_report_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugtrackerdbDataSet2BindingSource)).EndInit();
            this.profilePanel.ResumeLayout(false);
            this.profilePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label menu;
        private System.Windows.Forms.Button profile;
        private System.Windows.Forms.Button view_reportButton;
        private System.Windows.Forms.Button update_bugsButton;
        private System.Windows.Forms.Button bugsButton;
        private System.Windows.Forms.Label dash_heading;
        private System.Windows.Forms.Label border;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Panel add_bugsPanel;
        private System.Windows.Forms.TextBox bug_titleTextbox;
        private System.Windows.Forms.Button add_bugsButton;
        private System.Windows.Forms.Label assigned_to;
        private System.Windows.Forms.Label bugTitle;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.Label Panel_Heading;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox statusTextbox;
        private System.Windows.Forms.Label priority;
        private System.Windows.Forms.ComboBox priorityCombobox;
        private System.Windows.Forms.ComboBox assigned_toCombobox;
        private System.Windows.Forms.ComboBox assigned_byCombobox;
        private System.Windows.Forms.Label delivery_date;
        private System.Windows.Forms.Label assigned_by;
        private System.Windows.Forms.Label code_line;
        private System.Windows.Forms.TextBox session_username;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox line_no_Textbox;
        private System.Windows.Forms.TextBox class_Textbox;
        private System.Windows.Forms.TextBox project_Textbox;
        private System.Windows.Forms.Label class_label;
        private System.Windows.Forms.Label project;
        private System.Windows.Forms.TextBox author_Textbox;
        private System.Windows.Forms.TextBox method_Textbox;
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label code_author;
        private System.Windows.Forms.Label method;
        private System.Windows.Forms.Panel update_bugs_Panel;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox assigned_combo;
        private System.Windows.Forms.ComboBox priority_combo;
        private System.Windows.Forms.ComboBox status_combo;
        private System.Windows.Forms.TextBox author_txtbox;
        private System.Windows.Forms.TextBox method_txtbox;
        private System.Windows.Forms.TextBox class_txtbox;
        private System.Windows.Forms.TextBox project_txtbox;
        private System.Windows.Forms.TextBox line_txtbox;
        private System.Windows.Forms.TextBox title_txtbox;
        private System.Windows.Forms.Button update_bugs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label bug_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox id_txtbox;
        private System.Windows.Forms.Button show;
        private System.Windows.Forms.Panel view_report_Panel;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label heading;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView reports_dataGridView;
        private System.Windows.Forms.ComboBox filter;
        private System.Windows.Forms.TextBox search;
        private System.Windows.Forms.Label search_label;
        private System.Windows.Forms.Label filter_label;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.BindingSource bugtrackerdbDataSet2BindingSource;
        private Bug_tracker_dbDataSet2 bug_tracker_dbDataSet2;
        private Bug_tracker_dbDataSet3 bug_tracker_dbDataSet3;
        private System.Windows.Forms.BindingSource bugsBindingSource;
        private Bug_tracker_dbDataSet3TableAdapters.BugsTableAdapter bugsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fillDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedtoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionalinfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugAssignedbyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugtitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateofdeliveryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn buglinenoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeauthornameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filesDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel profilePanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label profileHeader;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.TextBox text_username;
        private System.Windows.Forms.Label email_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.TextBox text_password;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox git;
        private System.Windows.Forms.Label label16;
        private FastColoredTextBoxNS.FastColoredTextBox desc_txtbox;
        private FastColoredTextBoxNS.FastColoredTextBox bug_descriptionTextbox;
    }
}