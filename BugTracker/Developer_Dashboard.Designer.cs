﻿namespace BugTracker
{
    partial class Developer_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Developer_Dashboard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.session_username = new System.Windows.Forms.TextBox();
            this.exit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logout = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.Label();
            this.profile = new System.Windows.Forms.Button();
            this.view_reportButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.view_BugsButton = new System.Windows.Forms.Button();
            this.dash_heading = new System.Windows.Forms.Label();
            this.border = new System.Windows.Forms.Label();
            this.Heading = new System.Windows.Forms.Label();
            this.view_bugsPanel = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bugIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fillDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedtoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionalinfoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugAssignedbyDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugtitleDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateofdeliveryDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buglinenoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.methodDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeauthornameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filesDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fixedDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.versionControlLinkDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewLinkColumn();
            this.bugsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bug_tracker_dbDataSet4 = new BugTracker.Bug_tracker_dbDataSet4();
            this.filter_label = new System.Windows.Forms.Label();
            this.filter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.view_report_Panel = new System.Windows.Forms.Panel();
            this.searchBtn = new System.Windows.Forms.Button();
            this.search_textbox = new System.Windows.Forms.TextBox();
            this.search_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.filter_combobox = new System.Windows.Forms.ComboBox();
            this.reports_dataGridView = new System.Windows.Forms.DataGridView();
            this.bugIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fillDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedtoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionalinfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugAssignedbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugtitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateofdeliveryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buglinenoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.methodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeauthornameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fixedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.versionControlLinkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.update_bugs_Panel = new System.Windows.Forms.Panel();
            this.desc_txtbox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.show = new System.Windows.Forms.Button();
            this.author_txtbox = new System.Windows.Forms.TextBox();
            this.method_txtbox = new System.Windows.Forms.TextBox();
            this.class_txtbox = new System.Windows.Forms.TextBox();
            this.project_txtbox = new System.Windows.Forms.TextBox();
            this.line_txtbox = new System.Windows.Forms.TextBox();
            this.id_txtbox = new System.Windows.Forms.TextBox();
            this.title_txtbox = new System.Windows.Forms.TextBox();
            this.update_bugs = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.bug_id = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.status_combo = new System.Windows.Forms.ComboBox();
            this.priority_combo = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.profilePanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.profileHeader = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.text_name = new System.Windows.Forms.TextBox();
            this.upload_btn = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_username = new System.Windows.Forms.TextBox();
            this.email_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.text_password = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.bugsTableAdapter = new BugTracker.Bug_tracker_dbDataSet4TableAdapters.BugsTableAdapter();
            this.bug_tracker_dbDataSet5 = new BugTracker.Bug_tracker_dbDataSet5();
            this.bugsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bugsTableAdapter1 = new BugTracker.Bug_tracker_dbDataSet5TableAdapters.BugsTableAdapter();
            this.label6 = new System.Windows.Forms.Label();
            this.view_project = new System.Windows.Forms.Button();
            this.projectPanel = new System.Windows.Forms.Panel();
            this.projectReportdatagridView = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_filter = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bug_tracker_dbDataSet10 = new BugTracker.Bug_tracker_dbDataSet10();
            this.projectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.projectTableAdapter = new BugTracker.Bug_tracker_dbDataSet10TableAdapters.ProjectTableAdapter();
            this.bug_tracker_dbDataSet11 = new BugTracker.Bug_tracker_dbDataSet11();
            this.projectBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.projectTableAdapter1 = new BugTracker.Bug_tracker_dbDataSet11TableAdapters.ProjectTableAdapter();
            this.projectIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assigneddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.submissiondateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedtoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectstatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectpriorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionaldetailsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.versionControlLinkDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.update_project = new System.Windows.Forms.Button();
            this.update_projectPanel = new System.Windows.Forms.Panel();
            this.comments_textbox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.show_project = new System.Windows.Forms.Button();
            this.id_textbox = new System.Windows.Forms.TextBox();
            this.update_button = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.status_combobox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.refresh = new System.Windows.Forms.Button();
            this.refresh2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.view_bugsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet4)).BeginInit();
            this.view_report_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).BeginInit();
            this.update_bugs_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.desc_txtbox)).BeginInit();
            this.profilePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource1)).BeginInit();
            this.projectPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectReportdatagridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource1)).BeginInit();
            this.update_projectPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comments_textbox)).BeginInit();
            this.SuspendLayout();
            // 
            // session_username
            // 
            this.session_username.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.session_username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.session_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.session_username.ForeColor = System.Drawing.Color.Maroon;
            this.session_username.Location = new System.Drawing.Point(922, 38);
            this.session_username.Multiline = true;
            this.session_username.Name = "session_username";
            this.session_username.Size = new System.Drawing.Size(261, 28);
            this.session_username.TabIndex = 22;
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("exit.BackgroundImage")));
            this.exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.exit.FlatAppearance.BorderSize = 0;
            this.exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exit.Location = new System.Drawing.Point(1033, 11);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(34, 29);
            this.exit.TabIndex = 20;
            this.exit.UseVisualStyleBackColor = false;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.panel1.Controls.Add(this.update_project);
            this.panel1.Controls.Add(this.view_project);
            this.panel1.Controls.Add(this.logout);
            this.panel1.Controls.Add(this.menu);
            this.panel1.Controls.Add(this.profile);
            this.panel1.Controls.Add(this.view_reportButton);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.view_BugsButton);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(6, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(268, 681);
            this.panel1.TabIndex = 17;
            // 
            // logout
            // 
            this.logout.BackColor = System.Drawing.Color.RosyBrown;
            this.logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.logout.FlatAppearance.BorderSize = 0;
            this.logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logout.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout.ForeColor = System.Drawing.Color.White;
            this.logout.Location = new System.Drawing.Point(24, 623);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(222, 49);
            this.logout.TabIndex = 2;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = false;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // menu
            // 
            this.menu.AutoSize = true;
            this.menu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menu.Font = new System.Drawing.Font("Stencil", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.ForeColor = System.Drawing.Color.Black;
            this.menu.Location = new System.Drawing.Point(62, 23);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(124, 46);
            this.menu.TabIndex = 1;
            this.menu.Text = "MENU";
            // 
            // profile
            // 
            this.profile.BackColor = System.Drawing.Color.RosyBrown;
            this.profile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.profile.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.profile.FlatAppearance.BorderSize = 0;
            this.profile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profile.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profile.ForeColor = System.Drawing.Color.White;
            this.profile.Location = new System.Drawing.Point(24, 560);
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(222, 49);
            this.profile.TabIndex = 0;
            this.profile.Text = "Profile";
            this.profile.UseVisualStyleBackColor = false;
            this.profile.Click += new System.EventHandler(this.profile_Click);
            // 
            // view_reportButton
            // 
            this.view_reportButton.BackColor = System.Drawing.Color.RosyBrown;
            this.view_reportButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_reportButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.view_reportButton.FlatAppearance.BorderSize = 0;
            this.view_reportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_reportButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_reportButton.ForeColor = System.Drawing.Color.White;
            this.view_reportButton.Location = new System.Drawing.Point(24, 498);
            this.view_reportButton.Name = "view_reportButton";
            this.view_reportButton.Size = new System.Drawing.Size(222, 49);
            this.view_reportButton.TabIndex = 0;
            this.view_reportButton.Text = "View Report";
            this.view_reportButton.UseVisualStyleBackColor = false;
            this.view_reportButton.Click += new System.EventHandler(this.view_reportButton_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RosyBrown;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(24, 398);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(222, 88);
            this.button1.TabIndex = 0;
            this.button1.Text = "Update Bug Status";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Update_BugsButton_Click);
            // 
            // view_BugsButton
            // 
            this.view_BugsButton.BackColor = System.Drawing.Color.RosyBrown;
            this.view_BugsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_BugsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.view_BugsButton.FlatAppearance.BorderSize = 0;
            this.view_BugsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_BugsButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_BugsButton.ForeColor = System.Drawing.Color.White;
            this.view_BugsButton.Location = new System.Drawing.Point(24, 294);
            this.view_BugsButton.Name = "view_BugsButton";
            this.view_BugsButton.Size = new System.Drawing.Size(222, 88);
            this.view_BugsButton.TabIndex = 0;
            this.view_BugsButton.Text = "View Assigned Bugs";
            this.view_BugsButton.UseVisualStyleBackColor = false;
            this.view_BugsButton.Click += new System.EventHandler(this.view_BugsButton_Click);
            // 
            // dash_heading
            // 
            this.dash_heading.AutoSize = true;
            this.dash_heading.BackColor = System.Drawing.Color.BurlyWood;
            this.dash_heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dash_heading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dash_heading.Location = new System.Drawing.Point(6, 34);
            this.dash_heading.Name = "dash_heading";
            this.dash_heading.Size = new System.Drawing.Size(237, 25);
            this.dash_heading.TabIndex = 15;
            this.dash_heading.Text = "Developer\'s Dashboard";
            // 
            // border
            // 
            this.border.AutoSize = true;
            this.border.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.border.Location = new System.Drawing.Point(3, 55);
            this.border.Name = "border";
            this.border.Size = new System.Drawing.Size(1064, 17);
            this.border.TabIndex = 16;
            this.border.Text = "_________________________________________________________________________________" +
    "___________________________________________________";
            // 
            // Heading
            // 
            this.Heading.AutoSize = true;
            this.Heading.BackColor = System.Drawing.Color.Transparent;
            this.Heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Heading.ForeColor = System.Drawing.Color.DimGray;
            this.Heading.Location = new System.Drawing.Point(33, 23);
            this.Heading.Name = "Heading";
            this.Heading.Size = new System.Drawing.Size(309, 36);
            this.Heading.TabIndex = 0;
            this.Heading.Text = "View Assigned Bugs";
            // 
            // view_bugsPanel
            // 
            this.view_bugsPanel.BackColor = System.Drawing.Color.White;
            this.view_bugsPanel.Controls.Add(this.refresh2);
            this.view_bugsPanel.Controls.Add(this.dataGridView1);
            this.view_bugsPanel.Controls.Add(this.filter_label);
            this.view_bugsPanel.Controls.Add(this.filter);
            this.view_bugsPanel.Controls.Add(this.Heading);
            this.view_bugsPanel.Controls.Add(this.label2);
            this.view_bugsPanel.ForeColor = System.Drawing.Color.DimGray;
            this.view_bugsPanel.Location = new System.Drawing.Point(290, 89);
            this.view_bugsPanel.Name = "view_bugsPanel";
            this.view_bugsPanel.Size = new System.Drawing.Size(786, 681);
            this.view_bugsPanel.TabIndex = 19;
            this.view_bugsPanel.Click += new System.EventHandler(this.view_bugsPanel_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bugIDDataGridViewTextBoxColumn1,
            this.fillDateDataGridViewTextBoxColumn1,
            this.statusDataGridViewTextBoxColumn1,
            this.priorityDataGridViewTextBoxColumn1,
            this.assignedtoDataGridViewTextBoxColumn1,
            this.additionalinfoDataGridViewTextBoxColumn1,
            this.bugAssignedbyDataGridViewTextBoxColumn1,
            this.bugtitleDataGridViewTextBoxColumn1,
            this.dateofdeliveryDataGridViewTextBoxColumn1,
            this.buglinenoDataGridViewTextBoxColumn1,
            this.methodDataGridViewTextBoxColumn1,
            this.classDataGridViewTextBoxColumn1,
            this.projectDataGridViewTextBoxColumn1,
            this.codeauthornameDataGridViewTextBoxColumn1,
            this.filesDataGridViewTextBoxColumn1,
            this.fixedDateDataGridViewTextBoxColumn1,
            this.versionControlLinkDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this.bugsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(13, 259);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(765, 404);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // bugIDDataGridViewTextBoxColumn1
            // 
            this.bugIDDataGridViewTextBoxColumn1.DataPropertyName = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn1.HeaderText = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn1.Name = "bugIDDataGridViewTextBoxColumn1";
            this.bugIDDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // fillDateDataGridViewTextBoxColumn1
            // 
            this.fillDateDataGridViewTextBoxColumn1.DataPropertyName = "FillDate";
            this.fillDateDataGridViewTextBoxColumn1.HeaderText = "FillDate";
            this.fillDateDataGridViewTextBoxColumn1.Name = "fillDateDataGridViewTextBoxColumn1";
            // 
            // statusDataGridViewTextBoxColumn1
            // 
            this.statusDataGridViewTextBoxColumn1.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn1.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn1.Name = "statusDataGridViewTextBoxColumn1";
            // 
            // priorityDataGridViewTextBoxColumn1
            // 
            this.priorityDataGridViewTextBoxColumn1.DataPropertyName = "Priority";
            this.priorityDataGridViewTextBoxColumn1.HeaderText = "Priority";
            this.priorityDataGridViewTextBoxColumn1.Name = "priorityDataGridViewTextBoxColumn1";
            // 
            // assignedtoDataGridViewTextBoxColumn1
            // 
            this.assignedtoDataGridViewTextBoxColumn1.DataPropertyName = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn1.HeaderText = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn1.Name = "assignedtoDataGridViewTextBoxColumn1";
            // 
            // additionalinfoDataGridViewTextBoxColumn1
            // 
            this.additionalinfoDataGridViewTextBoxColumn1.DataPropertyName = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn1.HeaderText = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn1.Name = "additionalinfoDataGridViewTextBoxColumn1";
            // 
            // bugAssignedbyDataGridViewTextBoxColumn1
            // 
            this.bugAssignedbyDataGridViewTextBoxColumn1.DataPropertyName = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn1.HeaderText = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn1.Name = "bugAssignedbyDataGridViewTextBoxColumn1";
            // 
            // bugtitleDataGridViewTextBoxColumn1
            // 
            this.bugtitleDataGridViewTextBoxColumn1.DataPropertyName = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn1.HeaderText = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn1.Name = "bugtitleDataGridViewTextBoxColumn1";
            // 
            // dateofdeliveryDataGridViewTextBoxColumn1
            // 
            this.dateofdeliveryDataGridViewTextBoxColumn1.DataPropertyName = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn1.HeaderText = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn1.Name = "dateofdeliveryDataGridViewTextBoxColumn1";
            // 
            // buglinenoDataGridViewTextBoxColumn1
            // 
            this.buglinenoDataGridViewTextBoxColumn1.DataPropertyName = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn1.HeaderText = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn1.Name = "buglinenoDataGridViewTextBoxColumn1";
            // 
            // methodDataGridViewTextBoxColumn1
            // 
            this.methodDataGridViewTextBoxColumn1.DataPropertyName = "method";
            this.methodDataGridViewTextBoxColumn1.HeaderText = "method";
            this.methodDataGridViewTextBoxColumn1.Name = "methodDataGridViewTextBoxColumn1";
            // 
            // classDataGridViewTextBoxColumn1
            // 
            this.classDataGridViewTextBoxColumn1.DataPropertyName = "Class";
            this.classDataGridViewTextBoxColumn1.HeaderText = "Class";
            this.classDataGridViewTextBoxColumn1.Name = "classDataGridViewTextBoxColumn1";
            // 
            // projectDataGridViewTextBoxColumn1
            // 
            this.projectDataGridViewTextBoxColumn1.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn1.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn1.Name = "projectDataGridViewTextBoxColumn1";
            // 
            // codeauthornameDataGridViewTextBoxColumn1
            // 
            this.codeauthornameDataGridViewTextBoxColumn1.DataPropertyName = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn1.HeaderText = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn1.Name = "codeauthornameDataGridViewTextBoxColumn1";
            // 
            // filesDataGridViewTextBoxColumn1
            // 
            this.filesDataGridViewTextBoxColumn1.DataPropertyName = "Files";
            this.filesDataGridViewTextBoxColumn1.HeaderText = "Files";
            this.filesDataGridViewTextBoxColumn1.Name = "filesDataGridViewTextBoxColumn1";
            // 
            // fixedDateDataGridViewTextBoxColumn1
            // 
            this.fixedDateDataGridViewTextBoxColumn1.DataPropertyName = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn1.HeaderText = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn1.Name = "fixedDateDataGridViewTextBoxColumn1";
            // 
            // versionControlLinkDataGridViewTextBoxColumn1
            // 
            this.versionControlLinkDataGridViewTextBoxColumn1.DataPropertyName = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn1.HeaderText = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn1.Name = "versionControlLinkDataGridViewTextBoxColumn1";
            this.versionControlLinkDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.versionControlLinkDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bugsBindingSource
            // 
            this.bugsBindingSource.DataMember = "Bugs";
            this.bugsBindingSource.DataSource = this.bug_tracker_dbDataSet4;
            // 
            // bug_tracker_dbDataSet4
            // 
            this.bug_tracker_dbDataSet4.DataSetName = "Bug_tracker_dbDataSet4";
            this.bug_tracker_dbDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // filter_label
            // 
            this.filter_label.AutoSize = true;
            this.filter_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter_label.ForeColor = System.Drawing.Color.Gray;
            this.filter_label.Location = new System.Drawing.Point(8, 118);
            this.filter_label.Name = "filter_label";
            this.filter_label.Size = new System.Drawing.Size(54, 25);
            this.filter_label.TabIndex = 15;
            this.filter_label.Text = "Filter";
            // 
            // filter
            // 
            this.filter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.filter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter.ForeColor = System.Drawing.Color.Gray;
            this.filter.FormattingEnabled = true;
            this.filter.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.filter.Location = new System.Drawing.Point(13, 146);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(354, 33);
            this.filter.TabIndex = 14;
            this.filter.SelectedIndexChanged += new System.EventHandler(this.filter_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(35, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(725, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "_________________________________________________________________";
            // 
            // view_report_Panel
            // 
            this.view_report_Panel.BackColor = System.Drawing.Color.White;
            this.view_report_Panel.Controls.Add(this.searchBtn);
            this.view_report_Panel.Controls.Add(this.search_textbox);
            this.view_report_Panel.Controls.Add(this.search_label);
            this.view_report_Panel.Controls.Add(this.label1);
            this.view_report_Panel.Controls.Add(this.filter_combobox);
            this.view_report_Panel.Controls.Add(this.reports_dataGridView);
            this.view_report_Panel.Controls.Add(this.textBox2);
            this.view_report_Panel.Controls.Add(this.label3);
            this.view_report_Panel.Controls.Add(this.label4);
            this.view_report_Panel.Location = new System.Drawing.Point(287, 88);
            this.view_report_Panel.Name = "view_report_Panel";
            this.view_report_Panel.Size = new System.Drawing.Size(796, 678);
            this.view_report_Panel.TabIndex = 17;
            this.view_report_Panel.Click += new System.EventHandler(this.view_report_Panel_Click);
            // 
            // searchBtn
            // 
            this.searchBtn.BackColor = System.Drawing.Color.Transparent;
            this.searchBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchBtn.BackgroundImage")));
            this.searchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchBtn.FlatAppearance.BorderSize = 0;
            this.searchBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.searchBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.searchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.searchBtn.Location = new System.Drawing.Point(320, 141);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(60, 42);
            this.searchBtn.TabIndex = 17;
            this.searchBtn.UseVisualStyleBackColor = false;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // search_textbox
            // 
            this.search_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_textbox.ForeColor = System.Drawing.Color.Gray;
            this.search_textbox.Location = new System.Drawing.Point(26, 146);
            this.search_textbox.Multiline = true;
            this.search_textbox.Name = "search_textbox";
            this.search_textbox.Size = new System.Drawing.Size(288, 33);
            this.search_textbox.TabIndex = 16;
            // 
            // search_label
            // 
            this.search_label.AutoSize = true;
            this.search_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_label.ForeColor = System.Drawing.Color.Gray;
            this.search_label.Location = new System.Drawing.Point(21, 118);
            this.search_label.Name = "search_label";
            this.search_label.Size = new System.Drawing.Size(75, 25);
            this.search_label.TabIndex = 15;
            this.search_label.Text = "Search";
            this.search_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(404, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 13;
            this.label1.Text = "Filter";
            // 
            // filter_combobox
            // 
            this.filter_combobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.filter_combobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filter_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter_combobox.ForeColor = System.Drawing.Color.Gray;
            this.filter_combobox.FormattingEnabled = true;
            this.filter_combobox.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.filter_combobox.Location = new System.Drawing.Point(409, 147);
            this.filter_combobox.Name = "filter_combobox";
            this.filter_combobox.Size = new System.Drawing.Size(354, 33);
            this.filter_combobox.TabIndex = 12;
            this.filter_combobox.SelectedIndexChanged += new System.EventHandler(this.filter_comboBox_SelectedIndexChanged);
            // 
            // reports_dataGridView
            // 
            this.reports_dataGridView.AutoGenerateColumns = false;
            this.reports_dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.reports_dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.reports_dataGridView.ColumnHeadersHeight = 43;
            this.reports_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bugIDDataGridViewTextBoxColumn,
            this.fillDateDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.priorityDataGridViewTextBoxColumn,
            this.assignedtoDataGridViewTextBoxColumn,
            this.additionalinfoDataGridViewTextBoxColumn,
            this.bugAssignedbyDataGridViewTextBoxColumn,
            this.bugtitleDataGridViewTextBoxColumn,
            this.dateofdeliveryDataGridViewTextBoxColumn,
            this.buglinenoDataGridViewTextBoxColumn,
            this.methodDataGridViewTextBoxColumn,
            this.classDataGridViewTextBoxColumn,
            this.projectDataGridViewTextBoxColumn,
            this.codeauthornameDataGridViewTextBoxColumn,
            this.filesDataGridViewTextBoxColumn,
            this.fixedDateDataGridViewTextBoxColumn,
            this.versionControlLinkDataGridViewTextBoxColumn});
            this.reports_dataGridView.DataSource = this.bugsBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reports_dataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.reports_dataGridView.EnableHeadersVisualStyles = false;
            this.reports_dataGridView.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.Location = new System.Drawing.Point(9, 251);
            this.reports_dataGridView.Name = "reports_dataGridView";
            this.reports_dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.reports_dataGridView.RowHeadersVisible = false;
            this.reports_dataGridView.RowHeadersWidth = 50;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(215)))), ((int)(((byte)(208)))));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.reports_dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.reports_dataGridView.RowTemplate.Height = 24;
            this.reports_dataGridView.Size = new System.Drawing.Size(773, 411);
            this.reports_dataGridView.TabIndex = 11;
            this.reports_dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.reports_dataGridView_CellContentClick);
            // 
            // bugIDDataGridViewTextBoxColumn
            // 
            this.bugIDDataGridViewTextBoxColumn.DataPropertyName = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn.HeaderText = "Bug_ID";
            this.bugIDDataGridViewTextBoxColumn.Name = "bugIDDataGridViewTextBoxColumn";
            this.bugIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fillDateDataGridViewTextBoxColumn
            // 
            this.fillDateDataGridViewTextBoxColumn.DataPropertyName = "FillDate";
            this.fillDateDataGridViewTextBoxColumn.HeaderText = "FillDate";
            this.fillDateDataGridViewTextBoxColumn.Name = "fillDateDataGridViewTextBoxColumn";
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            // 
            // priorityDataGridViewTextBoxColumn
            // 
            this.priorityDataGridViewTextBoxColumn.DataPropertyName = "Priority";
            this.priorityDataGridViewTextBoxColumn.HeaderText = "Priority";
            this.priorityDataGridViewTextBoxColumn.Name = "priorityDataGridViewTextBoxColumn";
            // 
            // assignedtoDataGridViewTextBoxColumn
            // 
            this.assignedtoDataGridViewTextBoxColumn.DataPropertyName = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.HeaderText = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.Name = "assignedtoDataGridViewTextBoxColumn";
            // 
            // additionalinfoDataGridViewTextBoxColumn
            // 
            this.additionalinfoDataGridViewTextBoxColumn.DataPropertyName = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn.HeaderText = "Additional_info";
            this.additionalinfoDataGridViewTextBoxColumn.Name = "additionalinfoDataGridViewTextBoxColumn";
            // 
            // bugAssignedbyDataGridViewTextBoxColumn
            // 
            this.bugAssignedbyDataGridViewTextBoxColumn.DataPropertyName = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn.HeaderText = "Bug_Assigned_by";
            this.bugAssignedbyDataGridViewTextBoxColumn.Name = "bugAssignedbyDataGridViewTextBoxColumn";
            // 
            // bugtitleDataGridViewTextBoxColumn
            // 
            this.bugtitleDataGridViewTextBoxColumn.DataPropertyName = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn.HeaderText = "Bug_title";
            this.bugtitleDataGridViewTextBoxColumn.Name = "bugtitleDataGridViewTextBoxColumn";
            // 
            // dateofdeliveryDataGridViewTextBoxColumn
            // 
            this.dateofdeliveryDataGridViewTextBoxColumn.DataPropertyName = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn.HeaderText = "Date_of_delivery";
            this.dateofdeliveryDataGridViewTextBoxColumn.Name = "dateofdeliveryDataGridViewTextBoxColumn";
            // 
            // buglinenoDataGridViewTextBoxColumn
            // 
            this.buglinenoDataGridViewTextBoxColumn.DataPropertyName = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn.HeaderText = "Bug_line_no";
            this.buglinenoDataGridViewTextBoxColumn.Name = "buglinenoDataGridViewTextBoxColumn";
            // 
            // methodDataGridViewTextBoxColumn
            // 
            this.methodDataGridViewTextBoxColumn.DataPropertyName = "method";
            this.methodDataGridViewTextBoxColumn.HeaderText = "method";
            this.methodDataGridViewTextBoxColumn.Name = "methodDataGridViewTextBoxColumn";
            // 
            // classDataGridViewTextBoxColumn
            // 
            this.classDataGridViewTextBoxColumn.DataPropertyName = "Class";
            this.classDataGridViewTextBoxColumn.HeaderText = "Class";
            this.classDataGridViewTextBoxColumn.Name = "classDataGridViewTextBoxColumn";
            // 
            // projectDataGridViewTextBoxColumn
            // 
            this.projectDataGridViewTextBoxColumn.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn.Name = "projectDataGridViewTextBoxColumn";
            // 
            // codeauthornameDataGridViewTextBoxColumn
            // 
            this.codeauthornameDataGridViewTextBoxColumn.DataPropertyName = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn.HeaderText = "Code_author_name";
            this.codeauthornameDataGridViewTextBoxColumn.Name = "codeauthornameDataGridViewTextBoxColumn";
            // 
            // filesDataGridViewTextBoxColumn
            // 
            this.filesDataGridViewTextBoxColumn.DataPropertyName = "Files";
            this.filesDataGridViewTextBoxColumn.HeaderText = "Files";
            this.filesDataGridViewTextBoxColumn.Name = "filesDataGridViewTextBoxColumn";
            // 
            // fixedDateDataGridViewTextBoxColumn
            // 
            this.fixedDateDataGridViewTextBoxColumn.DataPropertyName = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn.HeaderText = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn.Name = "fixedDateDataGridViewTextBoxColumn";
            // 
            // versionControlLinkDataGridViewTextBoxColumn
            // 
            this.versionControlLinkDataGridViewTextBoxColumn.DataPropertyName = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn.HeaderText = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn.Name = "versionControlLinkDataGridViewTextBoxColumn";
            this.versionControlLinkDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.versionControlLinkDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Maroon;
            this.textBox2.Location = new System.Drawing.Point(521, 3);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(261, 28);
            this.textBox2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(27, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 36);
            this.label3.TabIndex = 7;
            this.label3.Text = "Report";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(16, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(725, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "_________________________________________________________________";
            // 
            // update_bugs_Panel
            // 
            this.update_bugs_Panel.BackColor = System.Drawing.Color.White;
            this.update_bugs_Panel.Controls.Add(this.desc_txtbox);
            this.update_bugs_Panel.Controls.Add(this.show);
            this.update_bugs_Panel.Controls.Add(this.author_txtbox);
            this.update_bugs_Panel.Controls.Add(this.method_txtbox);
            this.update_bugs_Panel.Controls.Add(this.class_txtbox);
            this.update_bugs_Panel.Controls.Add(this.project_txtbox);
            this.update_bugs_Panel.Controls.Add(this.line_txtbox);
            this.update_bugs_Panel.Controls.Add(this.id_txtbox);
            this.update_bugs_Panel.Controls.Add(this.title_txtbox);
            this.update_bugs_Panel.Controls.Add(this.update_bugs);
            this.update_bugs_Panel.Controls.Add(this.label9);
            this.update_bugs_Panel.Controls.Add(this.bug_id);
            this.update_bugs_Panel.Controls.Add(this.label11);
            this.update_bugs_Panel.Controls.Add(this.label12);
            this.update_bugs_Panel.Controls.Add(this.label13);
            this.update_bugs_Panel.Controls.Add(this.label14);
            this.update_bugs_Panel.Controls.Add(this.label15);
            this.update_bugs_Panel.Controls.Add(this.label16);
            this.update_bugs_Panel.Controls.Add(this.label17);
            this.update_bugs_Panel.Controls.Add(this.label18);
            this.update_bugs_Panel.Controls.Add(this.label19);
            this.update_bugs_Panel.Controls.Add(this.label21);
            this.update_bugs_Panel.Controls.Add(this.label22);
            this.update_bugs_Panel.Controls.Add(this.status_combo);
            this.update_bugs_Panel.Controls.Add(this.priority_combo);
            this.update_bugs_Panel.Controls.Add(this.dateTimePicker2);
            this.update_bugs_Panel.ForeColor = System.Drawing.Color.DimGray;
            this.update_bugs_Panel.Location = new System.Drawing.Point(290, 92);
            this.update_bugs_Panel.Name = "update_bugs_Panel";
            this.update_bugs_Panel.Size = new System.Drawing.Size(789, 681);
            this.update_bugs_Panel.TabIndex = 18;
            // 
            // desc_txtbox
            // 
            this.desc_txtbox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.desc_txtbox.AutoScrollMinSize = new System.Drawing.Size(221, 18);
            this.desc_txtbox.BackBrush = null;
            this.desc_txtbox.BackColor = System.Drawing.Color.Silver;
            this.desc_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.desc_txtbox.CharHeight = 18;
            this.desc_txtbox.CharWidth = 10;
            this.desc_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.desc_txtbox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.desc_txtbox.IsReplaceMode = false;
            this.desc_txtbox.Location = new System.Drawing.Point(404, 451);
            this.desc_txtbox.Name = "desc_txtbox";
            this.desc_txtbox.Paddings = new System.Windows.Forms.Padding(0);
            this.desc_txtbox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.desc_txtbox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("desc_txtbox.ServiceColors")));
            this.desc_txtbox.Size = new System.Drawing.Size(368, 209);
            this.desc_txtbox.TabIndex = 13;
            this.desc_txtbox.Text = "fastColoredTextBox1";
            this.desc_txtbox.Zoom = 100;
            this.desc_txtbox.Load += new System.EventHandler(this.desc_txtbox_Load);
            // 
            // show
            // 
            this.show.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.show.Cursor = System.Windows.Forms.Cursors.Hand;
            this.show.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.show.FlatAppearance.BorderSize = 0;
            this.show.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.show.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.show.ForeColor = System.Drawing.Color.Black;
            this.show.Location = new System.Drawing.Point(23, 188);
            this.show.Name = "show";
            this.show.Size = new System.Drawing.Size(138, 24);
            this.show.TabIndex = 0;
            this.show.Text = "Show value";
            this.show.UseVisualStyleBackColor = false;
            this.show.Click += new System.EventHandler(this.show_Click);
            // 
            // author_txtbox
            // 
            this.author_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.author_txtbox.Location = new System.Drawing.Point(406, 372);
            this.author_txtbox.Multiline = true;
            this.author_txtbox.Name = "author_txtbox";
            this.author_txtbox.Size = new System.Drawing.Size(364, 36);
            this.author_txtbox.TabIndex = 1;
            // 
            // method_txtbox
            // 
            this.method_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.method_txtbox.Location = new System.Drawing.Point(406, 295);
            this.method_txtbox.Multiline = true;
            this.method_txtbox.Name = "method_txtbox";
            this.method_txtbox.Size = new System.Drawing.Size(364, 36);
            this.method_txtbox.TabIndex = 1;
            // 
            // class_txtbox
            // 
            this.class_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.class_txtbox.Location = new System.Drawing.Point(406, 220);
            this.class_txtbox.Multiline = true;
            this.class_txtbox.Name = "class_txtbox";
            this.class_txtbox.Size = new System.Drawing.Size(364, 36);
            this.class_txtbox.TabIndex = 1;
            // 
            // project_txtbox
            // 
            this.project_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project_txtbox.Location = new System.Drawing.Point(591, 148);
            this.project_txtbox.Multiline = true;
            this.project_txtbox.Name = "project_txtbox";
            this.project_txtbox.Size = new System.Drawing.Size(179, 36);
            this.project_txtbox.TabIndex = 1;
            // 
            // line_txtbox
            // 
            this.line_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_txtbox.Location = new System.Drawing.Point(406, 148);
            this.line_txtbox.Multiline = true;
            this.line_txtbox.Name = "line_txtbox";
            this.line_txtbox.Size = new System.Drawing.Size(179, 36);
            this.line_txtbox.TabIndex = 1;
            // 
            // id_txtbox
            // 
            this.id_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id_txtbox.Location = new System.Drawing.Point(23, 146);
            this.id_txtbox.Multiline = true;
            this.id_txtbox.Name = "id_txtbox";
            this.id_txtbox.Size = new System.Drawing.Size(354, 36);
            this.id_txtbox.TabIndex = 1;
            // 
            // title_txtbox
            // 
            this.title_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title_txtbox.Location = new System.Drawing.Point(23, 336);
            this.title_txtbox.Multiline = true;
            this.title_txtbox.Name = "title_txtbox";
            this.title_txtbox.Size = new System.Drawing.Size(354, 36);
            this.title_txtbox.TabIndex = 1;
            // 
            // update_bugs
            // 
            this.update_bugs.BackColor = System.Drawing.Color.DodgerBlue;
            this.update_bugs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_bugs.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.update_bugs.FlatAppearance.BorderSize = 0;
            this.update_bugs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update_bugs.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_bugs.ForeColor = System.Drawing.Color.White;
            this.update_bugs.Location = new System.Drawing.Point(23, 557);
            this.update_bugs.Name = "update_bugs";
            this.update_bugs.Size = new System.Drawing.Size(354, 39);
            this.update_bugs.TabIndex = 0;
            this.update_bugs.Text = "Update Bugs";
            this.update_bugs.UseVisualStyleBackColor = false;
            this.update_bugs.Click += new System.EventHandler(this.update_bugsClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 459);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bug Fixed Date";
            // 
            // bug_id
            // 
            this.bug_id.AutoSize = true;
            this.bug_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bug_id.Location = new System.Drawing.Point(22, 120);
            this.bug_id.Name = "bug_id";
            this.bug_id.Size = new System.Drawing.Size(72, 24);
            this.bug_id.TabIndex = 0;
            this.bug_id.Text = "Bug ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 381);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 24);
            this.label11.TabIndex = 0;
            this.label11.Text = "Priority";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(402, 418);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(177, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Code / Comments";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(405, 347);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 24);
            this.label13.TabIndex = 0;
            this.label13.Text = "Code Author";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(405, 272);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 24);
            this.label14.TabIndex = 0;
            this.label14.Text = "Method";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(19, 307);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 24);
            this.label15.TabIndex = 0;
            this.label15.Text = "Bug Title";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(405, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 24);
            this.label16.TabIndex = 0;
            this.label16.Text = "Class";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(599, 120);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 24);
            this.label17.TabIndex = 0;
            this.label17.Text = "Project";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(405, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 24);
            this.label18.TabIndex = 0;
            this.label18.Text = "Bug Line No.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(19, 224);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 24);
            this.label19.TabIndex = 0;
            this.label19.Text = "Status";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.DimGray;
            this.label21.Location = new System.Drawing.Point(30, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(282, 36);
            this.label21.TabIndex = 0;
            this.label21.Text = "Update Bug Status";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Silver;
            this.label22.Location = new System.Drawing.Point(22, 45);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(725, 24);
            this.label22.TabIndex = 9;
            this.label22.Text = "_________________________________________________________________";
            // 
            // status_combo
            // 
            this.status_combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.status_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.status_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status_combo.FormattingEnabled = true;
            this.status_combo.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Fixed"});
            this.status_combo.Location = new System.Drawing.Point(23, 257);
            this.status_combo.Name = "status_combo";
            this.status_combo.Size = new System.Drawing.Size(354, 33);
            this.status_combo.TabIndex = 10;
            // 
            // priority_combo
            // 
            this.priority_combo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.priority_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.priority_combo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priority_combo.ForeColor = System.Drawing.Color.Gray;
            this.priority_combo.FormattingEnabled = true;
            this.priority_combo.Items.AddRange(new object[] {
            "High",
            "Medium",
            "Low"});
            this.priority_combo.Location = new System.Drawing.Point(23, 408);
            this.priority_combo.Name = "priority_combo";
            this.priority_combo.Size = new System.Drawing.Size(354, 33);
            this.priority_combo.TabIndex = 10;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(23, 495);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(354, 22);
            this.dateTimePicker2.TabIndex = 12;
            // 
            // profilePanel
            // 
            this.profilePanel.BackColor = System.Drawing.Color.White;
            this.profilePanel.Controls.Add(this.pictureBox1);
            this.profilePanel.Controls.Add(this.profileHeader);
            this.profilePanel.Controls.Add(this.name_label);
            this.profilePanel.Controls.Add(this.text_name);
            this.profilePanel.Controls.Add(this.upload_btn);
            this.profilePanel.Controls.Add(this.Update);
            this.profilePanel.Controls.Add(this.text_email);
            this.profilePanel.Controls.Add(this.text_username);
            this.profilePanel.Controls.Add(this.email_label);
            this.profilePanel.Controls.Add(this.username_label);
            this.profilePanel.Controls.Add(this.password_label);
            this.profilePanel.Controls.Add(this.text_password);
            this.profilePanel.Controls.Add(this.label20);
            this.profilePanel.Location = new System.Drawing.Point(280, 83);
            this.profilePanel.Name = "profilePanel";
            this.profilePanel.Size = new System.Drawing.Size(802, 687);
            this.profilePanel.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.Location = new System.Drawing.Point(463, 175);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 250);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // profileHeader
            // 
            this.profileHeader.AutoSize = true;
            this.profileHeader.BackColor = System.Drawing.Color.Transparent;
            this.profileHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profileHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHeader.ForeColor = System.Drawing.Color.DimGray;
            this.profileHeader.Location = new System.Drawing.Point(41, 28);
            this.profileHeader.Name = "profileHeader";
            this.profileHeader.Size = new System.Drawing.Size(158, 36);
            this.profileHeader.TabIndex = 7;
            this.profileHeader.Text = "My Profile";
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_label.Location = new System.Drawing.Point(44, 149);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(61, 24);
            this.name_label.TabIndex = 0;
            this.name_label.Text = "Name";
            // 
            // text_name
            // 
            this.text_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_name.ForeColor = System.Drawing.Color.Gray;
            this.text_name.Location = new System.Drawing.Point(48, 176);
            this.text_name.Multiline = true;
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(342, 34);
            this.text_name.TabIndex = 2;
            // 
            // upload_btn
            // 
            this.upload_btn.BackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.upload_btn.FlatAppearance.BorderSize = 0;
            this.upload_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.upload_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upload_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upload_btn.ForeColor = System.Drawing.Color.White;
            this.upload_btn.Location = new System.Drawing.Point(512, 475);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(167, 43);
            this.upload_btn.TabIndex = 4;
            this.upload_btn.Text = "Upload";
            this.upload_btn.UseVisualStyleBackColor = false;
            this.upload_btn.Click += new System.EventHandler(this.upload_btn_Click);
            // 
            // Update
            // 
            this.Update.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Update.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Update.FlatAppearance.BorderSize = 0;
            this.Update.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Update.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update.ForeColor = System.Drawing.Color.White;
            this.Update.Location = new System.Drawing.Point(48, 475);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(348, 43);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = false;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // text_email
            // 
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.ForeColor = System.Drawing.Color.Gray;
            this.text_email.Location = new System.Drawing.Point(48, 248);
            this.text_email.Multiline = true;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(342, 34);
            this.text_email.TabIndex = 2;
            // 
            // text_username
            // 
            this.text_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_username.ForeColor = System.Drawing.Color.Gray;
            this.text_username.Location = new System.Drawing.Point(47, 320);
            this.text_username.Multiline = true;
            this.text_username.Name = "text_username";
            this.text_username.Size = new System.Drawing.Size(342, 32);
            this.text_username.TabIndex = 2;
            // 
            // email_label
            // 
            this.email_label.AutoSize = true;
            this.email_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email_label.Location = new System.Drawing.Point(44, 221);
            this.email_label.Name = "email_label";
            this.email_label.Size = new System.Drawing.Size(57, 24);
            this.email_label.TabIndex = 0;
            this.email_label.Text = "Email";
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_label.Location = new System.Drawing.Point(44, 294);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(97, 24);
            this.username_label.TabIndex = 0;
            this.username_label.Text = "Username";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_label.Location = new System.Drawing.Point(44, 364);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(92, 24);
            this.password_label.TabIndex = 0;
            this.password_label.Text = "Password";
            // 
            // text_password
            // 
            this.text_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_password.ForeColor = System.Drawing.Color.Gray;
            this.text_password.Location = new System.Drawing.Point(48, 391);
            this.text_password.Multiline = true;
            this.text_password.Name = "text_password";
            this.text_password.Size = new System.Drawing.Size(342, 34);
            this.text_password.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Silver;
            this.label20.Location = new System.Drawing.Point(29, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(725, 24);
            this.label20.TabIndex = 8;
            this.label20.Text = "_________________________________________________________________";
            // 
            // bugsTableAdapter
            // 
            this.bugsTableAdapter.ClearBeforeFill = true;
            // 
            // bug_tracker_dbDataSet5
            // 
            this.bug_tracker_dbDataSet5.DataSetName = "Bug_tracker_dbDataSet5";
            this.bug_tracker_dbDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bugsBindingSource1
            // 
            this.bugsBindingSource1.DataMember = "Bugs";
            this.bugsBindingSource1.DataSource = this.bug_tracker_dbDataSet5;
            // 
            // bugsTableAdapter1
            // 
            this.bugsTableAdapter1.ClearBeforeFill = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(858, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 23;
            this.label6.Text = "Hello,";
            // 
            // view_project
            // 
            this.view_project.BackColor = System.Drawing.Color.RosyBrown;
            this.view_project.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_project.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.view_project.FlatAppearance.BorderSize = 0;
            this.view_project.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_project.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_project.ForeColor = System.Drawing.Color.White;
            this.view_project.Location = new System.Drawing.Point(24, 90);
            this.view_project.Name = "view_project";
            this.view_project.Size = new System.Drawing.Size(222, 88);
            this.view_project.TabIndex = 3;
            this.view_project.Text = "View Assigned Project";
            this.view_project.UseVisualStyleBackColor = false;
            this.view_project.Click += new System.EventHandler(this.view_project_Click);
            // 
            // projectPanel
            // 
            this.projectPanel.BackColor = System.Drawing.Color.White;
            this.projectPanel.Controls.Add(this.projectReportdatagridView);
            this.projectPanel.Controls.Add(this.label5);
            this.projectPanel.Controls.Add(this.refresh);
            this.projectPanel.Controls.Add(this.comboBox_filter);
            this.projectPanel.Controls.Add(this.label7);
            this.projectPanel.Controls.Add(this.label8);
            this.projectPanel.ForeColor = System.Drawing.Color.DimGray;
            this.projectPanel.Location = new System.Drawing.Point(280, 75);
            this.projectPanel.Name = "projectPanel";
            this.projectPanel.Size = new System.Drawing.Size(804, 701);
            this.projectPanel.TabIndex = 20;
            // 
            // projectReportdatagridView
            // 
            this.projectReportdatagridView.AutoGenerateColumns = false;
            this.projectReportdatagridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.projectReportdatagridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectIDDataGridViewTextBoxColumn,
            this.projectnameDataGridViewTextBoxColumn,
            this.assigneddateDataGridViewTextBoxColumn,
            this.submissiondateDataGridViewTextBoxColumn,
            this.assignedtoDataGridViewTextBoxColumn2,
            this.projectstatusDataGridViewTextBoxColumn,
            this.projectpriorityDataGridViewTextBoxColumn,
            this.additionaldetailsDataGridViewTextBoxColumn,
            this.versionControlLinkDataGridViewTextBoxColumn2});
            this.projectReportdatagridView.DataSource = this.projectBindingSource1;
            this.projectReportdatagridView.Location = new System.Drawing.Point(13, 229);
            this.projectReportdatagridView.Name = "projectReportdatagridView";
            this.projectReportdatagridView.RowTemplate.Height = 24;
            this.projectReportdatagridView.Size = new System.Drawing.Size(780, 436);
            this.projectReportdatagridView.TabIndex = 16;
            this.projectReportdatagridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.projectReportdatagridView_CellContentClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(8, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 25);
            this.label5.TabIndex = 15;
            this.label5.Text = "Filter";
            // 
            // comboBox_filter
            // 
            this.comboBox_filter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox_filter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_filter.ForeColor = System.Drawing.Color.Gray;
            this.comboBox_filter.FormattingEnabled = true;
            this.comboBox_filter.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.comboBox_filter.Location = new System.Drawing.Point(13, 146);
            this.comboBox_filter.Name = "comboBox_filter";
            this.comboBox_filter.Size = new System.Drawing.Size(354, 33);
            this.comboBox_filter.TabIndex = 14;
            this.comboBox_filter.SelectedIndexChanged += new System.EventHandler(this.comboBox_filter_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(33, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(352, 36);
            this.label7.TabIndex = 0;
            this.label7.Text = "View Assigned Projects";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Silver;
            this.label8.Location = new System.Drawing.Point(35, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(725, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "_________________________________________________________________";
            // 
            // bug_tracker_dbDataSet10
            // 
            this.bug_tracker_dbDataSet10.DataSetName = "Bug_tracker_dbDataSet10";
            this.bug_tracker_dbDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // projectBindingSource
            // 
            this.projectBindingSource.DataMember = "Project";
            this.projectBindingSource.DataSource = this.bug_tracker_dbDataSet10;
            // 
            // projectTableAdapter
            // 
            this.projectTableAdapter.ClearBeforeFill = true;
            // 
            // bug_tracker_dbDataSet11
            // 
            this.bug_tracker_dbDataSet11.DataSetName = "Bug_tracker_dbDataSet11";
            this.bug_tracker_dbDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // projectBindingSource1
            // 
            this.projectBindingSource1.DataMember = "Project";
            this.projectBindingSource1.DataSource = this.bug_tracker_dbDataSet11;
            // 
            // projectTableAdapter1
            // 
            this.projectTableAdapter1.ClearBeforeFill = true;
            // 
            // projectIDDataGridViewTextBoxColumn
            // 
            this.projectIDDataGridViewTextBoxColumn.DataPropertyName = "Project_ID";
            this.projectIDDataGridViewTextBoxColumn.HeaderText = "Project_ID";
            this.projectIDDataGridViewTextBoxColumn.Name = "projectIDDataGridViewTextBoxColumn";
            this.projectIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // projectnameDataGridViewTextBoxColumn
            // 
            this.projectnameDataGridViewTextBoxColumn.DataPropertyName = "Project_name";
            this.projectnameDataGridViewTextBoxColumn.HeaderText = "Project_name";
            this.projectnameDataGridViewTextBoxColumn.Name = "projectnameDataGridViewTextBoxColumn";
            // 
            // assigneddateDataGridViewTextBoxColumn
            // 
            this.assigneddateDataGridViewTextBoxColumn.DataPropertyName = "Assigned_date";
            this.assigneddateDataGridViewTextBoxColumn.HeaderText = "Assigned_date";
            this.assigneddateDataGridViewTextBoxColumn.Name = "assigneddateDataGridViewTextBoxColumn";
            // 
            // submissiondateDataGridViewTextBoxColumn
            // 
            this.submissiondateDataGridViewTextBoxColumn.DataPropertyName = "Submission_date";
            this.submissiondateDataGridViewTextBoxColumn.HeaderText = "Submission_date";
            this.submissiondateDataGridViewTextBoxColumn.Name = "submissiondateDataGridViewTextBoxColumn";
            // 
            // assignedtoDataGridViewTextBoxColumn2
            // 
            this.assignedtoDataGridViewTextBoxColumn2.DataPropertyName = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn2.HeaderText = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn2.Name = "assignedtoDataGridViewTextBoxColumn2";
            // 
            // projectstatusDataGridViewTextBoxColumn
            // 
            this.projectstatusDataGridViewTextBoxColumn.DataPropertyName = "project_status";
            this.projectstatusDataGridViewTextBoxColumn.HeaderText = "project_status";
            this.projectstatusDataGridViewTextBoxColumn.Name = "projectstatusDataGridViewTextBoxColumn";
            // 
            // projectpriorityDataGridViewTextBoxColumn
            // 
            this.projectpriorityDataGridViewTextBoxColumn.DataPropertyName = "project_priority";
            this.projectpriorityDataGridViewTextBoxColumn.HeaderText = "project_priority";
            this.projectpriorityDataGridViewTextBoxColumn.Name = "projectpriorityDataGridViewTextBoxColumn";
            // 
            // additionaldetailsDataGridViewTextBoxColumn
            // 
            this.additionaldetailsDataGridViewTextBoxColumn.DataPropertyName = "Additional_details";
            this.additionaldetailsDataGridViewTextBoxColumn.HeaderText = "Additional_details";
            this.additionaldetailsDataGridViewTextBoxColumn.Name = "additionaldetailsDataGridViewTextBoxColumn";
            // 
            // versionControlLinkDataGridViewTextBoxColumn2
            // 
            this.versionControlLinkDataGridViewTextBoxColumn2.DataPropertyName = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn2.HeaderText = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn2.Name = "versionControlLinkDataGridViewTextBoxColumn2";
            // 
            // update_project
            // 
            this.update_project.BackColor = System.Drawing.Color.RosyBrown;
            this.update_project.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_project.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.update_project.FlatAppearance.BorderSize = 0;
            this.update_project.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update_project.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_project.ForeColor = System.Drawing.Color.White;
            this.update_project.Location = new System.Drawing.Point(24, 192);
            this.update_project.Name = "update_project";
            this.update_project.Size = new System.Drawing.Size(222, 88);
            this.update_project.TabIndex = 4;
            this.update_project.Text = "Update Project Status";
            this.update_project.UseVisualStyleBackColor = false;
            this.update_project.Click += new System.EventHandler(this.update_project_Click);
            // 
            // update_projectPanel
            // 
            this.update_projectPanel.BackColor = System.Drawing.Color.White;
            this.update_projectPanel.Controls.Add(this.comments_textbox);
            this.update_projectPanel.Controls.Add(this.show_project);
            this.update_projectPanel.Controls.Add(this.id_textbox);
            this.update_projectPanel.Controls.Add(this.update_button);
            this.update_projectPanel.Controls.Add(this.label10);
            this.update_projectPanel.Controls.Add(this.label23);
            this.update_projectPanel.Controls.Add(this.label25);
            this.update_projectPanel.Controls.Add(this.label32);
            this.update_projectPanel.Controls.Add(this.label33);
            this.update_projectPanel.Controls.Add(this.label34);
            this.update_projectPanel.Controls.Add(this.status_combobox);
            this.update_projectPanel.Controls.Add(this.dateTimePicker1);
            this.update_projectPanel.ForeColor = System.Drawing.Color.DimGray;
            this.update_projectPanel.Location = new System.Drawing.Point(279, 81);
            this.update_projectPanel.Name = "update_projectPanel";
            this.update_projectPanel.Size = new System.Drawing.Size(794, 686);
            this.update_projectPanel.TabIndex = 19;
            // 
            // comments_textbox
            // 
            this.comments_textbox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.comments_textbox.AutoScrollMinSize = new System.Drawing.Size(161, 18);
            this.comments_textbox.BackBrush = null;
            this.comments_textbox.BackColor = System.Drawing.Color.Silver;
            this.comments_textbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.comments_textbox.CharHeight = 18;
            this.comments_textbox.CharWidth = 10;
            this.comments_textbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.comments_textbox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.comments_textbox.IsReplaceMode = false;
            this.comments_textbox.Location = new System.Drawing.Point(29, 347);
            this.comments_textbox.Name = "comments_textbox";
            this.comments_textbox.Paddings = new System.Windows.Forms.Padding(0);
            this.comments_textbox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.comments_textbox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("comments_textbox.ServiceColors")));
            this.comments_textbox.Size = new System.Drawing.Size(738, 238);
            this.comments_textbox.TabIndex = 13;
            this.comments_textbox.Text = "Enter here...";
            this.comments_textbox.Zoom = 100;
            // 
            // show_project
            // 
            this.show_project.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.show_project.Cursor = System.Windows.Forms.Cursors.Hand;
            this.show_project.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.show_project.FlatAppearance.BorderSize = 0;
            this.show_project.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.show_project.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.show_project.ForeColor = System.Drawing.Color.Black;
            this.show_project.Location = new System.Drawing.Point(33, 205);
            this.show_project.Name = "show_project";
            this.show_project.Size = new System.Drawing.Size(138, 24);
            this.show_project.TabIndex = 0;
            this.show_project.Text = "Show values";
            this.show_project.UseVisualStyleBackColor = false;
            this.show_project.Click += new System.EventHandler(this.show_project_Click);
            // 
            // id_textbox
            // 
            this.id_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id_textbox.Location = new System.Drawing.Point(33, 162);
            this.id_textbox.Multiline = true;
            this.id_textbox.Name = "id_textbox";
            this.id_textbox.Size = new System.Drawing.Size(354, 36);
            this.id_textbox.TabIndex = 1;
            // 
            // update_button
            // 
            this.update_button.BackColor = System.Drawing.Color.DodgerBlue;
            this.update_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_button.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.update_button.FlatAppearance.BorderSize = 0;
            this.update_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update_button.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_button.ForeColor = System.Drawing.Color.White;
            this.update_button.Location = new System.Drawing.Point(29, 605);
            this.update_button.Name = "update_button";
            this.update_button.Size = new System.Drawing.Size(187, 46);
            this.update_button.TabIndex = 0;
            this.update_button.Text = "Update Project";
            this.update_button.UseVisualStyleBackColor = false;
            this.update_button.Click += new System.EventHandler(this.update_button_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(412, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(182, 24);
            this.label10.TabIndex = 0;
            this.label10.Text = "Project Fixed Date";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(27, 123);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 24);
            this.label23.TabIndex = 0;
            this.label23.Text = "Project ID";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(27, 314);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(177, 24);
            this.label25.TabIndex = 0;
            this.label25.Text = "Code / Comments";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(24, 238);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(66, 24);
            this.label32.TabIndex = 0;
            this.label32.Text = "Status";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.DimGray;
            this.label33.Location = new System.Drawing.Point(35, 36);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(325, 36);
            this.label33.TabIndex = 0;
            this.label33.Text = "Update Project Status";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Silver;
            this.label34.Location = new System.Drawing.Point(27, 58);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(725, 24);
            this.label34.TabIndex = 9;
            this.label34.Text = "_________________________________________________________________";
            // 
            // status_combobox
            // 
            this.status_combobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.status_combobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.status_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status_combobox.FormattingEnabled = true;
            this.status_combobox.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Fixed"});
            this.status_combobox.Location = new System.Drawing.Point(33, 266);
            this.status_combobox.Name = "status_combobox";
            this.status_combobox.Size = new System.Drawing.Size(354, 33);
            this.status_combobox.TabIndex = 10;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(413, 277);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(354, 22);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // refresh
            // 
            this.refresh.BackColor = System.Drawing.Color.Transparent;
            this.refresh.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("refresh.BackgroundImage")));
            this.refresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refresh.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.refresh.FlatAppearance.BorderSize = 0;
            this.refresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.refresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.refresh.Location = new System.Drawing.Point(739, 173);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(54, 48);
            this.refresh.TabIndex = 20;
            this.refresh.UseVisualStyleBackColor = false;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // refresh2
            // 
            this.refresh2.BackColor = System.Drawing.Color.Transparent;
            this.refresh2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("refresh2.BackgroundImage")));
            this.refresh2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.refresh2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refresh2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.refresh2.FlatAppearance.BorderSize = 0;
            this.refresh2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.refresh2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.refresh2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.refresh2.Location = new System.Drawing.Point(726, 206);
            this.refresh2.Name = "refresh2";
            this.refresh2.Size = new System.Drawing.Size(54, 48);
            this.refresh2.TabIndex = 21;
            this.refresh2.UseVisualStyleBackColor = false;
            this.refresh2.Click += new System.EventHandler(this.refresh2_Click);
            // 
            // Developer_Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 780);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.session_username);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dash_heading);
            this.Controls.Add(this.border);
            this.Controls.Add(this.view_bugsPanel);
            this.Controls.Add(this.update_projectPanel);
            this.Controls.Add(this.update_bugs_Panel);
            this.Controls.Add(this.profilePanel);
            this.Controls.Add(this.view_report_Panel);
            this.Controls.Add(this.projectPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Developer_Dashboard";
            this.Text = "Developer_Dashboard";
            this.Load += new System.EventHandler(this.Developer_Dashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.view_bugsPanel.ResumeLayout(false);
            this.view_bugsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet4)).EndInit();
            this.view_report_Panel.ResumeLayout(false);
            this.view_report_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).EndInit();
            this.update_bugs_Panel.ResumeLayout(false);
            this.update_bugs_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.desc_txtbox)).EndInit();
            this.profilePanel.ResumeLayout(false);
            this.profilePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource1)).EndInit();
            this.projectPanel.ResumeLayout(false);
            this.projectPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectReportdatagridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource1)).EndInit();
            this.update_projectPanel.ResumeLayout(false);
            this.update_projectPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comments_textbox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox session_username;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Label menu;
        private System.Windows.Forms.Button profile;
        private System.Windows.Forms.Button view_reportButton;
        private System.Windows.Forms.Button view_BugsButton;
        private System.Windows.Forms.Label dash_heading;
        private System.Windows.Forms.Label border;
        private System.Windows.Forms.Label Heading;
        private System.Windows.Forms.Panel view_bugsPanel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label filter_label;
        private System.Windows.Forms.ComboBox filter;
        private System.Windows.Forms.Panel view_report_Panel;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox search_textbox;
        private System.Windows.Forms.Label search_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox filter_combobox;
        private System.Windows.Forms.DataGridView reports_dataGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel update_bugs_Panel;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox priority_combo;
        private System.Windows.Forms.ComboBox status_combo;
        private System.Windows.Forms.Button show;
        private System.Windows.Forms.TextBox author_txtbox;
        private System.Windows.Forms.TextBox method_txtbox;
        private System.Windows.Forms.TextBox class_txtbox;
        private System.Windows.Forms.TextBox project_txtbox;
        private System.Windows.Forms.TextBox line_txtbox;
        private System.Windows.Forms.TextBox id_txtbox;
        private System.Windows.Forms.TextBox title_txtbox;
        private System.Windows.Forms.Button update_bugs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label bug_id;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel profilePanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label profileHeader;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.TextBox text_username;
        private System.Windows.Forms.Label email_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.TextBox text_password;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private Bug_tracker_dbDataSet4 bug_tracker_dbDataSet4;
        private System.Windows.Forms.BindingSource bugsBindingSource;
        private Bug_tracker_dbDataSet4TableAdapters.BugsTableAdapter bugsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fillDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedtoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionalinfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugAssignedbyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugtitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateofdeliveryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn buglinenoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeauthornameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fixedDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewLinkColumn versionControlLinkDataGridViewTextBoxColumn;
        private Bug_tracker_dbDataSet5 bug_tracker_dbDataSet5;
        private System.Windows.Forms.BindingSource bugsBindingSource1;
        private Bug_tracker_dbDataSet5TableAdapters.BugsTableAdapter bugsTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fillDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedtoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionalinfoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugAssignedbyDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugtitleDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateofdeliveryDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn buglinenoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn classDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeauthornameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn filesDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fixedDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewLinkColumn versionControlLinkDataGridViewTextBoxColumn1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private FastColoredTextBoxNS.FastColoredTextBox desc_txtbox;
        private System.Windows.Forms.Button view_project;
        private System.Windows.Forms.Panel projectPanel;
        private System.Windows.Forms.DataGridView projectReportdatagridView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_filter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Bug_tracker_dbDataSet10 bug_tracker_dbDataSet10;
        private System.Windows.Forms.BindingSource projectBindingSource;
        private Bug_tracker_dbDataSet10TableAdapters.ProjectTableAdapter projectTableAdapter;
        private Bug_tracker_dbDataSet11 bug_tracker_dbDataSet11;
        private System.Windows.Forms.BindingSource projectBindingSource1;
        private Bug_tracker_dbDataSet11TableAdapters.ProjectTableAdapter projectTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assigneddateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn submissiondateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedtoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectstatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectpriorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionaldetailsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn versionControlLinkDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button update_project;
        private System.Windows.Forms.Panel update_projectPanel;
        private FastColoredTextBoxNS.FastColoredTextBox comments_textbox;
        private System.Windows.Forms.Button show_project;
        private System.Windows.Forms.TextBox id_textbox;
        private System.Windows.Forms.Button update_button;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox status_combobox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Button refresh2;
    }
}