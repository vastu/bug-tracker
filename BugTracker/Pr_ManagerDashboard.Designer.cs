﻿namespace BugTracker
{
    partial class Pr_ManagerDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pr_ManagerDashboard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            this.border = new System.Windows.Forms.Label();
            this.dash_heading = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logout = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.Label();
            this.profile = new System.Windows.Forms.Button();
            this.viewProject = new System.Windows.Forms.Button();
            this.view_reportButton = new System.Windows.Forms.Button();
            this.allocate_projectButton = new System.Windows.Forms.Button();
            this.manage_usersButton = new System.Windows.Forms.Button();
            this.super_adminButton = new System.Windows.Forms.Button();
            this.super_adminPanel = new System.Windows.Forms.Panel();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.generate_password = new System.Windows.Forms.Button();
            this.generate_button = new System.Windows.Forms.Button();
            this.password = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.super_adminPanel_Heading = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Button();
            this.manage_usersPanel_Heading = new System.Windows.Forms.Label();
            this.view_users = new System.Windows.Forms.Label();
            this.delete_users = new System.Windows.Forms.Button();
            this.manage_usersPanel = new System.Windows.Forms.Panel();
            this.delete_error = new System.Windows.Forms.Label();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.manage_usersdataGridView = new System.Windows.Forms.DataGridView();
            this.useridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.designationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bug_tracker_dbDataSet6 = new BugTracker.Bug_tracker_dbDataSet6();
            this.Id_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.profilePanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.profileHeader = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.text_name = new System.Windows.Forms.TextBox();
            this.upload_btn = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.text_email = new System.Windows.Forms.TextBox();
            this.text_username = new System.Windows.Forms.TextBox();
            this.email_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.text_password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.reportPanel = new System.Windows.Forms.Panel();
            this.excel = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.search = new System.Windows.Forms.TextBox();
            this.search_label = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.filter_label = new System.Windows.Forms.Label();
            this.comboBox_year = new System.Windows.Forms.ComboBox();
            this.comboBox_month = new System.Windows.Forms.ComboBox();
            this.filter = new System.Windows.Forms.ComboBox();
            this.reports_dataGridView = new System.Windows.Forms.DataGridView();
            this.heading = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.add_projectPanel = new System.Windows.Forms.Panel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.assigned_toCombobox = new System.Windows.Forms.ComboBox();
            this.usersBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.bug_tracker_dbDataSet9 = new BugTracker.Bug_tracker_dbDataSet9();
            this.priorityCombobox = new System.Windows.Forms.ComboBox();
            this.statusTextbox = new System.Windows.Forms.ComboBox();
            this.project_titleTextbox = new System.Windows.Forms.TextBox();
            this.add_projectButton = new System.Windows.Forms.Button();
            this.delivery_date = new System.Windows.Forms.Label();
            this.assigned_to = new System.Windows.Forms.Label();
            this.priority = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.bugTitle = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Panel_Heading = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.git = new System.Windows.Forms.TextBox();
            this.session_username = new System.Windows.Forms.TextBox();
            this.usersTableAdapter = new BugTracker.Bug_tracker_dbDataSet6TableAdapters.UsersTableAdapter();
            this.bug_tracker_dbDataSet7 = new BugTracker.Bug_tracker_dbDataSet7();
            this.usersBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter1 = new BugTracker.Bug_tracker_dbDataSet7TableAdapters.UsersTableAdapter();
            this.bug_tracker_dbDataSet8 = new BugTracker.Bug_tracker_dbDataSet8();
            this.usersBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter2 = new BugTracker.Bug_tracker_dbDataSet8TableAdapters.UsersTableAdapter();
            this.usersTableAdapter3 = new BugTracker.Bug_tracker_dbDataSet9TableAdapters.UsersTableAdapter();
            this.bug_tracker_dbDataSet12 = new BugTracker.Bug_tracker_dbDataSet12();
            this.project_view = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.srch_btn = new System.Windows.Forms.Button();
            this.srch = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.filter2 = new System.Windows.Forms.ComboBox();
            this.project_report = new System.Windows.Forms.DataGridView();
            this.projectIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assigneddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.submissiondateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedtoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectstatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectpriorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.additionaldetailsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.versionControlLinkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fixedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bug_tracker_dbDataSet13 = new BugTracker.Bug_tracker_dbDataSet13();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.projectTableAdapter = new BugTracker.Bug_tracker_dbDataSet13TableAdapters.ProjectTableAdapter();
            this.panel1.SuspendLayout();
            this.super_adminPanel.SuspendLayout();
            this.manage_usersPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manage_usersdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet6)).BeginInit();
            this.profilePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.reportPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).BeginInit();
            this.add_projectPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet12)).BeginInit();
            this.project_view.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.project_report)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet13)).BeginInit();
            this.SuspendLayout();
            // 
            // border
            // 
            this.border.AutoSize = true;
            this.border.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.border.Location = new System.Drawing.Point(12, 56);
            this.border.Name = "border";
            this.border.Size = new System.Drawing.Size(1064, 17);
            this.border.TabIndex = 0;
            this.border.Text = "_________________________________________________________________________________" +
    "___________________________________________________";
            // 
            // dash_heading
            // 
            this.dash_heading.AutoSize = true;
            this.dash_heading.BackColor = System.Drawing.Color.BurlyWood;
            this.dash_heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dash_heading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dash_heading.Location = new System.Drawing.Point(15, 35);
            this.dash_heading.Name = "dash_heading";
            this.dash_heading.Size = new System.Drawing.Size(297, 25);
            this.dash_heading.TabIndex = 0;
            this.dash_heading.Text = "Project Manager\'s Dashboard";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Khaki;
            this.panel1.Controls.Add(this.logout);
            this.panel1.Controls.Add(this.menu);
            this.panel1.Controls.Add(this.profile);
            this.panel1.Controls.Add(this.viewProject);
            this.panel1.Controls.Add(this.view_reportButton);
            this.panel1.Controls.Add(this.allocate_projectButton);
            this.panel1.Controls.Add(this.manage_usersButton);
            this.panel1.Controls.Add(this.super_adminButton);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(15, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 681);
            this.panel1.TabIndex = 1;
            // 
            // logout
            // 
            this.logout.BackColor = System.Drawing.Color.RosyBrown;
            this.logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.logout.FlatAppearance.BorderSize = 0;
            this.logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logout.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout.ForeColor = System.Drawing.Color.White;
            this.logout.Location = new System.Drawing.Point(21, 554);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(222, 49);
            this.logout.TabIndex = 2;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = false;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // menu
            // 
            this.menu.AutoSize = true;
            this.menu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menu.Font = new System.Drawing.Font("Stencil", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.ForeColor = System.Drawing.Color.Black;
            this.menu.Location = new System.Drawing.Point(62, 23);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(124, 46);
            this.menu.TabIndex = 1;
            this.menu.Text = "MENU";
            // 
            // profile
            // 
            this.profile.BackColor = System.Drawing.Color.RosyBrown;
            this.profile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.profile.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.profile.FlatAppearance.BorderSize = 0;
            this.profile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profile.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profile.ForeColor = System.Drawing.Color.White;
            this.profile.Location = new System.Drawing.Point(21, 483);
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(222, 49);
            this.profile.TabIndex = 0;
            this.profile.Text = "Profile";
            this.profile.UseVisualStyleBackColor = false;
            this.profile.Click += new System.EventHandler(this.profileButton_Click);
            // 
            // viewProject
            // 
            this.viewProject.BackColor = System.Drawing.Color.RosyBrown;
            this.viewProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.viewProject.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.viewProject.FlatAppearance.BorderSize = 0;
            this.viewProject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.viewProject.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewProject.ForeColor = System.Drawing.Color.White;
            this.viewProject.Location = new System.Drawing.Point(21, 326);
            this.viewProject.Name = "viewProject";
            this.viewProject.Size = new System.Drawing.Size(222, 68);
            this.viewProject.TabIndex = 0;
            this.viewProject.Text = "View Project Report";
            this.viewProject.UseVisualStyleBackColor = false;
            this.viewProject.Click += new System.EventHandler(this.view_preportButton_Click);
            // 
            // view_reportButton
            // 
            this.view_reportButton.BackColor = System.Drawing.Color.RosyBrown;
            this.view_reportButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_reportButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.view_reportButton.FlatAppearance.BorderSize = 0;
            this.view_reportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_reportButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_reportButton.ForeColor = System.Drawing.Color.White;
            this.view_reportButton.Location = new System.Drawing.Point(21, 412);
            this.view_reportButton.Name = "view_reportButton";
            this.view_reportButton.Size = new System.Drawing.Size(222, 49);
            this.view_reportButton.TabIndex = 0;
            this.view_reportButton.Text = "View Bug Report";
            this.view_reportButton.UseVisualStyleBackColor = false;
            this.view_reportButton.Click += new System.EventHandler(this.view_reportButton_Click);
            // 
            // allocate_projectButton
            // 
            this.allocate_projectButton.BackColor = System.Drawing.Color.RosyBrown;
            this.allocate_projectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.allocate_projectButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.allocate_projectButton.FlatAppearance.BorderSize = 0;
            this.allocate_projectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allocate_projectButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allocate_projectButton.ForeColor = System.Drawing.Color.White;
            this.allocate_projectButton.Location = new System.Drawing.Point(21, 250);
            this.allocate_projectButton.Name = "allocate_projectButton";
            this.allocate_projectButton.Size = new System.Drawing.Size(222, 53);
            this.allocate_projectButton.TabIndex = 0;
            this.allocate_projectButton.Text = "Allocate Project";
            this.allocate_projectButton.UseVisualStyleBackColor = false;
            this.allocate_projectButton.Click += new System.EventHandler(this.allocate_projectButton_Click);
            // 
            // manage_usersButton
            // 
            this.manage_usersButton.BackColor = System.Drawing.Color.RosyBrown;
            this.manage_usersButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.manage_usersButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.manage_usersButton.FlatAppearance.BorderSize = 0;
            this.manage_usersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.manage_usersButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manage_usersButton.ForeColor = System.Drawing.Color.White;
            this.manage_usersButton.Location = new System.Drawing.Point(21, 175);
            this.manage_usersButton.Name = "manage_usersButton";
            this.manage_usersButton.Size = new System.Drawing.Size(222, 52);
            this.manage_usersButton.TabIndex = 0;
            this.manage_usersButton.Text = "Manage Users\r\n";
            this.manage_usersButton.UseVisualStyleBackColor = false;
            this.manage_usersButton.Click += new System.EventHandler(this.manage_usersButton_Click);
            // 
            // super_adminButton
            // 
            this.super_adminButton.BackColor = System.Drawing.Color.RosyBrown;
            this.super_adminButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.super_adminButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.super_adminButton.FlatAppearance.BorderSize = 0;
            this.super_adminButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.super_adminButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.super_adminButton.ForeColor = System.Drawing.Color.White;
            this.super_adminButton.Location = new System.Drawing.Point(21, 104);
            this.super_adminButton.Name = "super_adminButton";
            this.super_adminButton.Size = new System.Drawing.Size(222, 51);
            this.super_adminButton.TabIndex = 0;
            this.super_adminButton.Text = "Add Super Admin";
            this.super_adminButton.UseVisualStyleBackColor = false;
            this.super_adminButton.Click += new System.EventHandler(this.super_adminButton_Click);
            // 
            // super_adminPanel
            // 
            this.super_adminPanel.BackColor = System.Drawing.Color.White;
            this.super_adminPanel.Controls.Add(this.txt_password);
            this.super_adminPanel.Controls.Add(this.button1);
            this.super_adminPanel.Controls.Add(this.txt_username);
            this.super_adminPanel.Controls.Add(this.txt_email);
            this.super_adminPanel.Controls.Add(this.txt_name);
            this.super_adminPanel.Controls.Add(this.generate_password);
            this.super_adminPanel.Controls.Add(this.generate_button);
            this.super_adminPanel.Controls.Add(this.password);
            this.super_adminPanel.Controls.Add(this.username);
            this.super_adminPanel.Controls.Add(this.email);
            this.super_adminPanel.Controls.Add(this.name);
            this.super_adminPanel.Controls.Add(this.super_adminPanel_Heading);
            this.super_adminPanel.ForeColor = System.Drawing.Color.DimGray;
            this.super_adminPanel.Location = new System.Drawing.Point(406, 90);
            this.super_adminPanel.Name = "super_adminPanel";
            this.super_adminPanel.Size = new System.Drawing.Size(568, 681);
            this.super_adminPanel.TabIndex = 2;
            // 
            // txt_password
            // 
            this.txt_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.Location = new System.Drawing.Point(106, 412);
            this.txt_password.Multiline = true;
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(377, 36);
            this.txt_password.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(106, 514);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(377, 39);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.addButton_Click);
            // 
            // txt_username
            // 
            this.txt_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_username.Location = new System.Drawing.Point(106, 292);
            this.txt_username.Multiline = true;
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(377, 36);
            this.txt_username.TabIndex = 1;
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(106, 216);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(377, 36);
            this.txt_email.TabIndex = 1;
            // 
            // txt_name
            // 
            this.txt_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.Location = new System.Drawing.Point(106, 137);
            this.txt_name.Multiline = true;
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(377, 36);
            this.txt_name.TabIndex = 1;
            // 
            // generate_password
            // 
            this.generate_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.generate_password.Cursor = System.Windows.Forms.Cursors.Hand;
            this.generate_password.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.generate_password.FlatAppearance.BorderSize = 0;
            this.generate_password.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generate_password.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generate_password.ForeColor = System.Drawing.Color.DimGray;
            this.generate_password.Location = new System.Drawing.Point(106, 454);
            this.generate_password.Name = "generate_password";
            this.generate_password.Size = new System.Drawing.Size(222, 36);
            this.generate_password.TabIndex = 0;
            this.generate_password.Text = "Generate Password";
            this.generate_password.UseVisualStyleBackColor = false;
            this.generate_password.Click += new System.EventHandler(this.generate_passwordButton_Click);
            // 
            // generate_button
            // 
            this.generate_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.generate_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.generate_button.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.generate_button.FlatAppearance.BorderSize = 0;
            this.generate_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generate_button.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generate_button.ForeColor = System.Drawing.Color.DimGray;
            this.generate_button.Location = new System.Drawing.Point(106, 334);
            this.generate_button.Name = "generate_button";
            this.generate_button.Size = new System.Drawing.Size(222, 36);
            this.generate_button.TabIndex = 0;
            this.generate_button.Text = "Generate Username";
            this.generate_button.UseVisualStyleBackColor = false;
            this.generate_button.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(102, 385);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(100, 24);
            this.password.TabIndex = 0;
            this.password.Text = "Password";
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(102, 266);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(105, 24);
            this.username.TabIndex = 0;
            this.username.Text = "Username";
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email.Location = new System.Drawing.Point(102, 189);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(62, 24);
            this.email.TabIndex = 0;
            this.email.Text = "Email";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(102, 110);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(65, 24);
            this.name.TabIndex = 0;
            this.name.Text = "Name";
            // 
            // super_adminPanel_Heading
            // 
            this.super_adminPanel_Heading.AutoSize = true;
            this.super_adminPanel_Heading.BackColor = System.Drawing.Color.Transparent;
            this.super_adminPanel_Heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.super_adminPanel_Heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.super_adminPanel_Heading.ForeColor = System.Drawing.Color.DimGray;
            this.super_adminPanel_Heading.Location = new System.Drawing.Point(106, 23);
            this.super_adminPanel_Heading.Name = "super_adminPanel_Heading";
            this.super_adminPanel_Heading.Size = new System.Drawing.Size(315, 36);
            this.super_adminPanel_Heading.TabIndex = 0;
            this.super_adminPanel_Heading.Text = "Add Project Manager";
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("exit.BackgroundImage")));
            this.exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.exit.FlatAppearance.BorderSize = 0;
            this.exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exit.Location = new System.Drawing.Point(1042, 12);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(34, 29);
            this.exit.TabIndex = 3;
            this.exit.UseVisualStyleBackColor = false;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // manage_usersPanel_Heading
            // 
            this.manage_usersPanel_Heading.AutoSize = true;
            this.manage_usersPanel_Heading.BackColor = System.Drawing.Color.Transparent;
            this.manage_usersPanel_Heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.manage_usersPanel_Heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manage_usersPanel_Heading.ForeColor = System.Drawing.Color.DimGray;
            this.manage_usersPanel_Heading.Location = new System.Drawing.Point(113, 23);
            this.manage_usersPanel_Heading.Name = "manage_usersPanel_Heading";
            this.manage_usersPanel_Heading.Size = new System.Drawing.Size(220, 36);
            this.manage_usersPanel_Heading.TabIndex = 0;
            this.manage_usersPanel_Heading.Text = "Manage Users";
            // 
            // view_users
            // 
            this.view_users.AutoSize = true;
            this.view_users.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_users.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.view_users.Location = new System.Drawing.Point(650, 385);
            this.view_users.Name = "view_users";
            this.view_users.Size = new System.Drawing.Size(115, 24);
            this.view_users.TabIndex = 0;
            this.view_users.Text = "View Users";
            // 
            // delete_users
            // 
            this.delete_users.BackColor = System.Drawing.Color.Tomato;
            this.delete_users.Cursor = System.Windows.Forms.Cursors.Hand;
            this.delete_users.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.delete_users.FlatAppearance.BorderSize = 0;
            this.delete_users.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete_users.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete_users.ForeColor = System.Drawing.Color.White;
            this.delete_users.Location = new System.Drawing.Point(203, 258);
            this.delete_users.Name = "delete_users";
            this.delete_users.Size = new System.Drawing.Size(377, 39);
            this.delete_users.TabIndex = 0;
            this.delete_users.Text = "Delete User";
            this.delete_users.UseVisualStyleBackColor = false;
            this.delete_users.Click += new System.EventHandler(this.delete_userClick);
            // 
            // manage_usersPanel
            // 
            this.manage_usersPanel.BackColor = System.Drawing.Color.White;
            this.manage_usersPanel.Controls.Add(this.delete_error);
            this.manage_usersPanel.Controls.Add(this.txt_id);
            this.manage_usersPanel.Controls.Add(this.manage_usersdataGridView);
            this.manage_usersPanel.Controls.Add(this.delete_users);
            this.manage_usersPanel.Controls.Add(this.Id_label);
            this.manage_usersPanel.Controls.Add(this.view_users);
            this.manage_usersPanel.Controls.Add(this.manage_usersPanel_Heading);
            this.manage_usersPanel.Controls.Add(this.label1);
            this.manage_usersPanel.Controls.Add(this.flowLayoutPanel1);
            this.manage_usersPanel.ForeColor = System.Drawing.Color.DimGray;
            this.manage_usersPanel.Location = new System.Drawing.Point(296, 90);
            this.manage_usersPanel.Name = "manage_usersPanel";
            this.manage_usersPanel.Size = new System.Drawing.Size(780, 681);
            this.manage_usersPanel.TabIndex = 2;
            // 
            // delete_error
            // 
            this.delete_error.AutoSize = true;
            this.delete_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete_error.ForeColor = System.Drawing.Color.ForestGreen;
            this.delete_error.Location = new System.Drawing.Point(265, 78);
            this.delete_error.Name = "delete_error";
            this.delete_error.Size = new System.Drawing.Size(268, 20);
            this.delete_error.TabIndex = 6;
            this.delete_error.Text = "Successfully deleted one user!";
            // 
            // txt_id
            // 
            this.txt_id.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_id.ForeColor = System.Drawing.Color.Gray;
            this.txt_id.Location = new System.Drawing.Point(203, 202);
            this.txt_id.Multiline = true;
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(377, 34);
            this.txt_id.TabIndex = 4;
            // 
            // manage_usersdataGridView
            // 
            this.manage_usersdataGridView.AutoGenerateColumns = false;
            this.manage_usersdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.manage_usersdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.useridDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.usernameDataGridViewTextBoxColumn,
            this.passwordDataGridViewTextBoxColumn,
            this.designationDataGridViewTextBoxColumn,
            this.imageDataGridViewTextBoxColumn});
            this.manage_usersdataGridView.DataSource = this.usersBindingSource;
            this.manage_usersdataGridView.Location = new System.Drawing.Point(13, 412);
            this.manage_usersdataGridView.Name = "manage_usersdataGridView";
            this.manage_usersdataGridView.RowTemplate.Height = 24;
            this.manage_usersdataGridView.Size = new System.Drawing.Size(752, 252);
            this.manage_usersdataGridView.TabIndex = 1;
            // 
            // useridDataGridViewTextBoxColumn
            // 
            this.useridDataGridViewTextBoxColumn.DataPropertyName = "User_id";
            this.useridDataGridViewTextBoxColumn.HeaderText = "User_id";
            this.useridDataGridViewTextBoxColumn.Name = "useridDataGridViewTextBoxColumn";
            this.useridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            // 
            // usernameDataGridViewTextBoxColumn
            // 
            this.usernameDataGridViewTextBoxColumn.DataPropertyName = "Username";
            this.usernameDataGridViewTextBoxColumn.HeaderText = "Username";
            this.usernameDataGridViewTextBoxColumn.Name = "usernameDataGridViewTextBoxColumn";
            // 
            // passwordDataGridViewTextBoxColumn
            // 
            this.passwordDataGridViewTextBoxColumn.DataPropertyName = "Password";
            this.passwordDataGridViewTextBoxColumn.HeaderText = "Password";
            this.passwordDataGridViewTextBoxColumn.Name = "passwordDataGridViewTextBoxColumn";
            // 
            // designationDataGridViewTextBoxColumn
            // 
            this.designationDataGridViewTextBoxColumn.DataPropertyName = "Designation";
            this.designationDataGridViewTextBoxColumn.HeaderText = "Designation";
            this.designationDataGridViewTextBoxColumn.Name = "designationDataGridViewTextBoxColumn";
            // 
            // imageDataGridViewTextBoxColumn
            // 
            this.imageDataGridViewTextBoxColumn.DataPropertyName = "Image";
            this.imageDataGridViewTextBoxColumn.HeaderText = "Image";
            this.imageDataGridViewTextBoxColumn.Name = "imageDataGridViewTextBoxColumn";
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.bug_tracker_dbDataSet6;
            // 
            // bug_tracker_dbDataSet6
            // 
            this.bug_tracker_dbDataSet6.DataSetName = "Bug_tracker_dbDataSet6";
            this.bug_tracker_dbDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Id_label
            // 
            this.Id_label.AutoSize = true;
            this.Id_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Id_label.ForeColor = System.Drawing.Color.Gray;
            this.Id_label.Location = new System.Drawing.Point(199, 149);
            this.Id_label.Name = "Id_label";
            this.Id_label.Size = new System.Drawing.Size(178, 24);
            this.Id_label.TabIndex = 0;
            this.Id_label.Text = "Enter ID of a user:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(113, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(659, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "___________________________________________________________";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(147, 110);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(498, 218);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // profilePanel
            // 
            this.profilePanel.BackColor = System.Drawing.Color.White;
            this.profilePanel.Controls.Add(this.pictureBox1);
            this.profilePanel.Controls.Add(this.profileHeader);
            this.profilePanel.Controls.Add(this.name_label);
            this.profilePanel.Controls.Add(this.text_name);
            this.profilePanel.Controls.Add(this.upload_btn);
            this.profilePanel.Controls.Add(this.Update);
            this.profilePanel.Controls.Add(this.text_email);
            this.profilePanel.Controls.Add(this.text_username);
            this.profilePanel.Controls.Add(this.email_label);
            this.profilePanel.Controls.Add(this.username_label);
            this.profilePanel.Controls.Add(this.password_label);
            this.profilePanel.Controls.Add(this.text_password);
            this.profilePanel.Controls.Add(this.label3);
            this.profilePanel.Location = new System.Drawing.Point(296, 90);
            this.profilePanel.Name = "profilePanel";
            this.profilePanel.Size = new System.Drawing.Size(781, 678);
            this.profilePanel.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.Location = new System.Drawing.Point(458, 175);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 250);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // profileHeader
            // 
            this.profileHeader.AutoSize = true;
            this.profileHeader.BackColor = System.Drawing.Color.Transparent;
            this.profileHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profileHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHeader.ForeColor = System.Drawing.Color.DimGray;
            this.profileHeader.Location = new System.Drawing.Point(41, 28);
            this.profileHeader.Name = "profileHeader";
            this.profileHeader.Size = new System.Drawing.Size(158, 36);
            this.profileHeader.TabIndex = 7;
            this.profileHeader.Text = "My Profile";
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_label.Location = new System.Drawing.Point(44, 149);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(61, 24);
            this.name_label.TabIndex = 0;
            this.name_label.Text = "Name";
            // 
            // text_name
            // 
            this.text_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_name.ForeColor = System.Drawing.Color.Gray;
            this.text_name.Location = new System.Drawing.Point(48, 176);
            this.text_name.Multiline = true;
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(342, 34);
            this.text_name.TabIndex = 2;
            // 
            // upload_btn
            // 
            this.upload_btn.BackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.upload_btn.FlatAppearance.BorderSize = 0;
            this.upload_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.upload_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.upload_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upload_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upload_btn.ForeColor = System.Drawing.Color.White;
            this.upload_btn.Location = new System.Drawing.Point(512, 475);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(167, 43);
            this.upload_btn.TabIndex = 4;
            this.upload_btn.Text = "Upload";
            this.upload_btn.UseVisualStyleBackColor = false;
            this.upload_btn.Click += new System.EventHandler(this.image_upload_Click);
            // 
            // Update
            // 
            this.Update.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Update.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Update.FlatAppearance.BorderSize = 0;
            this.Update.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Update.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update.ForeColor = System.Drawing.Color.White;
            this.Update.Location = new System.Drawing.Point(48, 475);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(348, 43);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = false;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // text_email
            // 
            this.text_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_email.ForeColor = System.Drawing.Color.Gray;
            this.text_email.Location = new System.Drawing.Point(48, 248);
            this.text_email.Multiline = true;
            this.text_email.Name = "text_email";
            this.text_email.Size = new System.Drawing.Size(342, 34);
            this.text_email.TabIndex = 2;
            // 
            // text_username
            // 
            this.text_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_username.ForeColor = System.Drawing.Color.Gray;
            this.text_username.Location = new System.Drawing.Point(47, 320);
            this.text_username.Multiline = true;
            this.text_username.Name = "text_username";
            this.text_username.Size = new System.Drawing.Size(342, 32);
            this.text_username.TabIndex = 2;
            // 
            // email_label
            // 
            this.email_label.AutoSize = true;
            this.email_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email_label.Location = new System.Drawing.Point(44, 221);
            this.email_label.Name = "email_label";
            this.email_label.Size = new System.Drawing.Size(57, 24);
            this.email_label.TabIndex = 0;
            this.email_label.Text = "Email";
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_label.Location = new System.Drawing.Point(44, 294);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(97, 24);
            this.username_label.TabIndex = 0;
            this.username_label.Text = "Username";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_label.Location = new System.Drawing.Point(44, 364);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(92, 24);
            this.password_label.TabIndex = 0;
            this.password_label.Text = "Password";
            // 
            // text_password
            // 
            this.text_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_password.ForeColor = System.Drawing.Color.Gray;
            this.text_password.Location = new System.Drawing.Point(48, 391);
            this.text_password.Multiline = true;
            this.text_password.Name = "text_password";
            this.text_password.Size = new System.Drawing.Size(342, 34);
            this.text_password.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(29, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(725, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "_________________________________________________________________";
            // 
            // reportPanel
            // 
            this.reportPanel.BackColor = System.Drawing.Color.White;
            this.reportPanel.Controls.Add(this.excel);
            this.reportPanel.Controls.Add(this.button2);
            this.reportPanel.Controls.Add(this.search);
            this.reportPanel.Controls.Add(this.search_label);
            this.reportPanel.Controls.Add(this.label5);
            this.reportPanel.Controls.Add(this.label4);
            this.reportPanel.Controls.Add(this.filter_label);
            this.reportPanel.Controls.Add(this.comboBox_year);
            this.reportPanel.Controls.Add(this.comboBox_month);
            this.reportPanel.Controls.Add(this.filter);
            this.reportPanel.Controls.Add(this.reports_dataGridView);
            this.reportPanel.Controls.Add(this.heading);
            this.reportPanel.Controls.Add(this.label2);
            this.reportPanel.Location = new System.Drawing.Point(290, 87);
            this.reportPanel.Name = "reportPanel";
            this.reportPanel.Size = new System.Drawing.Size(889, 700);
            this.reportPanel.TabIndex = 14;
            // 
            // excel
            // 
            this.excel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("excel.BackgroundImage")));
            this.excel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.excel.FlatAppearance.BorderSize = 0;
            this.excel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.excel.Location = new System.Drawing.Point(671, -2);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(121, 114);
            this.excel.TabIndex = 18;
            this.excel.UseVisualStyleBackColor = true;
            this.excel.Click += new System.EventHandler(this.excel_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(318, 122);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 42);
            this.button2.TabIndex = 17;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.search_Click);
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.ForeColor = System.Drawing.Color.Gray;
            this.search.Location = new System.Drawing.Point(24, 127);
            this.search.Multiline = true;
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(288, 33);
            this.search.TabIndex = 16;
            // 
            // search_label
            // 
            this.search_label.AutoSize = true;
            this.search_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_label.ForeColor = System.Drawing.Color.Gray;
            this.search_label.Location = new System.Drawing.Point(19, 99);
            this.search_label.Name = "search_label";
            this.search_label.Size = new System.Drawing.Size(75, 25);
            this.search_label.TabIndex = 15;
            this.search_label.Text = "Search";
            this.search_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(411, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 25);
            this.label5.TabIndex = 13;
            this.label5.Text = "Filter by month";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(411, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "Filter by year";
            // 
            // filter_label
            // 
            this.filter_label.AutoSize = true;
            this.filter_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter_label.ForeColor = System.Drawing.Color.Gray;
            this.filter_label.Location = new System.Drawing.Point(19, 174);
            this.filter_label.Name = "filter_label";
            this.filter_label.Size = new System.Drawing.Size(137, 25);
            this.filter_label.TabIndex = 13;
            this.filter_label.Text = "Filter by status";
            // 
            // comboBox_year
            // 
            this.comboBox_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_year.ForeColor = System.Drawing.Color.Gray;
            this.comboBox_year.FormattingEnabled = true;
            this.comboBox_year.Items.AddRange(new object[] {
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.comboBox_year.Location = new System.Drawing.Point(416, 125);
            this.comboBox_year.Name = "comboBox_year";
            this.comboBox_year.Size = new System.Drawing.Size(354, 33);
            this.comboBox_year.TabIndex = 12;
            this.comboBox_year.SelectedIndexChanged += new System.EventHandler(this.year_filterSelectedIndexChanged);
            // 
            // comboBox_month
            // 
            this.comboBox_month.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox_month.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_month.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_month.ForeColor = System.Drawing.Color.Gray;
            this.comboBox_month.FormattingEnabled = true;
            this.comboBox_month.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboBox_month.Location = new System.Drawing.Point(416, 202);
            this.comboBox_month.Name = "comboBox_month";
            this.comboBox_month.Size = new System.Drawing.Size(354, 33);
            this.comboBox_month.TabIndex = 12;
            this.comboBox_month.SelectedIndexChanged += new System.EventHandler(this.month_filterSelectedIndexChanged);
            // 
            // filter
            // 
            this.filter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.filter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter.ForeColor = System.Drawing.Color.Gray;
            this.filter.FormattingEnabled = true;
            this.filter.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.filter.Location = new System.Drawing.Point(20, 202);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(354, 33);
            this.filter.TabIndex = 12;
            this.filter.SelectedIndexChanged += new System.EventHandler(this.filter_SelectedIndexChanged);
            // 
            // reports_dataGridView
            // 
            this.reports_dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.reports_dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.reports_dataGridView.ColumnHeadersHeight = 43;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reports_dataGridView.DefaultCellStyle = dataGridViewCellStyle26;
            this.reports_dataGridView.EnableHeadersVisualStyles = false;
            this.reports_dataGridView.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reports_dataGridView.Location = new System.Drawing.Point(23, 251);
            this.reports_dataGridView.Name = "reports_dataGridView";
            this.reports_dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.reports_dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.reports_dataGridView.RowHeadersVisible = false;
            this.reports_dataGridView.RowHeadersWidth = 50;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(215)))), ((int)(((byte)(208)))));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.reports_dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.reports_dataGridView.RowTemplate.Height = 24;
            this.reports_dataGridView.Size = new System.Drawing.Size(747, 411);
            this.reports_dataGridView.TabIndex = 11;
            // 
            // heading
            // 
            this.heading.AutoSize = true;
            this.heading.BackColor = System.Drawing.Color.Transparent;
            this.heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heading.ForeColor = System.Drawing.Color.DimGray;
            this.heading.Location = new System.Drawing.Point(27, 28);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(111, 36);
            this.heading.TabIndex = 7;
            this.heading.Text = "Report";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(725, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "_________________________________________________________________";
            // 
            // add_projectPanel
            // 
            this.add_projectPanel.BackColor = System.Drawing.Color.White;
            this.add_projectPanel.Controls.Add(this.dateTimePicker2);
            this.add_projectPanel.Controls.Add(this.label8);
            this.add_projectPanel.Controls.Add(this.richTextBox1);
            this.add_projectPanel.Controls.Add(this.label16);
            this.add_projectPanel.Controls.Add(this.dateTimePicker1);
            this.add_projectPanel.Controls.Add(this.assigned_toCombobox);
            this.add_projectPanel.Controls.Add(this.priorityCombobox);
            this.add_projectPanel.Controls.Add(this.statusTextbox);
            this.add_projectPanel.Controls.Add(this.project_titleTextbox);
            this.add_projectPanel.Controls.Add(this.add_projectButton);
            this.add_projectPanel.Controls.Add(this.delivery_date);
            this.add_projectPanel.Controls.Add(this.assigned_to);
            this.add_projectPanel.Controls.Add(this.priority);
            this.add_projectPanel.Controls.Add(this.description);
            this.add_projectPanel.Controls.Add(this.bugTitle);
            this.add_projectPanel.Controls.Add(this.label6);
            this.add_projectPanel.Controls.Add(this.Panel_Heading);
            this.add_projectPanel.Controls.Add(this.label7);
            this.add_projectPanel.Controls.Add(this.git);
            this.add_projectPanel.ForeColor = System.Drawing.Color.DimGray;
            this.add_projectPanel.Location = new System.Drawing.Point(285, 82);
            this.add_projectPanel.Name = "add_projectPanel";
            this.add_projectPanel.Size = new System.Drawing.Size(797, 702);
            this.add_projectPanel.TabIndex = 19;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(26, 380);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(352, 22);
            this.dateTimePicker2.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 345);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 24);
            this.label8.TabIndex = 16;
            this.label8.Text = "Assigned Date";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(28, 448);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(734, 188);
            this.richTextBox1.TabIndex = 15;
            this.richTextBox1.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(408, 271);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 24);
            this.label16.TabIndex = 13;
            this.label16.Text = "Git Link";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(28, 312);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(352, 22);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // assigned_toCombobox
            // 
            this.assigned_toCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.assigned_toCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.assigned_toCombobox.DataSource = this.usersBindingSource3;
            this.assigned_toCombobox.DisplayMember = "Username";
            this.assigned_toCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_toCombobox.ForeColor = System.Drawing.Color.Gray;
            this.assigned_toCombobox.FormattingEnabled = true;
            this.assigned_toCombobox.Location = new System.Drawing.Point(412, 217);
            this.assigned_toCombobox.Name = "assigned_toCombobox";
            this.assigned_toCombobox.Size = new System.Drawing.Size(354, 33);
            this.assigned_toCombobox.TabIndex = 10;
            this.assigned_toCombobox.ValueMember = "Username";
            // 
            // usersBindingSource3
            // 
            this.usersBindingSource3.DataMember = "Users";
            this.usersBindingSource3.DataSource = this.bug_tracker_dbDataSet9;
            // 
            // bug_tracker_dbDataSet9
            // 
            this.bug_tracker_dbDataSet9.DataSetName = "Bug_tracker_dbDataSet9";
            this.bug_tracker_dbDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // priorityCombobox
            // 
            this.priorityCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.priorityCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.priorityCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priorityCombobox.ForeColor = System.Drawing.Color.Gray;
            this.priorityCombobox.FormattingEnabled = true;
            this.priorityCombobox.Items.AddRange(new object[] {
            "High",
            "Medium",
            "Low"});
            this.priorityCombobox.Location = new System.Drawing.Point(26, 217);
            this.priorityCombobox.Name = "priorityCombobox";
            this.priorityCombobox.Size = new System.Drawing.Size(354, 33);
            this.priorityCombobox.TabIndex = 10;
            // 
            // statusTextbox
            // 
            this.statusTextbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.statusTextbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.statusTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusTextbox.FormattingEnabled = true;
            this.statusTextbox.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Fixed"});
            this.statusTextbox.Location = new System.Drawing.Point(26, 137);
            this.statusTextbox.Name = "statusTextbox";
            this.statusTextbox.Size = new System.Drawing.Size(354, 33);
            this.statusTextbox.TabIndex = 10;
            // 
            // project_titleTextbox
            // 
            this.project_titleTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project_titleTextbox.Location = new System.Drawing.Point(411, 134);
            this.project_titleTextbox.Multiline = true;
            this.project_titleTextbox.Name = "project_titleTextbox";
            this.project_titleTextbox.Size = new System.Drawing.Size(354, 36);
            this.project_titleTextbox.TabIndex = 1;
            // 
            // add_projectButton
            // 
            this.add_projectButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.add_projectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add_projectButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.add_projectButton.FlatAppearance.BorderSize = 0;
            this.add_projectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add_projectButton.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add_projectButton.ForeColor = System.Drawing.Color.White;
            this.add_projectButton.Location = new System.Drawing.Point(191, 642);
            this.add_projectButton.Name = "add_projectButton";
            this.add_projectButton.Size = new System.Drawing.Size(354, 39);
            this.add_projectButton.TabIndex = 0;
            this.add_projectButton.Text = "Add Project";
            this.add_projectButton.UseVisualStyleBackColor = false;
            this.add_projectButton.Click += new System.EventHandler(this.add_projectButton_Click);
            // 
            // delivery_date
            // 
            this.delivery_date.AutoSize = true;
            this.delivery_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delivery_date.Location = new System.Drawing.Point(24, 283);
            this.delivery_date.Name = "delivery_date";
            this.delivery_date.Size = new System.Drawing.Size(156, 24);
            this.delivery_date.TabIndex = 0;
            this.delivery_date.Text = "Date of Delivery";
            // 
            // assigned_to
            // 
            this.assigned_to.AutoSize = true;
            this.assigned_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assigned_to.Location = new System.Drawing.Point(408, 189);
            this.assigned_to.Name = "assigned_to";
            this.assigned_to.Size = new System.Drawing.Size(191, 24);
            this.assigned_to.TabIndex = 0;
            this.assigned_to.Text = "Project Assigned to";
            // 
            // priority
            // 
            this.priority.AutoSize = true;
            this.priority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priority.Location = new System.Drawing.Point(22, 190);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(74, 24);
            this.priority.TabIndex = 0;
            this.priority.Text = "Priority";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.Location = new System.Drawing.Point(24, 412);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(186, 24);
            this.description.TabIndex = 0;
            this.description.Text = "Project Description";
            // 
            // bugTitle
            // 
            this.bugTitle.AutoSize = true;
            this.bugTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugTitle.Location = new System.Drawing.Point(407, 102);
            this.bugTitle.Name = "bugTitle";
            this.bugTitle.Size = new System.Drawing.Size(121, 24);
            this.bugTitle.TabIndex = 0;
            this.bugTitle.Text = "Project Title";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Status";
            // 
            // Panel_Heading
            // 
            this.Panel_Heading.AutoSize = true;
            this.Panel_Heading.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Heading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Panel_Heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Heading.ForeColor = System.Drawing.Color.DimGray;
            this.Panel_Heading.Location = new System.Drawing.Point(30, 23);
            this.Panel_Heading.Name = "Panel_Heading";
            this.Panel_Heading.Size = new System.Drawing.Size(182, 36);
            this.Panel_Heading.TabIndex = 0;
            this.Panel_Heading.Text = "Add Project";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Silver;
            this.label7.Location = new System.Drawing.Point(22, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(725, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "_________________________________________________________________";
            // 
            // git
            // 
            this.git.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.git.Location = new System.Drawing.Point(408, 298);
            this.git.Multiline = true;
            this.git.Name = "git";
            this.git.Size = new System.Drawing.Size(354, 36);
            this.git.TabIndex = 14;
            // 
            // session_username
            // 
            this.session_username.BackColor = System.Drawing.SystemColors.Control;
            this.session_username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.session_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.session_username.ForeColor = System.Drawing.Color.Maroon;
            this.session_username.Location = new System.Drawing.Point(833, 39);
            this.session_username.Multiline = true;
            this.session_username.Name = "session_username";
            this.session_username.Size = new System.Drawing.Size(261, 28);
            this.session_username.TabIndex = 10;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // bug_tracker_dbDataSet7
            // 
            this.bug_tracker_dbDataSet7.DataSetName = "Bug_tracker_dbDataSet7";
            this.bug_tracker_dbDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource1
            // 
            this.usersBindingSource1.DataMember = "Users";
            this.usersBindingSource1.DataSource = this.bug_tracker_dbDataSet7;
            // 
            // usersTableAdapter1
            // 
            this.usersTableAdapter1.ClearBeforeFill = true;
            // 
            // bug_tracker_dbDataSet8
            // 
            this.bug_tracker_dbDataSet8.DataSetName = "Bug_tracker_dbDataSet8";
            this.bug_tracker_dbDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource2
            // 
            this.usersBindingSource2.DataMember = "Users";
            this.usersBindingSource2.DataSource = this.bug_tracker_dbDataSet8;
            // 
            // usersTableAdapter2
            // 
            this.usersTableAdapter2.ClearBeforeFill = true;
            // 
            // usersTableAdapter3
            // 
            this.usersTableAdapter3.ClearBeforeFill = true;
            // 
            // bug_tracker_dbDataSet12
            // 
            this.bug_tracker_dbDataSet12.DataSetName = "Bug_tracker_dbDataSet12";
            this.bug_tracker_dbDataSet12.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // project_view
            // 
            this.project_view.BackColor = System.Drawing.Color.White;
            this.project_view.Controls.Add(this.button3);
            this.project_view.Controls.Add(this.srch_btn);
            this.project_view.Controls.Add(this.srch);
            this.project_view.Controls.Add(this.label9);
            this.project_view.Controls.Add(this.label12);
            this.project_view.Controls.Add(this.filter2);
            this.project_view.Controls.Add(this.project_report);
            this.project_view.Controls.Add(this.label13);
            this.project_view.Controls.Add(this.label14);
            this.project_view.Location = new System.Drawing.Point(285, 76);
            this.project_view.Name = "project_view";
            this.project_view.Size = new System.Drawing.Size(804, 700);
            this.project_view.TabIndex = 20;
            // 
            // button3
            // 
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(662, 92);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(121, 114);
            this.button3.TabIndex = 18;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.excelp_Click);
            // 
            // srch_btn
            // 
            this.srch_btn.BackColor = System.Drawing.Color.Transparent;
            this.srch_btn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("srch_btn.BackgroundImage")));
            this.srch_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.srch_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.srch_btn.FlatAppearance.BorderSize = 0;
            this.srch_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.srch_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.srch_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.srch_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.srch_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.srch_btn.Location = new System.Drawing.Point(318, 122);
            this.srch_btn.Name = "srch_btn";
            this.srch_btn.Size = new System.Drawing.Size(60, 42);
            this.srch_btn.TabIndex = 17;
            this.srch_btn.UseVisualStyleBackColor = false;
            this.srch_btn.Click += new System.EventHandler(this.srch_btn_Click);
            // 
            // srch
            // 
            this.srch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.srch.ForeColor = System.Drawing.Color.Gray;
            this.srch.Location = new System.Drawing.Point(24, 127);
            this.srch.Multiline = true;
            this.srch.Name = "srch";
            this.srch.Size = new System.Drawing.Size(288, 33);
            this.srch.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(19, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 25);
            this.label9.TabIndex = 15;
            this.label9.Text = "Search";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.Location = new System.Drawing.Point(19, 174);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 25);
            this.label12.TabIndex = 13;
            this.label12.Text = "Filter by status";
            // 
            // filter2
            // 
            this.filter2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.filter2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter2.ForeColor = System.Drawing.Color.Gray;
            this.filter2.FormattingEnabled = true;
            this.filter2.Items.AddRange(new object[] {
            "Closed",
            "Open",
            "Fixed"});
            this.filter2.Location = new System.Drawing.Point(20, 202);
            this.filter2.Name = "filter2";
            this.filter2.Size = new System.Drawing.Size(354, 33);
            this.filter2.TabIndex = 12;
            this.filter2.SelectedIndexChanged += new System.EventHandler(this.filter2_SelectedIndexChanged);
            // 
            // project_report
            // 
            this.project_report.AutoGenerateColumns = false;
            this.project_report.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.project_report.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.project_report.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.project_report.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.project_report.ColumnHeadersHeight = 43;
            this.project_report.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectIDDataGridViewTextBoxColumn,
            this.projectnameDataGridViewTextBoxColumn,
            this.assigneddateDataGridViewTextBoxColumn,
            this.submissiondateDataGridViewTextBoxColumn,
            this.assignedtoDataGridViewTextBoxColumn,
            this.projectstatusDataGridViewTextBoxColumn,
            this.projectpriorityDataGridViewTextBoxColumn,
            this.additionaldetailsDataGridViewTextBoxColumn,
            this.versionControlLinkDataGridViewTextBoxColumn,
            this.fixedDateDataGridViewTextBoxColumn});
            this.project_report.DataSource = this.projectBindingSource;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.project_report.DefaultCellStyle = dataGridViewCellStyle30;
            this.project_report.EnableHeadersVisualStyles = false;
            this.project_report.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.project_report.Location = new System.Drawing.Point(23, 251);
            this.project_report.Name = "project_report";
            this.project_report.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.project_report.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.project_report.RowHeadersVisible = false;
            this.project_report.RowHeadersWidth = 50;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(215)))), ((int)(((byte)(208)))));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.project_report.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.project_report.RowTemplate.Height = 24;
            this.project_report.Size = new System.Drawing.Size(747, 411);
            this.project_report.TabIndex = 11;
            // 
            // projectIDDataGridViewTextBoxColumn
            // 
            this.projectIDDataGridViewTextBoxColumn.DataPropertyName = "Project_ID";
            this.projectIDDataGridViewTextBoxColumn.HeaderText = "Project_ID";
            this.projectIDDataGridViewTextBoxColumn.Name = "projectIDDataGridViewTextBoxColumn";
            this.projectIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // projectnameDataGridViewTextBoxColumn
            // 
            this.projectnameDataGridViewTextBoxColumn.DataPropertyName = "Project_name";
            this.projectnameDataGridViewTextBoxColumn.HeaderText = "Project_name";
            this.projectnameDataGridViewTextBoxColumn.Name = "projectnameDataGridViewTextBoxColumn";
            // 
            // assigneddateDataGridViewTextBoxColumn
            // 
            this.assigneddateDataGridViewTextBoxColumn.DataPropertyName = "Assigned_date";
            this.assigneddateDataGridViewTextBoxColumn.HeaderText = "Assigned_date";
            this.assigneddateDataGridViewTextBoxColumn.Name = "assigneddateDataGridViewTextBoxColumn";
            // 
            // submissiondateDataGridViewTextBoxColumn
            // 
            this.submissiondateDataGridViewTextBoxColumn.DataPropertyName = "Submission_date";
            this.submissiondateDataGridViewTextBoxColumn.HeaderText = "Submission_date";
            this.submissiondateDataGridViewTextBoxColumn.Name = "submissiondateDataGridViewTextBoxColumn";
            // 
            // assignedtoDataGridViewTextBoxColumn
            // 
            this.assignedtoDataGridViewTextBoxColumn.DataPropertyName = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.HeaderText = "Assigned_to";
            this.assignedtoDataGridViewTextBoxColumn.Name = "assignedtoDataGridViewTextBoxColumn";
            // 
            // projectstatusDataGridViewTextBoxColumn
            // 
            this.projectstatusDataGridViewTextBoxColumn.DataPropertyName = "project_status";
            this.projectstatusDataGridViewTextBoxColumn.HeaderText = "project_status";
            this.projectstatusDataGridViewTextBoxColumn.Name = "projectstatusDataGridViewTextBoxColumn";
            // 
            // projectpriorityDataGridViewTextBoxColumn
            // 
            this.projectpriorityDataGridViewTextBoxColumn.DataPropertyName = "project_priority";
            this.projectpriorityDataGridViewTextBoxColumn.HeaderText = "project_priority";
            this.projectpriorityDataGridViewTextBoxColumn.Name = "projectpriorityDataGridViewTextBoxColumn";
            // 
            // additionaldetailsDataGridViewTextBoxColumn
            // 
            this.additionaldetailsDataGridViewTextBoxColumn.DataPropertyName = "Additional_details";
            this.additionaldetailsDataGridViewTextBoxColumn.HeaderText = "Additional_details";
            this.additionaldetailsDataGridViewTextBoxColumn.Name = "additionaldetailsDataGridViewTextBoxColumn";
            // 
            // versionControlLinkDataGridViewTextBoxColumn
            // 
            this.versionControlLinkDataGridViewTextBoxColumn.DataPropertyName = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn.HeaderText = "Version_Control_Link";
            this.versionControlLinkDataGridViewTextBoxColumn.Name = "versionControlLinkDataGridViewTextBoxColumn";
            // 
            // fixedDateDataGridViewTextBoxColumn
            // 
            this.fixedDateDataGridViewTextBoxColumn.DataPropertyName = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn.HeaderText = "FixedDate";
            this.fixedDateDataGridViewTextBoxColumn.Name = "fixedDateDataGridViewTextBoxColumn";
            // 
            // projectBindingSource
            // 
            this.projectBindingSource.DataMember = "Project";
            this.projectBindingSource.DataSource = this.bug_tracker_dbDataSet13;
            // 
            // bug_tracker_dbDataSet13
            // 
            this.bug_tracker_dbDataSet13.DataSetName = "Bug_tracker_dbDataSet13";
            this.bug_tracker_dbDataSet13.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DimGray;
            this.label13.Location = new System.Drawing.Point(27, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(220, 36);
            this.label13.TabIndex = 7;
            this.label13.Text = "Project Report";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Silver;
            this.label14.Location = new System.Drawing.Point(16, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(725, 24);
            this.label14.TabIndex = 8;
            this.label14.Text = "_________________________________________________________________";
            // 
            // projectTableAdapter
            // 
            this.projectTableAdapter.ClearBeforeFill = true;
            // 
            // Pr_ManagerDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 787);
            this.ControlBox = false;
            this.Controls.Add(this.project_view);
            this.Controls.Add(this.session_username);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dash_heading);
            this.Controls.Add(this.border);
            this.Controls.Add(this.reportPanel);
            this.Controls.Add(this.profilePanel);
            this.Controls.Add(this.manage_usersPanel);
            this.Controls.Add(this.super_adminPanel);
            this.Controls.Add(this.add_projectPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Pr_ManagerDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pr_ManagerDashboard";
            this.Load += new System.EventHandler(this.Pr_ManagerDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.super_adminPanel.ResumeLayout(false);
            this.super_adminPanel.PerformLayout();
            this.manage_usersPanel.ResumeLayout(false);
            this.manage_usersPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.manage_usersdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet6)).EndInit();
            this.profilePanel.ResumeLayout(false);
            this.profilePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.reportPanel.ResumeLayout(false);
            this.reportPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reports_dataGridView)).EndInit();
            this.add_projectPanel.ResumeLayout(false);
            this.add_projectPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet12)).EndInit();
            this.project_view.ResumeLayout(false);
            this.project_view.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.project_report)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bug_tracker_dbDataSet13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label border;
        private System.Windows.Forms.Label dash_heading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button super_adminButton;
        private System.Windows.Forms.Label menu;
        private System.Windows.Forms.Button view_reportButton;
        private System.Windows.Forms.Button allocate_projectButton;
        private System.Windows.Forms.Button manage_usersButton;
        private System.Windows.Forms.Panel super_adminPanel;
        private System.Windows.Forms.Label super_adminPanel_Heading;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button generate_button;
        private System.Windows.Forms.Button generate_password;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Label manage_usersPanel_Heading;
        private System.Windows.Forms.Label view_users;
        private System.Windows.Forms.Button delete_users;
        private System.Windows.Forms.Panel manage_usersPanel;
        private System.Windows.Forms.DataGridView manage_usersdataGridView;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.Label Id_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label delete_error;
        private System.Windows.Forms.Button profile;
        private System.Windows.Forms.Panel profilePanel;
        private System.Windows.Forms.Label profileHeader;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.TextBox text_email;
        private System.Windows.Forms.TextBox text_username;
        private System.Windows.Forms.Label email_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.TextBox text_password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.TextBox session_username;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Panel reportPanel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox search;
        private System.Windows.Forms.Label search_label;
        private System.Windows.Forms.Label filter_label;
        private System.Windows.Forms.ComboBox filter;
        private System.Windows.Forms.DataGridView reports_dataGridView;
        private System.Windows.Forms.Label heading;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button excel;
        private System.Windows.Forms.ComboBox comboBox_month;
        private System.Windows.Forms.ComboBox comboBox_year;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Bug_tracker_dbDataSet6 bug_tracker_dbDataSet6;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private Bug_tracker_dbDataSet6TableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn useridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn passwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn designationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imageDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel add_projectPanel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox assigned_toCombobox;
        private System.Windows.Forms.ComboBox priorityCombobox;
        private System.Windows.Forms.ComboBox statusTextbox;
        private System.Windows.Forms.TextBox project_titleTextbox;
        private System.Windows.Forms.Button add_projectButton;
        private System.Windows.Forms.Label delivery_date;
        private System.Windows.Forms.Label assigned_to;
        private System.Windows.Forms.Label priority;
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label bugTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Panel_Heading;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox git;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label8;
        private Bug_tracker_dbDataSet7 bug_tracker_dbDataSet7;
        private System.Windows.Forms.BindingSource usersBindingSource1;
        private Bug_tracker_dbDataSet7TableAdapters.UsersTableAdapter usersTableAdapter1;
        private Bug_tracker_dbDataSet8 bug_tracker_dbDataSet8;
        private System.Windows.Forms.BindingSource usersBindingSource2;
        private Bug_tracker_dbDataSet8TableAdapters.UsersTableAdapter usersTableAdapter2;
        private Bug_tracker_dbDataSet9 bug_tracker_dbDataSet9;
        private System.Windows.Forms.BindingSource usersBindingSource3;
        private Bug_tracker_dbDataSet9TableAdapters.UsersTableAdapter usersTableAdapter3;
        private System.Windows.Forms.Button viewProject;
        private Bug_tracker_dbDataSet12 bug_tracker_dbDataSet12;
        private System.Windows.Forms.Panel project_view;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button srch_btn;
        private System.Windows.Forms.TextBox srch;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox filter2;
        private System.Windows.Forms.DataGridView project_report;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Bug_tracker_dbDataSet13 bug_tracker_dbDataSet13;
        private System.Windows.Forms.BindingSource projectBindingSource;
        private Bug_tracker_dbDataSet13TableAdapters.ProjectTableAdapter projectTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assigneddateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn submissiondateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedtoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectstatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectpriorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn additionaldetailsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn versionControlLinkDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fixedDateDataGridViewTextBoxColumn;
    }
}