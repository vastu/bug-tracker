﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using Tulpep.NotificationWindow;
using System.Configuration;

namespace BugTracker
{
    public partial class Pr_ManagerDashboard : Form
    {

        //String con = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        // Creates a connection to the database
        SqlConnection con = new SqlConnection(@"Data Source=utsav;Initial Catalog=Bug_tracker_db;Integrated Security=True");
        private SqlCommand command;
        String ImageFileName;
        String usrname;
        OpenFileDialog open = new OpenFileDialog();// Opens the file Dialog


        public Pr_ManagerDashboard(String Username) //Username Argument passed for the Session Purpose
        {
            InitializeComponent();
            session_username.Text = Username;
            usrname = Username;           

        }

        //Application is Exited when clicked on cross Button
        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Pr_ManagerDashboard_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet13.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter.Fill(this.bug_tracker_dbDataSet13.Project);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet12.Project' table. You can move, or remove it, as needed.
            //this.projectTableAdapter.Fill(this.bug_tracker_dbDataSet12.Project);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet9.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter3.Fill(this.bug_tracker_dbDataSet9.Users);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet8.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter2.Fill(this.bug_tracker_dbDataSet8.Users);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet7.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter1.Fill(this.bug_tracker_dbDataSet7.Users);
            // TODO: This line of code loads data into the 'bug_tracker_dbDataSet6.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter.Fill(this.bug_tracker_dbDataSet6.Users);
            reportPanel.Show();
            super_adminPanel.Hide();
            manage_usersPanel.Hide();
            profilePanel.Hide();

            // Creating the Object of Account Class
            Account account = new Account();
            account.Close();
            con.Open();
            display_users();
            con.Close();
            display_report();
            delete_error.Hide();

            //fetch and display the data in textbox
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Users Where Username='"+usrname+"'", con);
            SqlDataReader dr = cmd.ExecuteReader(); //provides the way of reading Rows from the database Tables

            //Displays all the data from database in respective profile textBoxes 
            if (dr.Read())
            {
                text_name.Text = (dr["Name"].ToString());
                text_email.Text = (dr["Email"].ToString());
                text_username.Text = (dr["Username"].ToString());
                text_password.Text = (dr["Password"].ToString());

                // Checks if the table row consist of a image path value
                try
                {
                    String img_path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    pictureBox1.Image = Image.FromFile(img_path + dr["Image"].ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("please update your profile picture!");
                }
            }

            con.Close();
        }
       
        //Event handler when Generate Username Button is clicked to assign the username and password to the new Project Manager
        private void generateButton_Click(object sender, EventArgs e)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8]; // consist of 8 values in an array
            var random = new Random(); // generates random number

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            txt_username.Text = finalString;
        }

        //Event handler when Generate Password Button is clicked to assign the username and password to the new Project Manager
        private void generate_passwordButton_Click(object sender, EventArgs e)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];// consist of 8 values in an array
            var random = new Random(); // generates random number

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            txt_password.Text = finalString; // assigns the value stored in a string to the textBox
        }

        //Event handler When the Add Manager Button is Clicked
        private void addButton_Click(object sender, EventArgs e)
        {        
            con.Open();
            //Insert Query into the database Table
            String str = "insert into Users(Name,Email,Username,Password,Designation) values('" + txt_name.Text + "','" + txt_email.Text + "','" + txt_username.Text + "','" + txt_password.Text + "', 'Pr_Manager')";
            SqlCommand cmd = new SqlCommand(str, con); // Represents a sql statement
            cmd.ExecuteNonQuery();// Executes the Query
            MessageBox.Show("Sucessfully Registered!");
            con.Close();
            
        }

        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void manage_usersButton_Click(object sender, EventArgs e)
        {
            project_view.Hide();
            super_adminPanel.Hide();
            manage_usersPanel.Show();
            add_projectPanel.Hide();
            reportPanel.Hide();
            profilePanel.Hide();
        }

        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void view_preportButton_Click(object sender, EventArgs e)
        {
            super_adminPanel.Hide();
            manage_usersPanel.Hide();
            add_projectPanel.Hide();
            reportPanel.Hide();
            profilePanel.Hide();
            project_view.Show();
        }

        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void super_adminButton_Click(object sender, EventArgs e)
        {
            super_adminPanel.Show();
            manage_usersPanel.Hide();
            add_projectPanel.Hide();
            reportPanel.Hide();
            profilePanel.Hide();
            project_view.Hide();
        }

        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void profileButton_Click(object sender, EventArgs e)
        {
            getUsers();
            super_adminPanel.Hide();
            manage_usersPanel.Hide();
            add_projectPanel.Hide();
            reportPanel.Hide();
            profilePanel.Show();
            project_view.Hide();
        }

        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void allocate_projectButton_Click(object sender, EventArgs e)
        {
            add_projectPanel.Show();
            super_adminPanel.Hide();
            manage_usersPanel.Hide();
            reportPanel.Hide();
            profilePanel.Hide();
            project_view.Hide();
        }
        //Event handler When the  Button is Clicked to Show or Hide the Panels
        private void view_reportButton_Click(object sender, EventArgs e)
        {
            reportPanel.Show();
            project_view.Hide();
            add_projectPanel.Hide();
            super_adminPanel.Hide();
            manage_usersPanel.Hide();
            profilePanel.Hide();
        }

        //Event handler When the Logot Button is Clicked to Restart the application
        private void logout_Click(object sender, EventArgs e)
        {
            Application.Restart();// Restarts the application
        }

        //Event handler When the  Button is Clicked to Delete the users from database table
        private void delete_userClick(object sender, EventArgs e)
        {
            con.Open();
            String str = "delete users where User_id='" +txt_id.Text+"' ";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            delete_error.Show();
            display_users();
            txt_id.Text = "";
            con.Close();
        }

        private void image_upload_Click(object sender, EventArgs e)
        {     
         
            //To where your opendialog box get starting location. My initial directory location is desktop.
            open.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            open.Title = "Select image to be upload.";
            //which type image format you want to upload in database. just add them.
            open.Filter = "Image Only(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            open.FilterIndex = 1;
            try
            {
                if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (open.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(open.FileName);
                        label1.Text = path;
                        pictureBox1.Image = new Bitmap(open.FileName); //Initializes a new instance of bitMap Class
                        // Stretches the image according to the size of PictureBox
                        pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage; 
                    }
                }
                else
                {
                    MessageBox.Show("Please Upload image.");
                }
            }
            catch (Exception ex)
            {
                //it will give if file is already exits..
                MessageBox.Show(ex.Message);
            }

        }
        /// <summary>
        /// Selects the Users from users Table and displays in a respective gridBox 
        /// </summary>
        public void display_users()
        {

            String str = "select * from Users";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable(); //Represents 1 table of in-memory Data
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);  // Represents a set of data commands and a database connection that are used to fill the DataSet and update a SQL Server database.          
            adapt.Fill(dt);
            manage_usersdataGridView.DataSource = dt;
            
        }

        //Event Handler when Update Button is clicked 
        private void Update_Click(object sender, EventArgs e)
        {

            try
            {
                String filename = System.IO.Path.GetFileName(open.FileName);
                
                con.Open();
                String sql = "UPDATE users SET Image='\\Image\\" + filename + "', Name = '" + text_name.Text + "', Email = '" + text_email.Text + "',Username='" + text_username.Text + "',Password= '" + text_password.Text + "' where Username='" + usrname + "' ";

                SqlCommand cmd = new SqlCommand(sql, con);                
                String path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(open.FileName, path + "\\Image\\" + filename);
                cmd.ExecuteNonQuery();
                con.Close();
                
                MessageBox.Show("Updated successfully!!");
           

            }
            catch(Exception ex)
            {
                con.Close();
                MessageBox.Show(ex.Message);
                    
            }
        }

        /// <summary>
        /// Displays all the Bugs from The "Bugs" Table in a Database
        /// </summary>
        public void display_report()
        {

            con.Open();
            String str = "select * from Bugs";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable(); //Represents 1 table of in-memory Data
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler When Search button is clicked
        private void search_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where CONCAT(Status,Bug_title,Priority,Assigned_to,Bug_Assigned_by,Date_of_delivery,Additional_info,Bug_line_no,project,Class,method,Code_author_name) LIKE '%" + search.Text + "%' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler When Filter button is clicked
        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Bugs where status = '" + filter.Text + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        /// <summary>
        /// Transfers all the data from table into Excel file and saves the file in .xls format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excel_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int i = 0;
            int j = 0;

            for (i = 0; i <= reports_dataGridView.RowCount - 1; i++)
            {
                for (j = 0; j <= reports_dataGridView.ColumnCount - 1; j++)
                {
                    DataGridViewCell cell = reports_dataGridView[j, i];
                    xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
                }
            }

            //Saves the file as excel format in a particular folder
            xlWorkBook.SaveAs("C:\\Users\\Vastu\\source\\repos\\BugTracker\\BugTracker\\Files\\excel.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            
            MessageBox.Show("Excel file created , you can find the file C:\\Users\\Vastu\\source\\repos\\BugTracker\\BugTracker\\Files\\excel.xls");

        }

        //Event handler When Filter button is clicked to view the reports of a particular month
        private void month_filterSelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();            
            string query = "SELECT * FROM Bugs WHERE Month(FillDate)= '" + comboBox_month.SelectedItem + "' ORDER BY  month(FillDate) ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler When Filter button is clicked to view the reports of a particular year
        private void year_filterSelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();            
            string query = "SELECT * FROM Bugs WHERE Year(FillDate)= '" + comboBox_year.SelectedItem + "' ORDER BY  Year(FillDate) ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            reports_dataGridView.DataSource = dt;
            con.Close();
        }

        //Event handler for adding project Button
        private void add_projectButton_Click(object sender, EventArgs e)
        {
            con.Open();
            String str = "insert into Project(Version_Control_Link,Project_status,project_name,Project_priority,Assigned_to,Assigned_date,Submission_date,Additional_details) values('" + git.Text + "','" + statusTextbox.Text + "','" + project_titleTextbox.Text + "','" + priorityCombobox.SelectedItem + "','" + assigned_toCombobox.Text + "', '" + dateTimePicker2.Text + "', '" + dateTimePicker1.Text + "','" + richTextBox1.Text + "' )";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Project Sucessfully Assigned!");
            con.Close();
            
        }

        /// <summary>
        /// Selects all the users whose designation is developer and assignes the value to a particular Combobox
        /// </summary>
        public void getUsers()
        {
            DataSet datas = new DataSet();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select Name, Username from users where designation='Developer' ", con);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(datas);
                assigned_toCombobox.DisplayMember = "Name";
                assigned_toCombobox.ValueMember = "Username";
                assigned_toCombobox.DataSource = datas.Tables[0];
            }
            catch (Exception Ex)
            {
                //Exception Message
            }
            finally
            {
                con.Close();
            }

        }

        //Event handler When Search button is clicked to view the Project reports
        private void srch_btn_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Project where CONCAT(Project_Status,Project_name,Project_priority,Assigned_to,Submission_date,Additional_details,Assigned_date) LIKE '%" + srch.Text + "%' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            project_report.DataSource = dt;
            con.Close();
        }

        //Event handler When Filter button is clicked to view the Project reports
        private void filter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "select * from Project where project_status = '" + filter2.Text + "' ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            adapt.Fill(dt);
            project_report.DataSource = dt;
            con.Close();
        }

        /// <summary>
        ///  Transfers all the data from table into Excel file and saves the file in .xls format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelp_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int i = 0;
            int j = 0;

            for (i = 0; i <= project_report.RowCount - 1; i++)
            {
                for (j = 0; j <= project_report.ColumnCount - 1; j++)
                {
                    DataGridViewCell cell = project_report[j, i];
                    xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
                }
            }

            // Saves the File as .xls format
            xlWorkBook.SaveAs("C:\\Users\\Vastu\\source\\repos\\BugTracker\\BugTracker\\Files\\excelProject.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            
            MessageBox.Show("Excel file created , you can find the file C:\\Users\\Vastu\\source\\repos\\BugTracker\\BugTracker\\Files\\excelProject.xls");

        }
    }
}
