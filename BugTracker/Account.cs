﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class Account : Form
    {
        //String conString = ConfigurationManager.ConnectionStrings["connection"].ToString();
        //SqlConnection con = new SqlConnection(conString);
        SqlConnection con = new SqlConnection(@"Data Source=utsav;Initial Catalog=Bug_tracker_db;Integrated Security=True");
        DataSet ds = new DataSet();
        SqlCommand cmd;
        



        public Account()
        {
            InitializeComponent();

            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        
         private void Account_Load(object sender, EventArgs e)
        {
            panel3.Hide();
            passwords_error.Hide();
            designation_null_error.Hide();
            login_error.Hide();
            username_null_error.Hide();
            username_check_error.Hide();


        }

        private void Account_Click(object sender, EventArgs e)
        {
            txt_name.Text = "";
            txt_username.Text = "";
            txt_password.Text = "";
            text_username.Text = "";
            text_password.Text = "";
            txt_email.Text = "";
            txt_cpassword.Text = "";
            designation.Text = "";
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        private void Login_Click(object sender, EventArgs e)
        {
           
            string query = "Select * from users Where username = '" + text_username.Text.Trim() + "' and password = '" + text_password.Text.Trim() + "' and designation = 'Pr_Manager'";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dtbl = new DataTable();
            sda.Fill(dtbl);
            string query1 = "Select * from users Where username = '" + text_username.Text.Trim() + "' and password = '" + text_password.Text.Trim() + "' and designation = 'Tester'";
            SqlDataAdapter sda1 = new SqlDataAdapter(query1, con);
            DataTable dtbl1 = new DataTable();
            sda1.Fill(dtbl1);
            string query2 = "Select * from users Where username = '" + text_username.Text.Trim() + "' and password = '" + text_password.Text.Trim() + "' and designation = 'Developer'";
            SqlDataAdapter sda2 = new SqlDataAdapter(query2, con);
            DataTable dtbl2 = new DataTable();
            sda2.Fill(dtbl2);
            if (dtbl.Rows.Count == 1)
            {
                
                MessageBox.Show("You are logged in as Project Manager.");
                Account account = new Account();
                account.Close();
                Pr_ManagerDashboard pr_manager_dash = new Pr_ManagerDashboard(text_username.Text);
                pr_manager_dash.Show();
            }
            else if (dtbl1.Rows.Count == 1)
            {
               
                MessageBox.Show("You are logged in as Tester.");
                Tester_Dashboard tester = new Tester_Dashboard(text_username.Text);
                Account account = new Account();
                account.Close();
                tester.Show();
            }
            else if (dtbl2.Rows.Count == 1)
            {
                Developer_Dashboard dev = new Developer_Dashboard(text_username.Text);
                Account account = new Account();
                account.Close();
                dev.Show();
                MessageBox.Show("You are logged in as Developer.");

            }
            else
            {
                MessageBox.Show("Invalid username or password!");
            }
        }

        private void Register_Click(object sender, EventArgs e)
        {

            if (txt_username.Text == "" || txt_password.Text == "")
            {
                panel3.Show();
                username_null_error.Show();
                passwords_error.Hide();
                designation_null_error.Hide();
                login_error.Hide();         
                username_check_error.Hide();
            }            
            else if(txt_password.Text != txt_cpassword.Text)
            {
                panel3.Show();
                passwords_error.Show();             
                designation_null_error.Hide();
                login_error.Hide();
                username_null_error.Hide();
                username_check_error.Hide();
            }
            else if (designation.Text == "")
            {
                panel3.Show();
                designation_null_error.Show();
                passwords_error.Hide();
                login_error.Hide();
                username_null_error.Hide();
                username_check_error.Hide();
            }
            else if(designation.Text != "")
            {
                String sel = "select * from users where Username='" + txt_username.Text + "'";
                SqlDataAdapter sda = new SqlDataAdapter(sel, con);
                DataTable dtbl = new DataTable();
                sda.Fill(dtbl);
                if (dtbl.Rows.Count == 1)
                {
                    panel3.Show();
                    username_check_error.Show();
                    passwords_error.Hide();
                    login_error.Hide();
                    username_null_error.Hide();
                    designation_null_error.Hide();
                }
                else
                {

                    panel3.Hide();
                    con.Open();
                    String str = "insert into Users(Name,Email,Username,Password,Designation) values('" + txt_name.Text + "','" + txt_email.Text + "','" + txt_username.Text + "','" + txt_password.Text + "', '" + designation.SelectedItem + "')";
                    SqlCommand cmd = new SqlCommand(str, con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Sucessfully Registered!");
                    con.Close();
                }
                
            }
            
        }

        
    }
}
