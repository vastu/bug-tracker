USE [master]
GO
/****** Object:  Database [Bug_tracker_db]    Script Date: 1/12/2018 6:10:22 PM ******/
CREATE DATABASE [Bug_tracker_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Bug_tracker_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Bug_tracker_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Bug_tracker_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Bug_tracker_db_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Bug_tracker_db] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Bug_tracker_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Bug_tracker_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Bug_tracker_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Bug_tracker_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Bug_tracker_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Bug_tracker_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET RECOVERY FULL 
GO
ALTER DATABASE [Bug_tracker_db] SET  MULTI_USER 
GO
ALTER DATABASE [Bug_tracker_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Bug_tracker_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Bug_tracker_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Bug_tracker_db] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Bug_tracker_db] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Bug_tracker_db', N'ON'
GO
ALTER DATABASE [Bug_tracker_db] SET QUERY_STORE = OFF
GO
USE [Bug_tracker_db]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Bug_tracker_db]
GO
/****** Object:  Table [dbo].[Bugs]    Script Date: 1/12/2018 6:10:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bugs](
	[Bug_ID] [int] IDENTITY(1,1) NOT NULL,
	[FillDate] [datetime] NOT NULL,
	[Status] [varchar](100) NULL,
	[Priority] [varchar](50) NULL,
	[Assigned_to] [varchar](50) NULL,
	[Additional_info] [varchar](255) NULL,
	[Bug_Assigned_by] [varchar](50) NULL,
	[Bug_title] [varchar](255) NULL,
	[Date_of_delivery] [varchar](50) NULL,
	[Bug_line_no] [int] NULL,
	[method] [varchar](150) NULL,
	[Class] [varchar](150) NULL,
	[Project] [varchar](150) NULL,
	[Code_author_name] [varchar](150) NULL,
	[Files] [varchar](200) NULL,
	[FixedDate] [varchar](200) NULL,
	[Version_Control_Link] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Bug_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Project]    Script Date: 1/12/2018 6:10:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[Project_ID] [int] IDENTITY(1,1) NOT NULL,
	[Project_name] [varchar](100) NULL,
	[Assigned_date] [varchar](200) NULL,
	[Submission_date] [varchar](200) NULL,
	[Assigned_to] [varchar](100) NULL,
	[project_status] [varchar](100) NULL,
	[project_priority] [varchar](100) NULL,
	[Additional_details] [varchar](255) NULL,
	[Version_Control_Link] [varchar](255) NULL,
	[FixedDate] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Project_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/12/2018 6:10:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[User_id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NULL,
	[Email] [varchar](100) NULL,
	[Username] [varchar](100) NULL,
	[Password] [varchar](100) NULL,
	[Designation] [varchar](10) NULL,
	[Image] [varchar](200) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[User_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bugs] ADD  DEFAULT (getdate()) FOR [FillDate]
GO
/****** Object:  StoredProcedure [dbo].[getBugs]    Script Date: 1/12/2018 6:10:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getBugs]
AS 
	BEGIN
	SELECT * FROM BUGS;
	END;
GO
USE [master]
GO
ALTER DATABASE [Bug_tracker_db] SET  READ_WRITE 
GO
